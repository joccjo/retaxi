package com.system.sumo.retaxi.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class CityLocation {
    public static final double[] CITY_LOCATION = {-27.4806000,-58.8341000};
    public static final int ACCUARY_LOCATION_MTS = 20;
    /*private static final LatLng BOTOM_LEFT = new LatLng(-27.534662, -58.860710);
    private static final LatLng TOP_RIGHT = new LatLng(-27.433760, -58.716209);*/
    private static final LatLng BOTOM_LEFT = new LatLng(-27.496583, -58.850056);
    private static final LatLng TOP_RIGHT = new LatLng(-27.467224, -58.781867);
    public static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds( BOTOM_LEFT,TOP_RIGHT);
}
