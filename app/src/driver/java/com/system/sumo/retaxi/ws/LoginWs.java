package com.system.sumo.retaxi.ws;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.system.sumo.retaxi.bo.LocationBo;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.utils.Constantes;
import com.system.sumo.retaxi.utils.RestCustomerVolley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.system.sumo.retaxi.utils.ServicesRoutes.BASE_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_ID;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_ID_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_SECRET;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_SECRET_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.GET_ACCESS_TOKEN_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_ASSOCIATE_DRIVER_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_CONVER_TOKEN;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_DRIVER_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_BACKEND_FACEBOOK;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_BACKEND_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_IP;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_ROUTE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_CODE;

public class LoginWs extends GenericWs {

    private void convertToken(String tokenGoogle, String tokenFacebook, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("grant_type", "convert_token");
            body.put("client_id", CLIENT_ID);
            body.put("client_secret", CLIENT_SECRET);
            if(tokenGoogle != null)
                body.put("backend", "google-oauth2");
            else if (tokenFacebook != null)
                body.put("backend", "facebook");
            body.put("token", tokenGoogle);
        } catch (JSONException e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_CONVER_TOKEN, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                JSONObject result = null;
                try {
                    result = new JSONObject(object);
                    String accessToken = result.getString("access_token");
                    String refreshToken = result.getString("refresh_token");
                    PreferencesBo.saveTokensInPreferences(accessToken);
                    genericWsResponse.onOkResult(accessToken);
                } catch (JSONException e) {
                    e.printStackTrace();
                    genericWsResponse.onErrorResult();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void loginGoogle(String codigoAuth, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("provider", WS_BACKEND_GOOGLE);
            body.put("code", codigoAuth);
            body.put("redirect_uri",BASE_WS + WS_IP + WS_ROUTE);
        } catch (JSONException e) { e.printStackTrace(); }

        loginDriver(body, genericWsResponse);
    }

    public void loginFacebook(String tokenfc, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("provider", WS_BACKEND_FACEBOOK);
            body.put("code", 1);
            body.put("access_token", tokenfc);
            body.put("redirect_uri",BASE_WS + WS_IP + WS_ROUTE);
        } catch (JSONException e) { e.printStackTrace(); }
        loginDriver(body, genericWsResponse);
    }

    public void loginDriver(JSONObject body, final GenericWsResponse genericWsResponse){

        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_DRIVER_WS, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                checkResult(object, genericWsResponse);
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    private void checkResult(String object, GenericWsResponse genericWsResponse){
        String firstName = null;
        Gson gson = new Gson();
        try {
            JSONObject jsonObject = new JSONObject(object);
            firstName = jsonObject.getString(Constantes.FIRST_NAME);
            object = LocationBo.initializeLocationPoint(jsonObject, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (firstName != null && !firstName.trim().equals("")){
            try {
                Usuario user = gson.fromJson(object, Usuario.class);
                user.setTipoUsuario(Usuario.TIPO_CONDUCTOR);
                PreferencesBo.saveUserInPreferences(user);
                genericWsResponse.onOkResult(user);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
            try {
                ResultWsCall resultWsCall = gson.fromJson(object, ResultWsCall.class);
                genericWsResponse.onOkResult(resultWsCall);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void associateDriver(String dni, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("dni", dni);
        } catch (JSONException e) { e.printStackTrace(); }

        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_ASSOCIATE_DRIVER_WS, body, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                checkResult(object, genericWsResponse);
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void getAccessToken(String code, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("grant_type", "authorization_code");
            body.put("client_id", CLIENT_ID_GOOGLE);
            body.put("client_secret", CLIENT_SECRET_GOOGLE);
            body.put("redirect_uri","");
            body.put("code", code);
        } catch (JSONException e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, GET_ACCESS_TOKEN_GOOGLE, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                try {
                    JSONObject result = new JSONObject(object);
                    String access_token = result.getString("access_token");
                    convertToken(access_token, null, genericWsResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }
}
