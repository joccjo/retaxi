package com.system.sumo.retaxi.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.view.chofer.TravelsFragment;
import com.system.sumo.retaxi.view.chofer.ConectarseTabFragment;
import com.system.sumo.retaxi.view.chofer.CuentaChoferFragment;


public class MainActivity extends AppCompatActivity {
    private static String TAB1 = "tab1";
    private static String TAB2 = "tab2";
    private static String TAB3 = "tab3";
    private FragmentTabHost tabHost;
    private static String EXTRA_IS_CHOFER_EN_VIAJE = "EXTRA_IS_CHOFER_EN_VIAJE";
    private static String EXTRA_IS_CHOFER_CONECTADO = "EXTRA_IS_CHOFER_CONECTADO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chofer_main);
        initComponents();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Fragment uploadType = getSupportFragmentManager().findFragmentByTag(TAB2);
        if (uploadType != null) {
            uploadType.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initComponents(){

        tabHost= (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec(TAB1).setIndicator(getTabIndicator(this, getString(R.string.cuenta), R.mipmap.ic_perm_identity_white_24dp)),
                CuentaChoferFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec(TAB2).setIndicator(getTabIndicator(this, getString(R.string.conectarse), R.mipmap.ic_directions_car_white_24dp)),
                ConectarseTabFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec(TAB3).setIndicator(getTabIndicator(this, getString(R.string.viajes), R.mipmap.ic_trending_up_white_24dp)),
                TravelsFragment.class, null);
        /*Intent i = new Intent(this, UpdateScreenOnOfService.class);
        this.startService(i);*/

    }

    private View getTabIndicator(Context context, String title, int icon) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(icon);
        return view;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}

