package com.system.sumo.retaxi.services;

import android.content.Intent;
import android.media.RingtoneManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.dto.Movil;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Tarifa;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Constantes;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.chat.ChatActivity;
import com.system.sumo.retaxi.view.pasajero.PedirTaxiFragment;
import com.system.sumo.retaxi.view.chofer.GestionarSolicitudActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.system.sumo.retaxi.utils.Avisos.getRingtonNotification;
import static com.system.sumo.retaxi.utils.Avisos.startNotification;
import static com.system.sumo.retaxi.utils.Avisos.startNotificationChat;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CANCELADO_X_CONDUCTOR;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CANCELLED_BY_RIDER;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CHAT;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_REQUEST_ACCEPTED;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_UBICACION_CONDUCTOR;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CANCELADO_X_BASE;
import com.system.sumo.retaxi.bo.ChatMessageBo;

/**
 * Created by usuario on 15/09/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    //public static Context mContext;
    private static OnMessageRecibed onMessageRecibed;
    private static OnMessageChatRecibed onMessageChatRecibed;

    public MyFirebaseMessagingService(){
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Map<String, String> mensaje = remoteMessage.getData();
            switch (mensaje.get(Constantes.MESSAGE_TYPE)) {
                case Constantes.MESSAGE_TYPE_TAXI_REQUEST:
                    //the driver recives a request
                    showManageRequest(mensaje);
                    break;
                case MESSAGE_TYPE_REQUEST_ACCEPTED:
                    //the travel comes accepted
                    showManageRequest(mensaje);
                    break;
                case MESSAGE_TYPE_CANCELLED_BY_RIDER:
                    //al conductor le cancelan el viaje
                    if (onMessageRecibed != null) {
                        onMessageRecibed.doCallBack(null, MESSAGE_TYPE_CANCELLED_BY_RIDER);
                        MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_CANCELLED_BY_RIDER);
                    }
                    else {
                        MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_CANCELLED_BY_RIDER);
                        startNotification(getString(R.string.pasajeroCancelo), this);
                    }
                    getRingtonNotification(this, RingtoneManager.TYPE_RINGTONE);
                    break;
                case MESSAGE_TYPE_CANCELADO_X_CONDUCTOR:
                    //al pasajero le cancelan el viaje
                    PreferencesBo.updateTravelState(Viaje.TRAVEL_STATE_CANCELLED_BY_COMPANY);
                    if (onMessageRecibed != null)
                        onMessageRecibed.doCallBack(null, MESSAGE_TYPE_CANCELADO_X_CONDUCTOR);
                    else {
                        MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_CANCELADO_X_CONDUCTOR);
                        getRingtonNotification(this, RingtoneManager.TYPE_RINGTONE);
                    }
                    startNotification(getString(R.string.conductoresCancelo), this);
                    break;
                case MESSAGE_TYPE_CHAT:
                    ChatMessage chatMessage = mappearMensajeChat(mensaje);
                    ChatMessageBo chatMessageBo = new ChatMessageBo();
                    chatMessageBo.create(chatMessage);
                    if(!ChatActivity.active) {
                        startNotificationChat(this, chatMessage);
                        //getRingtonNotification(this, RingtoneManager.TYPE_RINGTONE);
                    }
                    if((PedirTaxiFragment.itsFragmentActive || ChatActivity.active) && onMessageChatRecibed != null)
                        onMessageChatRecibed.doCallBack(chatMessage);
                    break;
                case MESSAGE_TYPE_CANCELADO_X_BASE:
                    if (onMessageRecibed != null)
                        onMessageRecibed.doCallBack(null, MESSAGE_TYPE_CANCELADO_X_CONDUCTOR);
                    else {
                        MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_CANCELADO_X_CONDUCTOR);
                        getRingtonNotification(this, RingtoneManager.TYPE_RINGTONE);
                    }
                    startNotification(getString(R.string.conductoresCancelo), this);
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showManageRequest(Map<String, String> mensaje){
        Viaje viaje = mappearVaije(mensaje);
        if (!MyApplication.getInstance().isConductorEnViaje()) {
            if (MyApplication.getInstance().getViajesPendientesChofer() == null) {
                List<Viaje> viajesPedientesChofer = new ArrayList<>();
                MyApplication.getInstance().setViajesPendientesChofer(viajesPedientesChofer);
            }
            MyApplication.getInstance().getViajesPendientesChofer().add(viaje);
            if (!MyApplication.getInstance().isConductorInRequest()) {
                Intent alarmIntent = new Intent(this, GestionarSolicitudActivity.class);
                alarmIntent.putExtra(GestionarSolicitudActivity.EXTRA_VIAJE, MyApplication.getInstance().getViajesPendientesChofer().get(0));
                alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
                MyApplication.getInstance().setConductorInRequest(true);
                startActivity(alarmIntent);
            }
        }
    }

    private ChatMessage mappearMensajeChat(Map<String, String> mensaje){

        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setMessage(mensaje.get(Constantes.MENSAJE_CHAT));
        chatMessage.setMe(false);
        try {
            chatMessage.setDate(Formatter.convertToDateTimeDjango(mensaje.get(Constantes.FECHA_CHAT)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return chatMessage;
    }

    private Viaje mappearVaije(Map<String, String> mensaje){
        Viaje viaje;
        String jsonViaje = mensaje.get(Constantes.VIAJE);
        Gson gson = new Gson();
        viaje = gson.fromJson(jsonViaje, Viaje.class);
        if(viaje != null) {
            Tarifa tarifa = null;
            String jsonTarifa = mensaje.get(Constantes.TARIFA);
            if (!jsonTarifa.equals(""))
                tarifa = gson.fromJson(jsonTarifa, Tarifa.class);
            else
                Log.e("tarifa: ", "tarifa vacia");
            Movil movil = null;
            String jsonMovil = mensaje.get(Constantes.MOVIL);
            if (!jsonMovil.equals("S/M") && !jsonMovil.equals(""))
                movil = gson.fromJson(jsonMovil, Movil.class);
            viaje.setTarifa(tarifa);
            viaje.setMovil(movil);
        }
        return viaje;
    }

    public interface OnMessageRecibed{
        void doCallBack(Object o, String messajeFirebase);
    }

    public interface OnMessageChatRecibed{
        void doCallBack(ChatMessage chatMessage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public static OnMessageRecibed getOnMessageRecibed() {
        return onMessageRecibed;
    }

    public static void setOnMessageRecibed(OnMessageRecibed onMessageRecibed) {
        MyFirebaseMessagingService.onMessageRecibed = onMessageRecibed;
    }

    public static void setOnMessageChatRecibed(OnMessageChatRecibed onMessageChatRecibed) {
        MyFirebaseMessagingService.onMessageChatRecibed = onMessageChatRecibed;
    }
}
