package com.system.sumo.retaxi.dto.mapping;

/**
 * Created by usuario on 19/09/2016.
 */
public class UsuarioMapping {
    public static String TABLA = "usuario";
    public static String ID = "id";
    public static String NOMBRE = "nombre";
    public static String APELLIDO = "apellido";
    public static String EMAIL = "email";
    public static String DNI = "dni";
    public static String URL_IMAGEN = "url_imagen";
    public static String USER_TYPE = "user_type";
    public static String REPUTATION = "reputation";
}
