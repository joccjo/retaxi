package com.system.sumo.retaxi.view.pasajero.viajesProgramados;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.RecyclerTouchListener;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.TravelWs;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TravelsProgrammedFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TravelsProgrammedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TravelsProgrammedFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerViewTravelsProgrammed;
    private OnFragmentInteractionListener mListener;
    private List<Viaje> travels;
    private TravelsProgrammedAdapter travelsProgrammedAdapter;
    private ViajeBo viajeBo = new ViajeBo();

    public TravelsProgrammedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TravelsProgrammedFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TravelsProgrammedFragment newInstance(String param1, String param2) {
        TravelsProgrammedFragment fragment = new TravelsProgrammedFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_travels_programmed, container, false);
        initComponents(view);
        loadData();
        return view;
    }

    private void initComponents(View view) {
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.back_button);
        TextView textView = toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.travelsProgrammedTittle);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        recyclerViewTravelsProgrammed = view.findViewById(R.id.recyclerViewTravelsProgrammed);
        recyclerViewTravelsProgrammed.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewTravelsProgrammed.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewTravelsProgrammed, new RecyclerTouchListener.ClickListener(){

            @Override
            public void onClick(View view, int position) {
                onItemClick(view, position);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void onItemClick(View view, final int position) {
        SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.cancelTravelProgrammed), new SimpleDialogFragment.OkCancelListener() {
            @Override
            public void onCancelSelected() {

            }

            @Override
            public void onOkSelected() {
                cancelTravelInServer(position);
            }
        }).show(getFragmentManager(), "");
    }

    private void cancelTravelInServer(final int position) {
        final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.getTravelsService));
        progressDialogFragment.show(getFragmentManager(), "");

        TravelWs travelWs = new TravelWs();
        travelWs.cancelTravel(travels.get(position).getId(), new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogFragment.dismissAllowingStateLoss();
                Toast.makeText(getActivity(), getString(R.string.viajeCancelado), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onErrorResult() {
                progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.getTravelsServiceError), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        cancelTravelInServer(position);
                    }
                }).show(getFragmentManager(), "");
            }
        });
    }

    private void loadData() {
        final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.getTravelsService));
        progressDialogFragment.show(getFragmentManager(), "");
        TravelWs travelWs = new TravelWs();
        travelWs.getDeferredTravels(new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogFragment.dismissAllowingStateLoss();
                travels = (List<Viaje>) result;
                travelsProgrammedAdapter = new TravelsProgrammedAdapter(travels);
                recyclerViewTravelsProgrammed.setAdapter(travelsProgrammedAdapter);
            }

            @Override
            public void onErrorResult() {
                progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.getTravelsServiceError), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        loadData();
                    }
                }).show(getFragmentManager(), "");
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
