package com.system.sumo.retaxi.view.dialogs.generics;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.system.sumo.retaxi.R;

public class InputDialogFragmentBar extends BaseDialogFragment{
    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_DESCRIPTION = "extra_description";
    FinalizarListener listener;
    TextView titleText;
    TextView description;

    public static InputDialogFragmentBar newInstance(String title, String description ,FinalizarListener listener){
        InputDialogFragmentBar dialogFragment = new InputDialogFragmentBar();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_TITLE, title);
        bundle.putString(EXTRA_DESCRIPTION, description);
        dialogFragment.setArguments(bundle);
        dialogFragment.listener = listener;
        return dialogFragment;
    }

    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.lyt_input_dialog_fragment_bar, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        return alertDialogBuilder.show();
    }

    private void initComponents(View view){
        final EditText nombreTarifa = view.findViewById(R.id.etInput);
        TextView titleText = view.findViewById(R.id.titleText);
        titleText.setText(getArguments().getString(EXTRA_TITLE));
        TextView description = view.findViewById(R.id.descriptionText);
        description.setText(getArguments().getString(EXTRA_DESCRIPTION));

        ImageView beCreateRoom = view.findViewById(R.id.btnOk);
        beCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.finalizarListener(nombreTarifa.getText().toString());
                dismiss();
            }
        });
    }

    public interface DialogListener {
    }

    public interface FinalizarListener extends DialogListener {
        void finalizarListener(String inputText);
    }
}
