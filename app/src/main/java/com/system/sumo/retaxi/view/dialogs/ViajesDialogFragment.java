package com.system.sumo.retaxi.view.dialogs;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.travels.ViajesArrayAdapter;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.TravelWs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by eMa on 29/11/2016.
 */

public class ViajesDialogFragment extends Fragment {
    private List<Viaje> viajes;
    private List<Viaje> filteredTravels = new ArrayList<>();
    private ListView listViewViajes;
    private ViajesArrayAdapter adapter;
    private TextView txtFechaDesde;
    private TextView txtFechaHasta;
    private Date fechaDesde;
    private Date fechaHasta;
    private static final int DIALOG_FECHA_DESDE = 0;
    private static final int DIALOG_FECHA_HASTA = 1;
    static ViajeSelected viajeSelected;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_viajes_tab, container, false);
        initComponents(view);
        initVar();
        loadViajes();
        return view;
    }

    public static ViajesDialogFragment newInstance(Bundle b, ViajeSelected listener){
        ViajesDialogFragment viajesDialogFragment = new ViajesDialogFragment();
        viajeSelected = listener;
        viajesDialogFragment.setArguments(b);
        return viajesDialogFragment;
    }

    /*@Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.viajes);
        View view = View.inflate(getActivity(), R.layout.fragment_viajes_tab, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        initVar();
        loadViajes();
        return alertDialogBuilder.show();
    }
*/
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void filterTravels(){
        try {
            filteredTravels.clear();
            filteredTravels.addAll(ViajeBo.filterByDate(viajes, txtFechaDesde.getText().toString(), txtFechaHasta.getText().toString()));
            //adapter.setViajes(filteredTravels);

            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadViajes(){
        final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.getTravelsService));
        progressDialogFragment.show(getFragmentManager(), "");
        TravelWs travelWs = new TravelWs();
        travelWs.getFinishedTravels(new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogFragment.dismissAllowingStateLoss();
                viajes = (List<Viaje>) result;

                if(viajes != null) {
                    filterTravels();
                }
            }

            @Override
            public void onErrorResult() {
                progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.getTravelsServiceError), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        loadViajes();
                    }
                }).show(getFragmentManager(), "");
            }
        });
    }

    private void initComponents(View view){
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.back_button);
        TextView textView = toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.viajes);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        this.txtFechaDesde = (TextView) view.findViewById(R.id.txtFechaDesde);
        this.txtFechaDesde.setKeyListener(null);
        this.txtFechaDesde.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_FECHA_DESDE);
            }
        });

        //txtFechaHasta
        this.txtFechaHasta = view.findViewById(R.id.txtFechaHasta);
        this.txtFechaHasta.setKeyListener(null);
        this.txtFechaHasta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_FECHA_HASTA);
            }
        });
        listViewViajes = view.findViewById(R.id.listView);
        listViewViajes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.repetirViaje), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        if (ViajeBo.existActiveTravel())
                            SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.activeTravel) ,getString(R.string.ups), new SimpleDialogFragment.OkListener() {
                                @Override
                                public void onOkSelected() {

                                }
                            }).show(getFragmentManager(),  "");
                        else
                            viajeSelected.onViajeSelected(viajes.get(i));
                        //dismiss();
                                /*PedirTaxiFragment pedirTaxiFragment = PedirTaxiFragment.newInstance(viajes.get(i));
                                getFragmentManager().beginTransaction().replace(R.id.content_frame, pedirTaxiFragment).commit();*/
                    }
                }).show(getFragmentManager(), "");
            }
        });
    }

    private void initVar(){
        Calendar calendar = Calendar.getInstance();

        this.fechaHasta = calendar.getTime();
        calendar.add(Calendar.MONTH, -1);
        this.fechaDesde = calendar.getTime();

        this.txtFechaDesde.setText(Formatter.formatDate(fechaDesde));
        this.txtFechaHasta.setText(Formatter.formatDate(fechaHasta));
        adapter = new ViajesArrayAdapter(getActivity(), R.layout.listview_item_viajes, filteredTravels);
        listViewViajes.setAdapter(adapter);
    }

    private void getDialogSeleccionarFecha(final int tipoFecha){
        Date fecha;
        if (tipoFecha == DIALOG_FECHA_DESDE)
            fecha = this.fechaDesde;
        else
            fecha = this.fechaHasta;

        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialogFecha = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year, monthOfYear, dayOfMonth);

                if (tipoFecha == DIALOG_FECHA_DESDE){
                    fechaDesde = cal.getTime();
                    String fechaDesdeString = Formatter.formatDate(fechaDesde);
                    txtFechaDesde.setText(fechaDesdeString);
                }else{
                    fechaHasta = cal.getTime();
                    String fechaHastaString = Formatter.formatDate(fechaHasta);
                    txtFechaHasta.setText(fechaHastaString);
                }
                filterTravels();
            }
        }, year, month, day);

        //_ventas = ventaBo.recoveryVentas(this.fechaDesde, this.fechaHasta);

        dialogFecha.show();
    }

    public interface ViajeSelected {
        void onViajeSelected(Viaje viaje);
    }
}