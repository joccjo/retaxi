package com.system.sumo.retaxi.utils;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_CONVER_TOKEN;

/**
 * Created by eMa on 28/5/2017.
 */

public class RestCustomerVolley {

    public static void addQueue(int method, String url, JSONObject body, boolean withAccessTokenBackend, OnRequestRestCustomerFinish onRequestRestCustomerFinish) {
        addQueue(method, url, body, withAccessTokenBackend, onRequestRestCustomerFinish, null, null);
    }

    public static void addArrayRequesr(int method, String url, JSONArray body, final boolean withAccessTokenBackend, final OnJsonArrayRestCustomerFinish onJsonArrayRestCustomerFinish){
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(method, url, body, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                if (onJsonArrayRestCustomerFinish != null)
                    onJsonArrayRestCustomerFinish.onOkRestCustomerResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onJsonArrayRestCustomerFinish.onErrorRestCustomerResult(error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                if(withAccessTokenBackend)
                    headers.put("Authorization", "Token " + MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth());
                return headers;
            }
        };
        MyApplication.getInstance().getQueue().add(jsonArrayRequest);
    }

    public static void addQueue(int method, String url, JSONObject body, final boolean withAccessTokenBackend,final OnRequestRestCustomerFinish onRequestRestCustomerFinish, final OnMessageSendResult onMessageSendResult, final ChatMessage chatMessage) {
        Log.d("URL REQUEST", url);
        if (body != null) {
            Log.d("BODY REQUEST", body.toString());
        }
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (method, url, body, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(onMessageSendResult != null){
                            if (response != null) {
                                onMessageSendResult.onOkMessageSend(response.toString(), chatMessage);
                            } else {
                                onMessageSendResult.onOErrorMessageSend(chatMessage);
                            }
                        }else if(onRequestRestCustomerFinish != null){
                            if (response != null) {
                                onRequestRestCustomerFinish.onOkRestCustomerResult(response.toString());
                            } else {
                                onRequestRestCustomerFinish.onErrorRestCustomerResult("Error desconocido");
                            }
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(onMessageSendResult != null){
                            onMessageSendResult.onOErrorMessageSend(chatMessage);
                        }else if(onRequestRestCustomerFinish != null){
                            onRequestRestCustomerFinish.onErrorRestCustomerResult(error.getMessage());
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json");
                if(withAccessTokenBackend) {
                    String token = "";
                    if (MyApplication.getInstance().getPreferences().getUsuario() != null)
                        token = MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth();
                    else
                        token = MyApplication.getInstance().getPreferences().getAccessTokenBackend();
                    headers.put("Authorization", "Token " + token);
                }
                return headers;
            }
        };
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().getQueue().add(jsObjRequest);
    }

    public interface OnMessageSendResult{
        void onOkMessageSend(String object, ChatMessage chatMessage);
        void onOErrorMessageSend(ChatMessage chatMessage);
    }

    public interface OnRequestRestCustomerFinish {
        void onOkRestCustomerResult(String object);

        void onErrorRestCustomerResult(String result);
    }

    public interface OnJsonArrayRestCustomerFinish {
        void onOkRestCustomerResult(JSONArray object);

        void onErrorRestCustomerResult(String result);
    }
}
