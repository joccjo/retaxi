package com.system.sumo.retaxi.bo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.system.sumo.retaxi.dto.mapping.ChatMessageMapping;
import com.system.sumo.retaxi.dto.mapping.MovimientoMapping;
import com.system.sumo.retaxi.dto.mapping.UsuarioMapping;
import com.system.sumo.retaxi.dto.mapping.ViajeMapping;
import com.system.sumo.retaxi.dto.MyApplication;

/**
 * Created by usuario on 19/09/2016.
 */
public class BaseDeDatosBo extends SQLiteOpenHelper {
    private static String DATABASE_NAME = "retaxiDb";
    private static int DATABASE_VERSION = 10;
    private static SQLiteDatabase _sqLiteDatabase;
    private static String dataBasePath;

    private static final String SQL_CREAR_MOVIMIENTO  = "create table "
            + MovimientoMapping.TABLA
            + "("
            + MovimientoMapping.ID + " integer primary key autoincrement, "
            + MovimientoMapping.TABLA_MOVIMIENTO + " varchar not null, "
            + MovimientoMapping.IS_ENVIADO + " boolean not null"
            + " );";

    private static final String SQL_CREAR_CHAT_MESSAGE  = "create table "
            + ChatMessageMapping.TABLA
            + "("
            + ChatMessageMapping.ID + " integer primary key autoincrement, "
            + ChatMessageMapping.DATE_TIME + " date, "
            + ChatMessageMapping.MESSAGE + " varchar, "
            + ChatMessageMapping.TYPE + " varchar, "
            + ChatMessageMapping.IS_ENVIADO + " boolean not null, "
            + ChatMessageMapping.IS_NEW + " boolean not null, "
            + ChatMessageMapping.IS_ME + " boolean not null"
            + " );";

    private static final String SQL_CREAR_USUARIO  = "create table "
            + UsuarioMapping.TABLA
            + "("
            + UsuarioMapping.ID + " integer, "
            + UsuarioMapping.USER_TYPE + " integer, "
            + UsuarioMapping.APELLIDO + " varchar, "
            + UsuarioMapping.DNI + " long, "
            + UsuarioMapping.EMAIL + " varchar, "
            + UsuarioMapping.NOMBRE + " varchar, "
            + UsuarioMapping.URL_IMAGEN + " varchar, "
            + UsuarioMapping.REPUTATION + " decimal, "
            + " PRIMARY KEY " + "(" + UsuarioMapping.ID + "," + UsuarioMapping.USER_TYPE + ") "
            + " );";

    private static final String SQL_CREAR_VIAJE  = "create table "
            + ViajeMapping.TABLA
            + "("
            + ViajeMapping.ID + " integer primary key autoincrement, "
            + ViajeMapping.DESDE + " varchar, "
            + ViajeMapping.HASTA + " varchar, "
            + ViajeMapping.FECHA + " date, "
            + ViajeMapping.LATITUD_DESDE + " double, "
            + ViajeMapping.LATITUD_HASTA + " double, "
            + ViajeMapping.LONGITUD_DESDE + " double, "
            + ViajeMapping.LONGITUD_HASTA + " double, "
            + ViajeMapping.TIPE + " varchar, "
            + ViajeMapping.MOTNO + " decimal, "
            + ViajeMapping.USER_TYPE + " integer "
            + " );";

    private static final String DATABASE_ALTER_TEAM_1 = "ALTER TABLE " + UsuarioMapping.TABLA + " ADD COLUMN " + UsuarioMapping.REPUTATION + " string;";

    public BaseDeDatosBo(){
        super(MyApplication.getInstance().getApplicationContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREAR_MOVIMIENTO);
        db.execSQL(SQL_CREAR_USUARIO);
        db.execSQL(SQL_CREAR_VIAJE);
        db.execSQL(SQL_CREAR_CHAT_MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 11) {
            //db.execSQL(DATABASE_ALTER_TEAM_1);
            db.execSQL("DROP TABLE IF EXISTS " + ViajeMapping.TABLA);
            db.execSQL(SQL_CREAR_CHAT_MESSAGE);
        }
    }

    public SQLiteDatabase open(){
        return this.getReadableDatabase();
    }
}
