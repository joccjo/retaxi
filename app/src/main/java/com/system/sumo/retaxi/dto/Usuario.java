package com.system.sumo.retaxi.dto;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.system.sumo.retaxi.utils.Constantes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 21/08/2016.
 */
public class Usuario implements Serializable{
    private static double [] ciudadorig =  {-27.45, -58.9833};
    public static final String KEY_USUARIO_PREFERENCES = "usuario_preferences";
    public static LocationPoint ciudadOrigen = new LocationPoint("point", ciudadorig);
    private int id;
    public static String KEY_EXTRA_USUARIO = "extra_usuario";
    public static int TIPO_PASAJERO = 1;
    public static int TIPO_CONDUCTOR = 2;
    public static final String SEX_MAN = "M";
    public static final String SEX_WOMAN = "F";
    @SerializedName(Constantes.FIRST_NAME)
    private String nombre;
    @SerializedName(Constantes.LAST_NAME)
    private String apellido;
    private String birthday;
    private String sex;
    private String telephone;
    private int tipoUsuario;
    private int dni;
    private int tokenFCM;
    private String email;
    @SerializedName(Constantes.SOCIAL_THUMB)
    private String urlImagen;
    @SerializedName(Constantes.TOKEN)
    private String tokenOauth;
    @SerializedName(Constantes.REPUTACION)
    private float reputation;
    private int viajes_realizados;
    //private double latitude;
    //private double longitude;"last_location":{"coordinates":[0.0,0.0],"type":"Point"},
    @SerializedName(Constantes.MOVIL)
    List<Movil> moviles;
    @SerializedName(Constantes.ULTIMA_UBICACION)
    private LocationPoint lastLocationLocationPoint;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static String getKeyExtraUsuario() {
        return KEY_EXTRA_USUARIO;
    }

    public static void setKeyExtraUsuario(String keyExtraUsuario) {
        KEY_EXTRA_USUARIO = keyExtraUsuario;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public int getTokenFCM() {
        return tokenFCM;
    }

    public void setTokenFCM(int tokenFCM) {
        this.tokenFCM = tokenFCM;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public String getTokenOauth() {
        return tokenOauth;
    }

    public void setTokenOauth(String tokenOauth) {
        this.tokenOauth = tokenOauth;
    }

    public float getReputation() {
        return reputation;
    }

    public void setReputation(float reputation) {
        this.reputation = reputation;
    }

    public List<Movil> getMoviles() {
        if(moviles == null)
            moviles = new ArrayList<>();
        return moviles;
    }

    public void setMoviles(List<Movil> moviles) {
        this.moviles = moviles;
    }

    /*public String getLastLocationString() {
        return lastLocationString;
    }

    public void setLastLocationString(String lastLocationString) {
        this.lastLocationString = lastLocationString;
    }*/

    public LatLng getLastLocation() {
        //double[] punto = Formatter.formatStringToDouble(lastLocationString);
        return new LatLng(lastLocationLocationPoint.getCoordinates()[0], lastLocationLocationPoint.getCoordinates()[1]);
    }

    public LocationPoint getLastLocationLocationPoint() {
        return lastLocationLocationPoint;
    }

    public void setLastLocationLocationPoint(LocationPoint lastLocationLocationPoint) {
        this.lastLocationLocationPoint = lastLocationLocationPoint;
    }

    public int getViajes_realizados() {
        return viajes_realizados;
    }

    public void setViajes_realizados(int viajes_realizados) {
        this.viajes_realizados = viajes_realizados;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
}
