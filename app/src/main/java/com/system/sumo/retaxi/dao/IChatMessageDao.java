package com.system.sumo.retaxi.dao;

import android.content.Context;

import com.system.sumo.retaxi.dto.ChatMessage;

import java.util.List;

/**
 * Created by eMa on 2/1/2017.
 */

public interface IChatMessageDao extends IGenericDao<ChatMessage, Integer> {
    void deletAll() throws Exception;
    public List<ChatMessage> recoveryNotSended() throws Exception;

    void updateNewMessages() throws Exception;

    void updateSendedMessage(long id) throws Exception;
    long getNumberUnSeenMessages() throws Exception;

    List<ChatMessage> getUnSeenMessages() throws Exception;
}
