package com.system.sumo.retaxi.asynkTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.toolbox.HttpClientStack;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.utils.Constantes;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * Created by usuario on 30/09/2016.
 */

public class RestCustomer extends AsyncTask<Void, Void, String> {
    String url;
    Context context;
    //OnRequestFinish onRequestFinish;
    String result;
    JSONObject body;

    String metodo;
    String header;
    OnRequestRestCustomerFinish onRequestRestCustomerFinish;
    OnMessageSendResult onMessageSendResult;
    ChatMessage chatMessage;

    public RestCustomer(Context context, String url, JSONObject body, String metodo, OnRequestRestCustomerFinish onRequestRestCustomerFinish) {
        this.context = context;
        this.url = url;
        this.body = body;
        this.metodo = metodo;
        this.onRequestRestCustomerFinish = onRequestRestCustomerFinish;
    }

    public RestCustomer(Context context, String url, JSONObject body, String metodo, String header, OnRequestRestCustomerFinish onRequestRestCustomerFinish) {
        this.context = context;
        this.url = url;
        this.body = body;
        this.metodo = metodo;
        this.header = header;
        this.onRequestRestCustomerFinish = onRequestRestCustomerFinish;
    }

    public RestCustomer(Context context, String url, ChatMessage chatMessage, JSONObject body, String metodo, String header, OnRequestRestCustomerFinish onRequestRestCustomerFinish) {
        this.context = context;
        this.url = url;
        this.body = body;
        this.metodo = metodo;
        this.header = header;
        this.onRequestRestCustomerFinish = onRequestRestCustomerFinish;
        this.chatMessage = chatMessage;
    }

    public RestCustomer(Context context, String url, ChatMessage chatMessage, JSONObject body, String metodo, String header, OnMessageSendResult onMessageSendResult) {
        this.context = context;
        this.url = url;
        this.body = body;
        this.metodo = metodo;
        this.header = header;
        this.onMessageSendResult = onMessageSendResult;
        this.chatMessage = chatMessage;
    }

    /*public RestCustomer(Context context, String url, Class objeto, String codigoAuth, JSONObject body, String metodo){
        this.context = context;
        this.url = url;
        this.objeto = objeto;
        this.codigoAuth = codigoAuth;
        this.body = body;
    }*/

    @Override
    protected String doInBackground(Void... params) {
        String objetoRespuesta = null;
        try {
            // set the connection timeout value to 30 seconds (30000 milliseconds)
            final HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 7000);
            HttpClient hc = new DefaultHttpClient(httpParams);
            String message = "";
            if(body != null)
                message = body.toString();

            HttpUriRequest request = null;
            switch (metodo) {
                case Constantes.POST:
                    HttpPost httpPost = new HttpPost(url.toString());
                    httpPost.setEntity(new StringEntity(message, "UTF8"));
                    httpPost.setHeader("Content-type", "application/json");
                    if (header != null) {
                        httpPost.addHeader("Authorization", "Token " + header);
                    }
                    request = httpPost;
                    break;
                case Constantes.GET:
                    HttpGet httpGet = new HttpGet(url.toString());
                    //httpGet.setEntity(new StringEntity(message, "UTF8"));
                    httpGet.setHeader("Content-type", "application/json");
                    request = new HttpGet(url.toString());
                    break;
                case Constantes.PATCH:
                    HttpClientStack.HttpPatch httpPatch = new HttpClientStack.HttpPatch(url.toString());
                    httpPatch.setEntity(new StringEntity(message, "UTF8"));
                    httpPatch.setHeader("Content-type", "application/json");
                    if (header != null) {
                        httpPatch.addHeader("Authorization", "Token " + header);
                    }
                    request = httpPatch;
            }


            //p.setEntity(new StringEntity(message, "UTF8"));
            //p.setHeader("Content-type", "application/json");
            HttpResponse resp = hc.execute(request);
            if (resp != null) {
                if (resp.getStatusLine().getStatusCode() == 204) {
                    result = resp.getStatusLine().toString();
                }
            }
            objetoRespuesta = EntityUtils.toString(resp.getEntity());
            Log.d("Status line", "" + resp.getStatusLine().getStatusCode());
            Log.d("Status line", "" + resp.getStatusLine().toString());

        } catch (Exception e) {
            Log.e("RestCustomer: ", e.getMessage(), e);
            result = e.getMessage();
        }

        return objetoRespuesta;
    }

    @Override
    protected void onPostExecute(String object) {
        if(onMessageSendResult != null){
            if (object != null) {
                onMessageSendResult.onOkMessageSend(object, chatMessage);
            } else {
                onMessageSendResult.onOErrorMessageSend(chatMessage);
            }
        }else if(onRequestRestCustomerFinish != null){
                if (object != null) {
                    onRequestRestCustomerFinish.onOkRestCustomerResult(object);
                } else {
                    onRequestRestCustomerFinish.onErrorRestCustomerResult(result);
                }
        }
        //OnRequestRestCustomerFinish onRequestFinish = (OnRequestRestCustomerFinish) this.context;

    }

    public interface OnRequestRestCustomerFinish {
        void onOkRestCustomerResult(String object);

        void onErrorRestCustomerResult(String result);
    }

    public interface OnMessageSendResult{
        void onOkMessageSend(String object, ChatMessage chatMessage);
        void onOErrorMessageSend(ChatMessage chatMessage);
    }

}
