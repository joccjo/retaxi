package com.system.sumo.retaxi.dto;

import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.LocationRequest;
import com.system.sumo.retaxi.BuildConfig;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.utils.VolleySingleton;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;


/**
 * Created by mansha on 30/10/15.
 */
public class MyApplication extends MultiDexApplication {

    public static final String TAG = MyApplication.class.getSimpleName();
    public static VolleySingleton volleyQueueInstance;
    private RequestQueue queue;
    private static MyApplication mInstance;
    //private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private Preferences preferences;
    //private GoogleApiClient mGoogleApiClient;
    private int movilSelected;
    private List<ChatMessage> chatMessages;
    private int userType;

    /*
    * se usa para saber si el chofer ya presiono el boton conectar
     */
    private boolean isChoferConectado = false;
    /*
    * Si el conductor esta en un viaje no se le dara otra solicitud
     */
    private boolean isConductorEnViaje = false;
    /*
    * Si el conductor esta conu una peticion en pantalla no se le dara otra
     */
    private boolean isConductorInRequest = false;
    LocationRequest locationRequest;
    //Viaje viaje;
    List<Viaje> viajesPendientesChofer;

    /*
    * Cuando la aplicacion queda en segundo plano y se reciben mensajes de firebase es necesario almacenar la infor para despues moder mostrarla
    * */
    private String firebaseMessajeType;
    private LocationPoint lastLocationFromConductorRecibed;
    private GoogleApiLocationHelper googleApiHelper;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        googleApiHelper = new GoogleApiLocationHelper(mInstance);
        instantiateVolleyQueue();
        loadPreferences();
    }

    public void loadPreferences() {
        preferences = PreferencesBo.loadPreferences();
    }

    public void instantiateVolleyQueue() {
        volleyQueueInstance = VolleySingleton.getInstance(getApplicationContext());
        queue = Volley.newRequestQueue(this/*, getHurlStack()*/);
    }

    public RequestQueue getQueue() {
        if(queue == null)
            instantiateVolleyQueue();
        return queue;
    }

    /*private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return hostname.equals(getString(R.string.ws_ip)) || verify(hostname, session);
            }
        };
    }*/

    private SSLSocketFactory getSSLSocketFactory()
            throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException, KeyManagementException {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        InputStream caInput = getResources().openRawResource(R.raw.aws); // this cert file stored in \app\src\main\res\raw folder path

        Certificate ca = cf.generateCertificate(caInput);
        caInput.close();

        KeyStore keyStore = KeyStore.getInstance("BKS");
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

        TrustManager[] wrappedTrustManagers = getWrappedTrustManagers(tmf.getTrustManagers());

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, wrappedTrustManagers, null);

        return sslContext.getSocketFactory();
    }

    private TrustManager[] getWrappedTrustManagers(TrustManager[] trustManagers) {
        final X509TrustManager originalTrustManager = (X509TrustManager) trustManagers[0];
        return new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return originalTrustManager.getAcceptedIssuers();
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkClientTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            e.printStackTrace();
                        }
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        try {
                            if (certs != null && certs.length > 0){
                                certs[0].checkValidity();
                            } else {
                                originalTrustManager.checkServerTrusted(certs, authType);
                            }
                        } catch (CertificateException e) {
                            e.printStackTrace();
                        }
                    }
                }
        };
    }

    /*public HurlStack getHurlStack() {
        HurlStack hurlStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(getSSLSocketFactory());
                    httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }
        };

        return hurlStack;
    }*/

    public void setQueue(RequestQueue queue) {
        this.queue = queue;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public void showGenericToast(String error) {
        try {
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public GoogleApiLocationHelper getGoogleApiHelperInstance() {
        return this.googleApiHelper;
    }
    public static GoogleApiLocationHelper getGoogleApiHelper() {
        return getInstance().getGoogleApiHelperInstance();
    }

    public LocationRequest getLocationRequest() {
        return locationRequest;
    }

    public void setLocationRequest(LocationRequest locationRequest) {
        this.locationRequest = locationRequest;
    }

    public boolean isChoferConectado() {
        return isChoferConectado;
    }

    public void setChoferConectado(boolean choferConectado) {
        isChoferConectado = choferConectado;
    }

    public boolean isConductorEnViaje() {
        return isConductorEnViaje;
    }

    public void setConductorEnViaje(boolean conductorEnViaje) {
        isConductorEnViaje = conductorEnViaje;
    }

    public boolean isConductorInRequest() {
        return isConductorInRequest;
    }

    public void setConductorInRequest(boolean conductorInRequest) {
        isConductorInRequest = conductorInRequest;
    }

    public Movil getMovilSelected() {
        return preferences.getUsuario().getMoviles().get(movilSelected);
    }

    public void setMovilSelected(int movilSelected) {
        this.movilSelected = movilSelected;
    }

    public String getFirebaseMessajeType() {
        if(firebaseMessajeType == null)
            return "";
        return firebaseMessajeType;
    }

    public void setFirebaseMessajeType(String firebaseMessajeType) {
        this.firebaseMessajeType = firebaseMessajeType;
    }

    public LocationPoint getLastLocationFromConductorRecibed() {
        return lastLocationFromConductorRecibed;
    }

    public void setLastLocationFromConductorRecibed(LocationPoint lastLocationFromConductorRecibed) {
        this.lastLocationFromConductorRecibed = lastLocationFromConductorRecibed;
    }

    /*public Viaje getViaje() {
        return viaje;
    }

    public void setViaje(Viaje viaje) {
        this.viaje = viaje;
    }*/

    public List<Viaje> getViajesPendientesChofer() {
        return viajesPendientesChofer;
    }

    public void setViajesPendientesChofer(List<Viaje> viajesPendientesChofer) {
        this.viajesPendientesChofer = viajesPendientesChofer;
    }

    public List<ChatMessage> getChatMessages() {
        if(chatMessages == null)
            chatMessages = new ArrayList<>();
        return chatMessages;
    }

    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public static int getUserType() {
        if(BuildConfig.APPLICATION_ID.contains("driver"))
            return Usuario.TIPO_CONDUCTOR;
            else
                return Usuario.TIPO_PASAJERO;
    }
}
