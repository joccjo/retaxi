package com.system.sumo.retaxi.dto;

import org.json.JSONObject;

/**
 * Created by usuario on 25/10/2016.
 */

public class ResultWsCall {
    int respuesta;
    String data;
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(int respuesta) {
        this.respuesta = respuesta;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
