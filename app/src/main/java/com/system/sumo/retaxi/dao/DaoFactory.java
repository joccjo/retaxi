package com.system.sumo.retaxi.dao;

/**
 * Created by usuario on 19/09/2016.
 */
public abstract class DaoFactory {
    public static final int SQLITE = 1;
    public static final int HIBERNATE = 2;
    public static final int TOPLINK = 3;

    public static DaoFactory getDaoFactory(int whichFactory){
        switch(whichFactory){
            case SQLITE: return new SqliteDaoFactory();
            default:return null;
        }
    }

    public abstract void beginTransaction() throws Exception;
    public abstract void rollback();
    public abstract void commit() throws Exception;
    public abstract void closeConnection() throws Exception;

    public abstract IUsuarioDao getUsuarioDao();
    public abstract IViajeDao getViajeDao();
    public abstract IChatMessageDao chatMessageDao();
}
