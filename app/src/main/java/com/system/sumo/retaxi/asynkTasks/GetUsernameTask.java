package com.system.sumo.retaxi.asynkTasks;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

import java.io.IOException;

/**
 * Created by usuario on 25/08/2016.
 */
public class GetUsernameTask extends AsyncTask<Void, Void, Void> {
    Activity mActivity;
    String mScope;
    String mEmail;
    ResultadoListener resultadoListener;

    public GetUsernameTask(Activity activity, String name, String scope, ResultadoListener resultadoListener) {
        this.mActivity = activity;
        this.mScope = scope;
        this.mEmail = name;
        this.resultadoListener = resultadoListener;
    }

    /**
     * Executes the asynchronous job. This runs when you call execute()
     * on the AsyncTask instance.
     */
    @Override
    protected Void doInBackground(Void... params) {
        try {
            String token = fetchToken();
            if (token != null) {
                resultadoListener.onDone(token);
            }
        } catch (IOException e) {
            // The fetchToken() method handles Google-specific exceptions,
            // so this indicates something went wrong at a higher level.
            // TIP: Check for network connectivity before starting the AsyncTask.
            resultadoListener.onError(e.getMessage());
        }
        return null;
    }

    /**
     * Gets an authentication token from Google and handles any
     * GoogleAuthException that may occur.
     */
    protected String fetchToken() throws IOException {
        try {
            return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
        } catch (UserRecoverableAuthException userRecoverableException) {

        } catch (GoogleAuthException fatalException) {
            // Some other type of unrecoverable exception has occurred.
            // Report and log the error as appropriate for your app.
        } catch (Exception e){
            String a = e.getMessage();
        }
        return null;
    }


    public interface ResultadoListener {
        void onDone(String token);
        void onError(String error);
    }
}
