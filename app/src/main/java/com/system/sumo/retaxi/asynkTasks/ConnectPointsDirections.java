package com.system.sumo.retaxi.asynkTasks;

import android.content.Context;
import android.os.AsyncTask;

import com.system.sumo.retaxi.utils.JSONParser;

/**
 * Created by usuario on 23/10/2016.
 */

public class ConnectPointsDirections extends AsyncTask<Void, Void, String> {
    //private ProgressDialog progressDialog;
    String url;
    Context context;

    public ConnectPointsDirections(String urlPass, Context context) {
        url = urlPass;
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*progressDialog = new ProgressDialog(PedirTaxiFragment.this);
        progressDialog.setMessage(getString(R.string.dibujandoRuta));
        progressDialog.setIndeterminate(true);
        progressDialog.show();*/
    }

    @Override
    protected String doInBackground(Void... params) {
        JSONParser jParser = new JSONParser();
        String json = jParser.getJSONFromUrl(url);
        return json;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        OnConnectPointsFinish onRequestFinish = (OnConnectPointsFinish) this.context;
        if(result != null){
            onRequestFinish.onConnectPoinsOk(result);
        }else{
            //onRequestFinish.onConnectPoinsOk(result);
        }
        /*progressDialog.hide();
        if (result != null) {
            polyline = mapDraw.drawPath(result, map);
        }*/
    }

    public interface OnConnectPointsFinish{
        void onConnectPoinsOk(String result);
    }
}
