package com.system.sumo.retaxi.view.chat;

import android.app.Activity;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.ChatMessage;

import java.util.List;

import static com.system.sumo.retaxi.utils.Formatter.formatHoursMinutes;

/**
 * Created by eMa on 21/12/2016.
 */

class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatAdapterViewHolder> {

    private final List<ChatMessage> chatMessages;
    private Activity context;

    ChatAdapter(Activity context, List<ChatMessage> chatMessages) {
        this.context = context;
        this.chatMessages = chatMessages;
    }

    @Override
    public ChatAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_chat_message,parent,false);
        return new ChatAdapterViewHolder(v);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (chatMessages != null){
            return chatMessages.size();
        }else{
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(final ChatAdapterViewHolder holder, int position) {
        ChatMessage chatMessage = chatMessages.get(position);
        boolean isFriendMsg = !chatMessage.getIsme() ;//Just a dummy check
        chatMessage.setNew(false);
        //to simulate whether it me or other sender
        setAlignment(holder, isFriendMsg);
        holder.txtMessage.setText(chatMessage.getMessage());
        holder.txtInfo.setText(formatHoursMinutes(chatMessage.getDate()));
        holder.imgIsEnviado.setVisibility(View.VISIBLE);
        if(isFriendMsg){
            holder.imgIsEnviado.setVisibility(View.GONE);
        }else if(chatMessage.isEnviado()){
            holder.imgIsEnviado.setImageResource(R.mipmap.ic_check_black_24dp);
        }else{
            holder.imgIsEnviado.setImageResource(R.mipmap.ic_access_time_black_24dp);
        }
    }

    public void add(ChatMessage message) {
        chatMessages.add(message);
    }

    public void add(List<ChatMessage> messages) {
        chatMessages.addAll(messages);
    }

    private void setAlignment(ChatAdapterViewHolder holder, boolean isMe) {LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        FrameLayout.LayoutParams fl = (FrameLayout.LayoutParams) holder.contentWithBG.getLayoutParams();

        //LinearLayout.LayoutParams llMensaje = (LinearLayout.LayoutParams) holder.tvMessage.getLayoutParams();
        //LinearLayout.LayoutParams llHora = (LinearLayout.LayoutParams) holder.tvHour.getLayoutParams();
        if (!isMe) {
            holder.contentWithBG.setBackgroundResource(R.drawable.in_message_bg);

            fl.gravity = Gravity.RIGHT;
            //holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.content.setLayoutParams(lp);
            //holder.txtMessage.setLayoutParams(layoutParams);

            //holder.txtInfo.setLayoutParams(layoutParams);
        } else {
            holder.contentWithBG.setBackgroundResource(R.drawable.out_message_bg);

            fl.gravity = Gravity.LEFT;
            //holder.contentWithBG.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams lp =
                    (RelativeLayout.LayoutParams) holder.content.getLayoutParams();
            lp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
            lp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            holder.content.setLayoutParams(lp);
        }
    }

    static class ChatAdapterViewHolder extends RecyclerView.ViewHolder{
        TextView txtMessage;
        TextView txtInfo;
        CardView content;
        LinearLayout contentWithBG;
        ImageView imgIsEnviado;

        ChatAdapterViewHolder(View v) {
            super(v);
            txtMessage = v.findViewById(R.id.txtMessage);
            content = v.findViewById(R.id.content);
            contentWithBG = v.findViewById(R.id.contentWithBackground);
            txtInfo = v.findViewById(R.id.txtInfo);
            imgIsEnviado = v.findViewById(R.id.imgIsEnviado);
        }
    }
}
