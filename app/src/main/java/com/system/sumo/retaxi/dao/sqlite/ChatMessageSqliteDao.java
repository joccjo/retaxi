package com.system.sumo.retaxi.dao.sqlite;

import android.content.Context;
import android.database.Cursor;

import com.system.sumo.retaxi.dao.CondicionDao;
import com.system.sumo.retaxi.dao.IChatMessageDao;
import com.system.sumo.retaxi.dao.SqliteDaoFactory;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.mapping.ChatMessageMapping;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.system.sumo.retaxi.utils.Formatter.convertToDateTime;

/**
 * Created by eMa on 2/1/2017.
 */

public class ChatMessageSqliteDao extends GenericSqliteDao implements IChatMessageDao {
    @Override
    public Integer create(ChatMessage entity) throws Exception {
        table = ChatMessageMapping.TABLA;
        insertMap.put(ChatMessageMapping.DATE_TIME, SqliteDaoFactory.formatDateTimeToString(entity.getDate()));
        insertMap.put(ChatMessageMapping.IS_ME, entity.getIsme());
        insertMap.put(ChatMessageMapping.MESSAGE, entity.getMessage());
        insertMap.put(ChatMessageMapping.IS_NEW, entity.isNew());
        insertMap.put(ChatMessageMapping.IS_ENVIADO, entity.isEnviado());
        insertMap.put(ChatMessageMapping.TYPE, entity.getTipo());
        super.create();
        entity.setId(recoveryLastIdOfMessageSaved());
        return null;
    }

    private long recoveryLastIdOfMessageSaved() throws Exception{
        listTables.add(ChatMessageMapping.TABLA);
        listColumns.add("max(" + ChatMessageMapping.ID + ") as " + ChatMessageMapping.ID    );
        Cursor cursor = recoveryData();
        if(cursor.moveToFirst())
            return cursor.getInt(cursor.getColumnIndex(ChatMessageMapping.ID ));
        return 0;
    }

    @Override
    public ChatMessage recoveryByID(Integer integer) throws Exception {
        return null;
    }

    @Override
    public List<ChatMessage> recoveryAll() throws Exception {
        listTables.add(ChatMessageMapping.TABLA);
        Cursor cursor = recoveryData();
        return getChatMessages(cursor);
    }

    @Override
    public List<ChatMessage> recoveryNotSended() throws Exception {
        listTables.add(ChatMessageMapping.TABLA);
        parameterWhere.put(ChatMessageMapping.IS_ENVIADO, new CondicionDao("=", 0));
        parameterWhere.put(ChatMessageMapping.IS_ME, new CondicionDao("=", 1));
        Cursor cursor = recoveryData();
        return getChatMessages(cursor);
    }

    @Override
    public long getNumberUnSeenMessages() throws Exception {
        table = ChatMessageMapping.TABLA;
        parameterWhere.put(ChatMessageMapping.IS_NEW, new CondicionDao("=", 1));
        parameterWhere.put(ChatMessageMapping.IS_ME, new CondicionDao("=", 0));
        return count();
    }

    @Override
    public List<ChatMessage> getUnSeenMessages() throws Exception {
        listTables.add(ChatMessageMapping.TABLA);
        parameterWhere.put(ChatMessageMapping.IS_NEW, new CondicionDao("=", 1));
        parameterWhere.put(ChatMessageMapping.IS_ME, new CondicionDao("=", 0));
        Cursor cursor = recoveryData();
        return getChatMessages(cursor);
    }
    @Override
    public void updateNewMessages() throws Exception {
        table = ChatMessageMapping.TABLA;
        updateMap.put(ChatMessageMapping.IS_NEW, false);
        parameterWhere.put(ChatMessageMapping.IS_NEW, new CondicionDao("=", 1));
        parameterWhere.put(ChatMessageMapping.IS_ME, new CondicionDao("=", 0));
        super.update();
    }

    @Override
    public void updateSendedMessage(long id) throws Exception {
        table = ChatMessageMapping.TABLA;
        updateMap.put(ChatMessageMapping.IS_ENVIADO, true);
        parameterWhere.put(ChatMessageMapping.ID, new CondicionDao("=", id));
        super.update();
    }

    @Override
    public void update(ChatMessage entity) throws Exception {

    }


    @Override
    public void delete(ChatMessage entity) throws Exception {

    }

    @Override
    public void delete(Integer integer) throws Exception {

    }


    public void deletAll() throws Exception {
        table = ChatMessageMapping.TABLA;
        delete();
    }

    private List<ChatMessage> getChatMessages(Cursor cursor){
        List<ChatMessage> chatMessages = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    chatMessages.add(mappingToDto(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return chatMessages;
    }

    public static ChatMessage mappingToDto(Cursor cursor){
        ChatMessage chatMessage = new ChatMessage();
        int indexId = cursor.getColumnIndex(ChatMessageMapping.ID);
        chatMessage.setId(cursor.getInt(indexId));

        int indexMessage = cursor.getColumnIndex(ChatMessageMapping.MESSAGE);
        if (indexMessage >= 0) {
            chatMessage.setMessage(cursor.getString(indexMessage));
        }

        int indexDateTime = cursor.getColumnIndex(ChatMessageMapping.DATE_TIME);
        if (indexDateTime >= 0) {
            String dateTime = cursor.getString(indexDateTime);
            try {
                chatMessage.setDate(SqliteDaoFactory.convertToDateTime(dateTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        int indexEnviado = cursor.getColumnIndex(ChatMessageMapping.IS_ENVIADO);
        if (indexEnviado >= 0) {
            chatMessage.setEnviado((cursor.getInt(indexEnviado) > 0));
        }

        int indexMe = cursor.getColumnIndex(ChatMessageMapping.IS_ME);
        if (indexMe >= 0) {
            chatMessage.setMe((cursor.getInt(indexMe) > 0));
        }

        int indexNew = cursor.getColumnIndex(ChatMessageMapping.IS_NEW);
        if (indexNew >= 0) {
            chatMessage.setNew(cursor.getInt(indexNew) > 0);
        }

        int indexType = cursor.getColumnIndex(ChatMessageMapping.TYPE);
        if (indexType >= 0) {
            chatMessage.setTipo(cursor.getString(indexType));
        }

        return chatMessage;
    }
}
