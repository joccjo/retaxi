package com.system.sumo.retaxi.view.markers;

import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Callback;

/**
 * Created by usuario on 22/09/2016.
 */
public class ImageRefresherMarker implements Callback{
    Marker marker;
    public ImageRefresherMarker(Marker marker){
        this.marker = marker;
    }

    @Override
    public void onSuccess() {
        marker.showInfoWindow();
    }

    @Override
    public void onError(Exception e) {

    }
}
