package com.system.sumo.retaxi.dto;


import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Kyra on 1/11/2016.
 */
public class PlaceAutoComplete {

    private String place_id;
    private String description;
    private LatLng punto;

    public String getPlaceDesc() {
        return description;
    }

    public void setPlaceDesc(String placeDesc) {
        description = placeDesc;
    }

    public String getPlaceID() {
        return place_id;
    }

    public void setPlaceID(String placeID) {
        place_id = placeID;
    }

    public LatLng getPunto() {
        return punto;
    }

    public void setPunto(LatLng punto) {
        this.punto = punto;
    }
}
