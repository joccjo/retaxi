package com.system.sumo.retaxi.dto;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.utils.Constantes;

import java.io.Serializable;
import java.util.Date;

import static com.system.sumo.retaxi.utils.Constantes.TARIFA;
import static com.system.sumo.retaxi.utils.Constantes.IS_DIFERIDO;

/**
 * Created by usuario on 16/08/2016.
 */
public class Viaje implements Serializable {
    public enum TravelTypes {
        DIFERIDO("D");

        private String type;
        TravelTypes(String type) {
            this.type = type;
        }
        public String type() {
            return type;
        }
    }
    public static final String TRAVEL_STATE_PENDING = "P";
    public static final String TRAVEL_STATE_ACCEPTED = "A";
    public static final String TRAVEL_STATE_FINISHED = "F";
    public static final String TRAVEL_STATE_CANCELLED_BY_ME = "CM";
    public static final String TRAVEL_STATE_CANCELLED_BY_COMPANY = "CC";
    public static final String TRAVEL_STATE_STARTED = "I";
    public static final String KEY_VIAJE_PREFERENCES = "viaje_preferences";
    private int id;
    @SerializedName(Constantes.FECHA_HORA)
    private String fechaHora;
    private Date fecha;
    @SerializedName(Constantes.ORIGIN_DESCRIPTION)
    private String desde;
    @SerializedName(Constantes.DESTINATION_DESCRIPTION)
    private String hasta;
    private String monto;
    @SerializedName(Constantes.ORIGIN)
    private LocationPoint puntoDesdeLocationPoint;
    @SerializedName(Constantes.DESTINATION)
    private LocationPoint puntoHastaLocationPoint;
    private String estado = "";
    private String tipo = "";
    private Usuario pasajero;
    private Usuario conductor;
    @SerializedName(TARIFA)
    private Tarifa tarifa;
    private Movil movil;
    @SerializedName(IS_DIFERIDO)
    private boolean diferido;


    /*private void writeObject(ObjectOutputStream out) throws IOException {
        //out.defaultWriteObject();
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        //in.defaultReadObject();
    }*/

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDesde() {
        return desde;
    }

    public void setDesde(String desde) {
        this.desde = desde;
    }

    public String getHasta() {
        return hasta;
    }

    public void setHasta(String hasta) {
        this.hasta = hasta;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getPasajero() {
        return pasajero;
    }

    public void setPasajero(Usuario pasajero) {
        this.pasajero = pasajero;
    }

    public Usuario getConductor() {
        return conductor;
    }

    public void setConductor(Usuario conductor) {
        this.conductor = conductor;
    }

    /*public String getPuntoDesdeString() {
        return puntoDesdeString;
    }

    public void setPuntoDesdeString(String puntoDesdeString) {
        this.puntoDesdeString = puntoDesdeString;
    }

    public String getPuntoHastaString() {
        return puntoHastaString;
    }

    public void setPuntoHastaString(String puntoHastaString) {
        this.puntoHastaString = puntoHastaString;
    }*/

    public LocationPoint getPuntoDesdeLocationPoint() {
        return puntoDesdeLocationPoint;
    }

    public void setPuntoDesdeLocationPoint(LocationPoint puntoDesdeLocationPoint) {
        this.puntoDesdeLocationPoint = puntoDesdeLocationPoint;
    }

    public LocationPoint getPuntoHastaLocationPoint() {
        return puntoHastaLocationPoint;
    }

    public void setPuntoHastaLocationPoint(LocationPoint puntoHastaLocationPoint) {
        this.puntoHastaLocationPoint = puntoHastaLocationPoint;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public Tarifa getTarifa() {
        return tarifa;
    }

    public void setTarifa(Tarifa tarifa) {
        this.tarifa = tarifa;
    }

    public Movil getMovil() {
        return movil;
    }

    public void setMovil(Movil movil) {
        this.movil = movil;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public LatLng getPuntoDesde(){
        return new LatLng(getPuntoDesdeLocationPoint().getCoordinates()[0], getPuntoDesdeLocationPoint().getCoordinates()[1]);
    }
    public LatLng getPuntoHasta(){
        return new LatLng(getPuntoHastaLocationPoint().getCoordinates()[0], getPuntoHastaLocationPoint().getCoordinates()[1]);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isDiferido() {
        return diferido;
    }

    public void setDiferido(boolean diferido) {
        this.diferido = diferido;
    }
}
