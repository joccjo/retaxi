package com.system.sumo.retaxi.dao;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.system.sumo.retaxi.dao.sqlite.ChatMessageSqliteDao;
import com.system.sumo.retaxi.dao.sqlite.UsuarioSqliteDao;
import com.system.sumo.retaxi.dao.sqlite.ViajeSqliteDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by usuario on 19/09/2016.
 */
public class SqliteDaoFactory extends DaoFactory {

    private static SQLiteDatabase _sqLiteDatabase;
    public static synchronized SQLiteDatabase open() throws Exception {
        //File file = BaseDeDatosBo.DATABASE_FILE;
        if (_sqLiteDatabase == null || !_sqLiteDatabase.isOpen()) {
           // _sqLiteDatabase = SQLiteDatabase.openDatabase(file.getPath(), null, SQLiteDatabase.OPEN_READWRITE | SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        }
        return _sqLiteDatabase;
    }

    @Override
    public void closeConnection() throws SQLException {
        _sqLiteDatabase.close();
    }

    @Override
    public IUsuarioDao getUsuarioDao() {
        return new UsuarioSqliteDao();
    }

    @Override
    public IViajeDao getViajeDao() {
        return new ViajeSqliteDao();
    }

    @Override
    public IChatMessageDao chatMessageDao() {
        return new ChatMessageSqliteDao();
    }

    @Override
    public void beginTransaction() {
        _sqLiteDatabase.beginTransaction();
    }

    @Override
    public void commit() {
        _sqLiteDatabase.setTransactionSuccessful();
        _sqLiteDatabase.endTransaction();
    }

    @Override
    public void rollback() {
        if(_sqLiteDatabase.inTransaction()){
            _sqLiteDatabase.endTransaction();
        }
    }

    public static String formatDateTimeToString(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.format(date);
    }

    public static Date convertToDateTime(String date) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return simpleDateFormat.parse(date);
    }
}
