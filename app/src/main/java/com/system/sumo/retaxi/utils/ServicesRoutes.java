package com.system.sumo.retaxi.utils;

public class ServicesRoutes {
    public static final String BASE_WS = "https://";
    //public static final String BASE_WS = "http://";
    //private static final String WS_IP = "retaxi.ddns.net";
    public static final String WS_IP = "retaxi.com.ar";
    //public static final String WS_IP = "192.168.0.113:8000";
    public static final String WS_ROUTE = "/";

    public static final String CLIENT_ID_GOOGLE = "541680084499-l5b0iqtu99gsmeh625i69u7ucgqsvf1r.apps.googleusercontent.com";
    public static final String CLIENT_SECRET_GOOGLE = "buhaiIUd8CWrx_-5O8AenJPo";

    public static final String CLIENT_ID = "l1rumVshwhqrXO9UdIMYNN6fBQxh7ja16YHcazsb";
    public static final String CLIENT_SECRET = "oMowzy8mm8l9OAN0bkMybMVYQlUBkfsBdkMcSKJc5ww5Glq85wX2A5uSaJBqygHWCuZYc0ZC5Tffc0H5CbKzTGcGwSDR6ueiA6b5GIzEOBwJyAq4EmLyVnp4yc7AqBQL";
    public static final String GET_ACCESS_TOKEN_GOOGLE = "https://www.googleapis.com/oauth2/v4/token";

    public static final String LOGIN_CONVER_TOKEN = BASE_WS + WS_IP + WS_ROUTE + "auth/convert-token";
    public static final String REFRESH_TOKEN = BASE_WS + WS_IP + WS_ROUTE + "auth/token";

    public static final String LOGIN_RIDER_WS = BASE_WS + WS_IP + WS_ROUTE + "login_pasajero/";
    public static final String LOGIN_DRIVER_WS = BASE_WS + WS_IP + WS_ROUTE + "login_conductor/";
    public static final String LOGIN_ADDITIONAL_DATA_WS = BASE_WS + WS_IP + WS_ROUTE + "pasajero_data_udt/";
    public static final String LOGIN_ASSOCIATE_DRIVER_WS = BASE_WS + WS_IP + WS_ROUTE + "asociar_conductor/";

    public static final String GET_TAXI_WS = BASE_WS + WS_IP + WS_ROUTE + "pedir_remis/";
    public static final String CANCEL_TRAVEL = BASE_WS + WS_IP + WS_ROUTE + "cancelar_viaje/";
    public static final String SEND_MESSAGE = BASE_WS + WS_IP + WS_ROUTE + "enviar_msj/";
    public static final String ACCEPT_TRAVEL = BASE_WS + WS_IP + WS_ROUTE + "aceptar_viaje/";
    public static final String START_TRAVEL = BASE_WS + WS_IP + WS_ROUTE + "iniciar_viaje/";
    public static final String END_TRAVEL = BASE_WS + WS_IP + WS_ROUTE + "finalizar_viaje/";
    public static final String QUALIFY_DRIVER = BASE_WS + WS_IP + WS_ROUTE + "puntuar_conductor/";

    public static final String SEND_LOCATION_TO_RIDER = BASE_WS + WS_IP + WS_ROUTE + "envia_posicion/";
    public static final String UPDATE_DRIVER_LOCATION = BASE_WS + WS_IP + WS_ROUTE + "actualizar_ubicacion/";

    public static final String UPDATE_FIREBASE_TOKEN_DRIVER = BASE_WS + WS_IP + WS_ROUTE + "conductor_msg_token/";
    public static final String UPDATE_FIREBASE_TOKEN_RIDER = BASE_WS + WS_IP + WS_ROUTE + "pasajero_msg_token/";

    public static final String SAVE_NEW_CAR = BASE_WS + WS_IP + WS_ROUTE + "crear_movil/";
    public static final String DELETE_CAR = BASE_WS + WS_IP + WS_ROUTE + "quitar_movil/";

    public static final String DIRECTION_ROUTE = BASE_WS + WS_IP + WS_ROUTE + "direcciones/ruta/";
    public static final String DIRECTION_PLACES = BASE_WS + WS_IP + WS_ROUTE + "direcciones/lugares/";

    public static final String WS_BACKEND_GOOGLE = "google-oauth2";
    public static final String WS_BACKEND_FACEBOOK = "facebook-app";
    public static final String WS_CODE = "code";
}
