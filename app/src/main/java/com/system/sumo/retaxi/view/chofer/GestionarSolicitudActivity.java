package com.system.sumo.retaxi.view.chofer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.gson.Gson;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.services.MyFirebaseMessagingService;
import com.system.sumo.retaxi.utils.Constantes;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.utils.maps.MapDraw;
import com.system.sumo.retaxi.view.chat.ChatActivity;
import com.system.sumo.retaxi.view.markers.InfoWindowMarker;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.dialogs.VerTarifaDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.TravelWs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.system.sumo.retaxi.utils.Constantes.HEADER_AUTHORIZATION;
import static com.system.sumo.retaxi.utils.Constantes.HEADER_CONTENT_TYPE;
import static com.system.sumo.retaxi.utils.Constantes.HEADER_JSON;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CANCELLED_BY_RIDER;
import static com.system.sumo.retaxi.utils.Constantes.TIEMPO_DISPONIBLE_PARA_ACEPTAR_SOLICITUD;
import static com.system.sumo.retaxi.utils.Constantes.TOKEN;
import static com.system.sumo.retaxi.utils.maps.MapDraw.angleBteweenCoordinate;
import static com.system.sumo.retaxi.utils.maps.MapDraw.midPoint;
import static com.system.sumo.retaxi.utils.ServicesRoutes.ACCEPT_TRAVEL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CANCEL_TRAVEL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.DIRECTION_ROUTE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.START_TRAVEL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.END_TRAVEL;

public class GestionarSolicitudActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        Response.Listener<String>,
        Response.ErrorListener,
        GoogleMap.OnInfoWindowClickListener{
    private static final String KEY_DIRECTION_ROUTE = "direction_route";
    //RestCustomer.OnRequestRestCustomerFinish,
        //MyFirebaseMessagingService.OnMessageRecibed{
    public static String EXTRA_USUARIO = "extra_usuario";
    private final static int REQUEST_CHECK_SETTINGS = 0;
    public static String EXTRA_VIAJE = "mViaje";
    private ProgressBar mProgress;
    private static final int PROGRESS = 0x1;
    private int mProgressStatus = 100;
    private Handler mHandler = new Handler();
    private Button rechazar;
    private Button aceptar;
    Button cancelarViaje;
    Button pasajeroABordo;
    private Thread thread;
    private LinearLayout linearTransparencia;
    private Vibrator v;
    private Ringtone ringtone;
    private GoogleMap map;
    Marker markerDesde;
    Marker markerHasta;
    MapDraw mapDraw;
    TextView tvDesde;
    TextView tvHasta;
    Polyline polyline;
    LocationRequest locationRequest;
    ImageView idImgMsjChat;
    ProgressDialogFragment progressDialogFragment;
    private static int REQUEST_CODE_ASK_PERMISSIONS_WINDOW_CHAT = 1;
    private static String DIRECTIONS_CONNECT_POINTS = "directions_connect_points";
    private static String ACCEPT_REQUEST_TRAVEL = "ACCEPT_REQUEST_TRAVEL";
    boolean itsActivityActive = false;
    boolean isInterrupted = false;
    private static String EXTRA_DIRECTIONS_ROUTE = "EXTRA_DIRECTIONS_ROUTE";
    /**
     * Indica si el activity se recrea porque android mato el proceso
     */
    boolean isRecreatedByAndroidKilled;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient mGoogleApiClient;
    Viaje mViaje;
    private String directionsRoute="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            Viaje viaje = (Viaje)savedInstanceState.getSerializable(EXTRA_VIAJE);
            directionsRoute = savedInstanceState.getString(EXTRA_DIRECTIONS_ROUTE);
            mViaje = viaje;
            isRecreatedByAndroidKilled = true;
        }else {
            mViaje = (Viaje) getIntent().getExtras().get(EXTRA_VIAJE);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON | WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON);
        setContentView(R.layout.activity_gestionar_solicitud);
        //buildGoogleApiClient();
        initComponents();
    }

    /*protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(GestionarSolicitudActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }*/

    private void finalizarActivity() {
        detenerSolicitud();
        this.finish();
    }

    private void detenerSolicitud() {
        v.cancel();
        ringtone.stop();
    }

    private MyFirebaseMessagingService.OnMessageRecibed getOnMansajeRecibed() {
        return new MyFirebaseMessagingService.OnMessageRecibed() {
            @Override
            public void doCallBack(Object o, String indicadorFirebase) {
                switch (indicadorFirebase){
                    case MESSAGE_TYPE_CANCELLED_BY_RIDER:
                        onRiderCancelledTravel();
                        break;
                }
            }
        };
    }

    private void onRiderCancelledTravel(){
        if(ChatActivity.finishListener != null)
            ChatActivity.finishListener.finishActivity();
        if(itsActivityActive) {
            //Intent chat = new Intent(GestionarSolicitudActivity.this, ChatHeadService.class);
            //stopService(chat);
            /*if (ChatActivity.myDialog != null)
                ChatActivity.myDialog.finish();*/
            SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.pasajeroCancelo), new SimpleDialogFragment.OkListener() {
                @Override
                public void onOkSelected() {
                    GestionarSolicitudActivity.this.finish();
                    MyApplication.getInstance().setConductorEnViaje(false);
                    MyApplication.getInstance().setConductorInRequest(false);
                    MyApplication.getInstance().setFirebaseMessajeType(null);
                }
            }, false).show(getSupportFragmentManager(), "");
        }
    }


    @Override
    protected void onResume() {
        itsActivityActive = true;
        super.onResume();
        if(MyApplication.getInstance().getFirebaseMessajeType().equals(MESSAGE_TYPE_CANCELLED_BY_RIDER)){
            onRiderCancelledTravel();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_VIAJE, mViaje);
        outState.putSerializable(EXTRA_USUARIO, MyApplication.getInstance().getPreferences().getUsuario());
        outState.putString(EXTRA_DIRECTIONS_ROUTE, directionsRoute);
        itsActivityActive = false;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    private void initComponents() {
        MyFirebaseMessagingService.setOnMessageRecibed(getOnMansajeRecibed());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        tvDesde = (TextView) findViewById(R.id.adressTextDesde);
        tvDesde.setText(mViaje.getDesde());
        tvHasta = (TextView) findViewById(R.id.adressTextHasta);
        tvHasta.setText(mViaje.getHasta());

        linearTransparencia = (LinearLayout) findViewById(R.id.linearLayoutTransparencia);

        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.getIndeterminateDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
        idImgMsjChat = (ImageView) findViewById(R.id.idImgMsjChat);
        idImgMsjChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(GestionarSolicitudActivity.this, ChatActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
            }
        });
        rechazar = (Button) findViewById(R.id.buttonReject);
        rechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInterrupted = true;
                getNextViaje();

            }
        });
        aceptar = (Button) findViewById(R.id.buttonAccept);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInterrupted = true;
                detenerSolicitud();
                //enviar notificacion de aceptacion del mViaje al servidor
                acceptTaxiToServer();
            }
        });

        if(!isRecreatedByAndroidKilled) {
            v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] patern = {0, 2000, 2000};
            v.vibrate(patern, 1);
            ringtone = RingtoneManager.getRingtone(getApplicationContext(),
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
            if (ringtone != null) {
                ringtone.play();
            }

            thread = new Thread(new Runnable() {
                public void run() {
                    isInterrupted = false;
                    while (mProgressStatus > 0) {
                        if (isInterrupted)
                            break;
                        try {
                            Thread.sleep(TIEMPO_DISPONIBLE_PARA_ACEPTAR_SOLICITUD);//300 30segundos
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        mProgressStatus--;
                        // Update the progress bar
                        mHandler.post(new Runnable() {
                            public void run() {
                                mProgress.setProgress(mProgressStatus);
                            }
                        });
                    }
                    if (!isInterrupted) {
                        MyApplication.getInstance().getViajesPendientesChofer().remove(0);
                        if(MyApplication.getInstance().getViajesPendientesChofer().size() > 0)
                            lanzarSolicitudPendiente();
                        else
                            MyApplication.getInstance().setConductorInRequest(false);
                        finalizarActivity();
                    }
                }
            });
            thread.start();
        }

    }

    private void getNextViaje(){
        detenerSolicitud();

        MyApplication.getInstance().getViajesPendientesChofer().remove(0);
        if(MyApplication.getInstance().getViajesPendientesChofer().size() > 0) {
            lanzarSolicitudPendiente();
        }else{
            MyApplication.getInstance().setConductorInRequest(false);
        }
        finalizarActivity();
    }

    private void lanzarSolicitudPendiente(){
        Intent alarmIntent = new Intent(GestionarSolicitudActivity.this, GestionarSolicitudActivity.class);
        alarmIntent.putExtra(GestionarSolicitudActivity.EXTRA_VIAJE, MyApplication.getInstance().getViajesPendientesChofer().get(0));
        alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        startActivity(alarmIntent);
    }

    private void acceptTaxiToServer(){
        JSONObject body = new JSONObject();
        Gson g = new Gson();
        try {
            body.put(getString(R.string.origen), markerDesde.getPosition().latitude + "," + markerDesde.getPosition().longitude);
            body.put(getString(R.string.destino), markerHasta.getPosition().latitude + "," + markerHasta.getPosition().longitude);
            body.put(getString(R.string.origenDescripcion), tvDesde.getText());
            body.put(getString(R.string.destinoDescripcion), tvHasta.getText());
            body.put(getString(R.string.pasajeroModel), mViaje.getPasajero().getDni());
            body.put(getString(R.string.movilModel), g.toJsonTree(MyApplication.getInstance().getMovilSelected()));
        } catch (JSONException e) { e.printStackTrace(); }
        progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.validandoOperacion));
        progressDialogFragment.show(getSupportFragmentManager(), "");
        //new RestCustomer(this, url, body, Constantes.POST, MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth(), getOnRequestCustomerFinish()).execute();
        RestCustomerVolley.addQueue(Request.Method.POST, ACCEPT_TRAVEL, body, true, getOnRequestCustomerFinish());
    }

    private VerTarifaDialogFragment.FinalizarListener getFinalizarListener() {
        return new VerTarifaDialogFragment.FinalizarListener() {
            @Override
            public void finalizarListener() {
                MyApplication.getInstance().setConductorEnViaje(false);
                MyApplication.getInstance().setConductorInRequest(false);
            }
        };
    }

    @Override
    public void onBackPressed() {
       /* super.onBackPressed();
        SimpleDialogFragment desconectar = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, "", getString(R.string.seguirRecibiendoSolicitudes), new SimpleDialogFragment.OkCancelListener() {
            @Override
            public void onCancelSelected() {
                finalizarActivity();
            }

            @Override
            public void onOkSelected() {

            }
        });
        desconectar.show(getSupportFragmentManager(), "");*/
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;
        map.clear();
        LatLng puntoDesde = new LatLng(mViaje.getPuntoDesdeLocationPoint().getCoordinates()[0], mViaje.getPuntoDesdeLocationPoint().getCoordinates()[1]);
        MarkerOptions markerOptionsDesde = new MarkerOptions().position(puntoDesde).title("");
        //markerOptionsDesde.draggable(true);
        markerOptionsDesde.flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_radio_button_unchecked_green_24dp))
                .anchor(0.5f, 0.5f);
        markerDesde = map.addMarker(markerOptionsDesde);
        //LatLng puntoHassta = new LatLng(mViaje.getPuntoHastaLocationPoint().getCoordinates()[0], mViaje.getPuntoHastaLocationPoint().getCoordinates()[1]);
        if (mViaje.getPuntoHastaLocationPoint() != null){
            MarkerOptions markerOptionsHasta = new MarkerOptions().position(mViaje.getPuntoHasta()).title(getString(R.string.hasta));
            markerOptionsHasta//.flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_flag_checkered_black_24dp));
            markerHasta = map.addMarker(markerOptionsHasta);
            CameraPosition cameraPosition = new CameraPosition.Builder().target(midPoint(markerDesde.getPosition().latitude, markerDesde.getPosition().longitude, markerHasta.getPosition().latitude, markerHasta.getPosition().longitude))
                    .zoom(14) // Sets the zoom
                    .bearing(angleBteweenCoordinate(markerDesde.getPosition().latitude, markerDesde.getPosition().longitude, markerHasta.getPosition().latitude, markerHasta.getPosition().longitude)) // Sets the orientation of the camera to
                    .tilt(30) // Sets the tilt of the camera to 30 degrees
                    .build();
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mapDraw = new MapDraw();
        }

        if (ContextCompat.checkSelfPermission(GestionarSolicitudActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }

        if(isRecreatedByAndroidKilled){
            polyline = mapDraw.drawPath(directionsRoute, map);
            switch (mViaje.getEstado()){
                case Viaje.TRAVEL_STATE_ACCEPTED:
                    showViajeAccepted();
                    break;
                case Viaje.TRAVEL_STATE_STARTED:
                    showViajeAccepted();
                    showViajeIniciado();
                    break;
            }
        }else if (markerHasta != null) {
            String url = makeURLForDirectionsApi(markerDesde.getPosition().latitude, markerDesde.getPosition().longitude, markerHasta.getPosition().latitude, markerHasta.getPosition().longitude);
            MyApplication.volleyQueueInstance.cancelRequestInQueue(DIRECTIONS_CONNECT_POINTS);
            HashMap<String, String> head = new HashMap<>();
            head.put(HEADER_CONTENT_TYPE, HEADER_JSON);
            head.put(HEADER_AUTHORIZATION, TOKEN + " " + MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth());
            RestCustomerVolley.addQueue(Request.Method.GET, url, null, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
                @Override
                public void onOkRestCustomerResult(String object) {
                    polyline = mapDraw.drawPath(object, map);
                    directionsRoute = object;
                }

                @Override
                public void onErrorRestCustomerResult(String result) {

                }
            });
        }
    }

    public String makeURLForDirectionsApi(double sourcelat, double sourcelog, double destlat, double destlog){
        StringBuilder urlString = new StringBuilder();
        urlString.append(DIRECTION_ROUTE);
        urlString.append(sourcelat+","+sourcelog);
        urlString.append("/");
        urlString.append(destlat+","+destlog);
        return urlString.toString();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String result) {
        polyline = mapDraw.drawPath(result, map);
        directionsRoute = result;
    }

    private void animateCameraToPoint(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng)
                .zoom(15) // Sets the zoom
                .bearing(90) // Sets the orientation of the camera to
                .tilt(30) // Sets the tilt of the camera to 30 degrees
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                if(resultCode == RESULT_OK){
                    //Intent intent = new Intent(GestionarSolicitudActivity.this, ChatHeadService.class);
                    //startService(intent);
                }

        }
    }

    private void showViajeAccepted(){
        mProgress.setVisibility(View.INVISIBLE);
        linearTransparencia.setVisibility(View.VISIBLE);
        map.setInfoWindowAdapter(new InfoWindowMarker(GestionarSolicitudActivity.this, markerDesde, mViaje));
        animateCameraToPoint(mViaje.getPuntoDesde());
        markerDesde.showInfoWindow();

        cancelarViaje = aceptar;
        cancelarViaje.setText(R.string.cancelarViaje);
        cancelarViaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.cancelarViajePregunta), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.cancelandoViaje));
                        progressDialogFragment.show(getSupportFragmentManager(), "");

                        RestCustomerVolley.addQueue(Request.Method.POST, CANCEL_TRAVEL, null, true, getOnRequestCustomerFinish());
                    }
                }).show(getSupportFragmentManager(), "");
            }
        });
        cancelarViaje.setTextColor(Color.parseColor(getString(R.string.buttonCancel)));

        pasajeroABordo = rechazar;
        pasajeroABordo.setText(R.string.pasajeroABordo);
        pasajeroABordo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startTravel();
                RestCustomerVolley.addQueue(Request.Method.POST, START_TRAVEL, null, true, getOnRequestCustomerFinish());
                progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.validandoOperacion));
                progressDialogFragment.show(getSupportFragmentManager(), "");
            }
        });

        //checkdrawPermission();
        idImgMsjChat.setVisibility(View.VISIBLE);
    }

    private void startTravel(){
        TravelWs travelWs = new TravelWs();
        travelWs.startTravel(new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                mViaje.setEstado(Viaje.TRAVEL_STATE_STARTED);
                ViajeBo v = new ViajeBo();
                try {
                    v.create(mViaje);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                showViajeIniciado();
            }

            @Override
            public void onErrorResult() {
                Toast.makeText(GestionarSolicitudActivity.this, R.string.startTravelFailed, Toast.LENGTH_SHORT);
            }
        });

    }

    private void showViajeIniciado(){
        Toast.makeText(GestionarSolicitudActivity.this, R.string.startedTravel, Toast.LENGTH_SHORT);
        Button verTarifa = pasajeroABordo;
        verTarifa.setText(R.string.consultarTarifa);
        verTarifa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                VerTarifaDialogFragment verTarifaDialogFragment = VerTarifaDialogFragment.newInstance(getFinalizarListener());
                verTarifaDialogFragment.show(getSupportFragmentManager(), "");
            }
        });
        Button finalizarViaje = cancelarViaje;
        finalizarViaje.setText(R.string.finalizarViaje);
        finalizarViaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RestCustomerVolley.addQueue(Request.Method.POST, END_TRAVEL, null, true, getOnRequestCustomerFinish());
                progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.validandoOperacion));
                progressDialogFragment.show(getSupportFragmentManager(), "");
            }
        });
    }

    private RestCustomerVolley.OnRequestRestCustomerFinish getOnRequestCustomerFinish() {
        return new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                try {
                    progressDialogFragment.dismissAllowingStateLoss();
                    Gson gson = new Gson();
                    ResultWsCall r = gson.fromJson(object, ResultWsCall.class);
                    switch (r.getRespuesta()) {
                        case Constantes.SOLICITUD_ACEPTADA_CORRECTAMENTE:
                            mViaje.setEstado(Viaje.TRAVEL_STATE_ACCEPTED);
                            isInterrupted = true;
                            MyApplication.getInstance().setConductorInRequest(false);
                            MyApplication.getInstance().setConductorEnViaje(true);
                            //elimino las solicitudes pendientes
                            MyApplication.getInstance().setViajesPendientesChofer(null);
                            PreferencesBo.saveTravelInPreferences(mViaje);

                            Toast.makeText(GestionarSolicitudActivity.this, R.string.aceptadoExito, Toast.LENGTH_SHORT);
                            showViajeAccepted();

                            break;
                        case Constantes.VIAJE_ACEPTADO_POR_OTRO_CONDUCTOR:
                            //if(!(MyApplication.getInstance().getViajesPendientesChofer().size() > 0))
                            //    MyApplication.getInstance().setConductorInRequest(false);

                            SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.aceptadoPorOtroConductor), getString(R.string.ups), new SimpleDialogFragment.OkListener() {

                                @Override
                                public void onOkSelected() {
                                    getNextViaje();
                                }
                            }).show(getSupportFragmentManager(), "");
                            break;
                        case Constantes.VIAJE_CANCELADO_POR_PASAJERO:
                            //if(!(MyApplication.getInstance().getViajesPendientesChofer().size() > 0))
                            //    MyApplication.getInstance().setConductorInRequest(false);
                            SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.canceladoXPasajero), getString(R.string.ups), new SimpleDialogFragment.OkListener() {

                                @Override
                                public void onOkSelected() {
                                    getNextViaje();
                                }
                            }, false).show(getSupportFragmentManager(), "");
                            break;
                        case Constantes.CANCELA_VIAJE_EXITO:
                            Toast.makeText(GestionarSolicitudActivity.this, R.string.viajeCancelado, Toast.LENGTH_SHORT).show();
                            MyApplication.getInstance().setConductorEnViaje(false);
                            //Intent intentChatHead = new Intent(GestionarSolicitudActivity.this, ChatHeadService.class);
                            //stopService(intentChatHead);
                        /*if(ChatActivity.myDialog != null)
                            ChatActivity.myDialog.finish();*/
                            if (ChatActivity.finishListener != null)
                                ChatActivity.finishListener.finishActivity();
                            GestionarSolicitudActivity.this.finish();
                            break;
                        case Constantes.FINALIZAR_VIAJE_EXITO:
                            //Intent intentt = new Intent(GestionarSolicitudActivity.this, ChatHeadService.class);
                            //stopService(intentt);
                        /*if(ChatActivity.myDialog != null)
                            ChatActivity.myDialog.finish();*/
                            if (ChatActivity.finishListener != null)
                                ChatActivity.finishListener.finishActivity();
                            progressDialogFragment.dismissAllowingStateLoss();
                            GestionarSolicitudActivity.this.finish();
                            MyApplication.getInstance().setConductorEnViaje(false);
                            MyApplication.getInstance().setConductorInRequest(false);
                            break;
                        default:
                            progressDialogFragment.dismissAllowingStateLoss();
                    }
                }catch (Exception e){
                    MyApplication.getInstance().setConductorEnViaje(false);
                    MyApplication.getInstance().setConductorInRequest(false);
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.noSepudoRealizarPeticion)).show(getSupportFragmentManager(), "");
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.falloRequestSv)).show(getSupportFragmentManager(), "");
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if(MyApplication.getInstance().isConductorEnViaje()) {
            SharedPreferences sharedPref = UsuarioBo.getSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = sharedPref.edit();
            Gson gson = new Gson();
            String jsonViaje = gson.toJson(mViaje);
            String jsonUsuario = gson.toJson(MyApplication.getInstance().getPreferences().getUsuario());
            editor.putString(Viaje.KEY_VIAJE_PREFERENCES, jsonViaje);
            editor.putString(Usuario.KEY_USUARIO_PREFERENCES, jsonUsuario);
            editor.putString(KEY_DIRECTION_ROUTE, directionsRoute);
            editor.apply();
        }*/
    }
}
