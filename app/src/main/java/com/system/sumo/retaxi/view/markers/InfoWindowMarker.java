package com.system.sumo.retaxi.view.markers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.Viaje;

/**
 * Created by usuario on 22/09/2016.
 */
public class InfoWindowMarker implements GoogleMap.InfoWindowAdapter {
    private final View myContentsView;
    private Context context;
    private Marker marker;
    private boolean isFirstClick = true;
    Usuario usuario;
    Viaje viaje;

    /*public InfoWindowMarker(Context context, Marker marker, Usuario usuario){
        this.context = context;
        this.marker = marker;
        this.usuario = usuario;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        myContentsView = inflater.inflate(R.layout.usuario_info_window_map, null);
    }*/

    public InfoWindowMarker(Context context, Marker marker, Viaje viaje){
        this.context = context;
        this.marker = marker;
        this.viaje = viaje;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        myContentsView = inflater.inflate(R.layout.usuario_info_window_map, null);
    }

    @Override
    public View getInfoContents(final Marker marker) {
        if(marker.equals(this.marker)) {
            if(isFirstClick) {
                TextView tvVehiculo = ((TextView) myContentsView.findViewById(R.id.tvVehiculo));
                RatingBar ratingBar = (RatingBar) myContentsView.findViewById(R.id.ratingBar);
                LinearLayout linearLayout = (LinearLayout) myContentsView.findViewById(R.id.linearPrincipal);
                linearLayout.setPadding(0,0,0,0);
                //TextView tvDireccionPasajero = (TextView) myContentsView.findViewById(R.id.tvDireccion);
                if(MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario() == Usuario.TIPO_PASAJERO){
                    usuario = viaje.getConductor();
                    if (viaje.getMovil() != null)
                        tvVehiculo.setText(viaje.getMovil().toString());
                    ratingBar.setRating(usuario.getReputation() / usuario.getViajes_realizados());
                    //tvDireccionPasajero.setVisibility(View.GONE);
                }else{
                    usuario = viaje.getPasajero();
                    tvVehiculo.setVisibility(View.GONE);
                    ratingBar.setVisibility(View.GONE);
                    //tvDireccionPasajero.setText(viaje.getDesde());
                }
                isFirstClick = false;
                TextView tvApellido = ((TextView) myContentsView.findViewById(R.id.tvApellido));
                tvApellido.setText(usuario.getApellido());
                TextView tvNombre = ((TextView) myContentsView.findViewById(R.id.tvNombre));
                tvNombre.setText(usuario.getNombre());
                final RoundedImageView imageView = (RoundedImageView) myContentsView.findViewById(R.id.imgViewProfile);
                //if(isFirstClick) {
                if (!usuario.getUrlImagen().equals(""))
                    Picasso.get().load(usuario.getUrlImagen())
                            .into(imageView, new Callback() {
                                @Override
                                public void onSuccess() {
                                    marker.showInfoWindow();
                                    marker.showInfoWindow();
                                }

                                @Override
                                public void onError(Exception e) {
                                    Picasso.get().load(usuario.getUrlImagen())
                                            .into(imageView);
                                }
                            });
            }
                return myContentsView;

        }
        return null;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
