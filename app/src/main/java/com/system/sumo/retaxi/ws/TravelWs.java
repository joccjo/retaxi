package com.system.sumo.retaxi.ws;

import com.android.volley.Request;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Constantes;
import com.system.sumo.retaxi.utils.RestCustomerVolley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.system.sumo.retaxi.utils.Constantes.FECHA_HORA;
import static com.system.sumo.retaxi.utils.Constantes.DESTINATION;
import static com.system.sumo.retaxi.utils.Constantes.DESTINATION_DESCRIPTION;
import static com.system.sumo.retaxi.utils.Constantes.ORIGIN;
import static com.system.sumo.retaxi.utils.Constantes.ORIGIN_DESCRIPTION;
import static com.system.sumo.retaxi.utils.Constantes.TRAVEL_ID;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CANCEL_TRAVEL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.GET_TAXI_WS;
import static com.system.sumo.retaxi.utils.DistinctServicesRoutes.GET_TRAVELS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.START_TRAVEL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.END_TRAVEL;

public class TravelWs extends GenericWs{

    public void getTaxi(final String dateTime, final Marker markerFrom, final Marker markerTo, final String originDescription, final String destinationDescription, final GenericWsResponse genericWsResponse){

        JSONObject body = new JSONObject();
        try {
            if (dateTime != null) {
                body.put(FECHA_HORA, dateTime);
            }
            body.put(ORIGIN, markerFrom.getPosition().latitude + "," + markerFrom.getPosition().longitude);
            if (!destinationDescription.equals("")){
                body.put(DESTINATION, markerTo.getPosition().latitude + "," + markerTo.getPosition().longitude);
                body.put(DESTINATION_DESCRIPTION, destinationDescription);
            }
            body.put(ORIGIN_DESCRIPTION, originDescription);
        } catch (JSONException e) { e.printStackTrace(); }

        RestCustomerVolley.addQueue(Request.Method.POST, GET_TAXI_WS, body, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Gson gson = new Gson();
                try {
                    /*if (dateTime != null){
                        ViajeBo viajeBo = new ViajeBo();
                        viajeBo.createDeferredTravel(dateTime, latOrigin, lngOrigin, latDestination, longDestination, originDescription, destinationDescription);
                    }*/
                    ResultWsCall resultWsCall = gson.fromJson(object, ResultWsCall.class);
                    genericWsResponse.onOkResult(resultWsCall);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void cancelTravel(final GenericWsResponse genericWsResponse){
        cancelTravel(MyApplication.getInstance().getPreferences().getViaje().getId(), genericWsResponse);
    }

    public void cancelTravel(int travelId, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put(TRAVEL_ID, travelId);
        } catch (JSONException e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, CANCEL_TRAVEL, body, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Gson gson = new Gson();
                try {
                    ResultWsCall resultWsCall = gson.fromJson(object, ResultWsCall.class);
                    genericWsResponse.onOkResult(resultWsCall);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void getFinishedTravels(final GenericWsResponse genericWsResponse){
        getTravels(GET_TRAVELS + "?estado=" + Viaje.TRAVEL_STATE_FINISHED + "&diferido=0", genericWsResponse);
    }

    public void getDeferredTravels(final GenericWsResponse genericWsResponse){
        getTravels(GET_TRAVELS + "?estado=" + Viaje.TRAVEL_STATE_PENDING + "&diferido=1", genericWsResponse);
    }

    public void getTravels(String url, final GenericWsResponse genericWsResponse){
        RestCustomerVolley.addArrayRequesr(Request.Method.GET, url , null, true, new RestCustomerVolley.OnJsonArrayRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(JSONArray object) {
                Gson gson = new Gson();
                try {
                    List<Viaje> travels = new ArrayList<>();
                    for (int i = 0; i < object.length(); ++i) {
                        JSONObject rec = object.getJSONObject(i);
                        Viaje resultWsCall = gson.fromJson(rec.toString(), Viaje.class);
                        travels.add(resultWsCall);
                        //....
                    }
                    genericWsResponse.onOkResult(travels);
                }catch (Exception e){
                    genericWsResponse.onErrorResult();
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void startTravel(final GenericWsResponse genericWsResponse){
        RestCustomerVolley.addQueue(Request.Method.POST, START_TRAVEL, null, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Gson gson = new Gson();
                ResultWsCall r = gson.fromJson(object, ResultWsCall.class);
                if (r.getRespuesta() == Constantes.INICIA_VIAJE_EXITO){
                    genericWsResponse.onOkResult("");
                }else{
                    genericWsResponse.onErrorResult();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void finishTravel(final GenericWsResponse genericWsResponse){
        RestCustomerVolley.addQueue(Request.Method.POST, END_TRAVEL, null, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Gson gson = new Gson();
                ResultWsCall r = gson.fromJson(object, ResultWsCall.class);
                if (r.getRespuesta() == Constantes.INICIA_VIAJE_EXITO){
                    genericWsResponse.onOkResult("");
                }else{
                    genericWsResponse.onErrorResult();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }
}
