package com.system.sumo.retaxi.view.dialogs;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Tarifa;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.dialogs.generics.BaseDialogFragment;

/**
 * Created by usuario on 10/09/2016.
 */
public class VerTarifaDialogFragment extends BaseDialogFragment {
    private static final String KEY_EXTRA_BANDERA = "bandera";
    private static final String KEY_EXTRA_FICHA = "ficha";
    private static final String KEY_EXTRA_DESDE = "desde";
    private static final String KEY_EXTRA_HASTA = "hasta";
    private static final String KEY_EXTRA_TOTAL_RECORRIDO = "totalRecorrido";
    private static final String KEY_EXTRA_MONTO_VIAJE = "montoViaje";
    public static final String KEY_EXTRA_VIAJE = "viaje";
    FinalizarListener listener;

    public static VerTarifaDialogFragment newInstance(FinalizarListener listener){
        VerTarifaDialogFragment dialogFragment = new VerTarifaDialogFragment();
        dialogFragment.listener = listener;
        //dialogFragment.setArguments(b);
        return dialogFragment;
    }

    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.ver_tarifa_dialog_fragment, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        return alertDialogBuilder.show();
    }

   /* @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            initComponents(savedInstanceState);
        }
    }*/

    private void initComponents(View view){
        Viaje viaje = MyApplication.getInstance().getPreferences().getViaje();
        Tarifa tarifa = viaje.getTarifa();
        if (tarifa != null) {
            TextView nombreTarifa = view.findViewById(R.id.idNombreTarifa);
            nombreTarifa.setText(tarifa.getFirst_name());
            TextView textViewBajadaBandera = view.findViewById(R.id.idBajadaDeBandera);
            textViewBajadaBandera.setText(Formatter.formatMoney(Float.parseFloat(tarifa.getBajada_de_bandera())));
        /*TextView adicional = (TextView) view.findViewById(R.id.idAdicional);
        adicional.setText(textViewBajadaBandera.getText() + Formatter.formatMoney(Float.parseFloat(tarifa.getAdicional())));*/
            TextView totalCuadras = view.findViewById(R.id.idTotalRecorridoCuadras);
            totalCuadras.setText(Formatter.formatTwoDecimals(tarifa.getCuadras()));
            TextView totalMetros = view.findViewById(R.id.idTotalRecorridoMts);
            totalMetros.setText(tarifa.getMetros());
            TextView textViewFicha = view.findViewById(R.id.idFicha);
            textViewFicha.setText(Formatter.formatMoney(Float.parseFloat(tarifa.getFicha())));
            //TextView textViewDesde = view.findViewById(R.id.idDesde);
            //textViewDesde.setText(viaje.getDesde());
            //TextView textViewHasta = view.findViewById(R.id.idHasta);
            //textViewHasta.setText(viaje.getHasta());
            TextView textViewMontoViaje = view.findViewById(R.id.idMontoViaje);
            textViewMontoViaje.setText(Formatter.formatMoney(Float.parseFloat(tarifa.getPrecio_viaje())));
        }
        Button buttonFinalizar = view.findViewById(R.id.idBtnFinalizarViaje);
        buttonFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.finalizarListener();
                dismiss();
            }
        });
    }

    public interface DialogListener {
    }

    public interface FinalizarListener extends DialogListener {
        void finalizarListener();
    }
}
