package com.system.sumo.retaxi.view.travels;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Formatter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by usuario on 16/08/2016.
 */
public class ViajesArrayAdapter extends ArrayAdapter<Viaje> {
    List<Viaje> viajes;
    public ViajesArrayAdapter(Context context, int layoutResourceId, List<Viaje> viajes) {
        super(context, layoutResourceId, viajes);
        this.viajes = viajes;
    }

    @Override
    public int getCount() {
        return viajes.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Viaje viaje = getItem(position);
        ViewHolder holder = null;
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.listview_item_viajes, parent, false);
            holder = new ViewHolder();
            holder.fechaYHoraValue = (TextView) convertView.findViewById(R.id.fechaYHoraValue);
            holder.precioViajeValue = (TextView) convertView.findViewById(R.id.precioViajeValue);
            holder.desdeValue = (TextView) convertView.findViewById(R.id.desdeValue);
            holder.hastaValue = (TextView) convertView.findViewById(R.id.hastaValue);
            // Populate the data into the template view using the data object

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            Date date = Formatter.convertToDateTimeDjangoTravel(viaje.getFechaHora());
            holder.fechaYHoraValue.setText(Formatter.formatDateTimeMinutes(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (viaje.getMonto() != null)
            holder.precioViajeValue.setText(Formatter.formatMoney(Float.parseFloat(viaje.getMonto())));
        holder.desdeValue.setText(viaje.getDesde());
        holder.hastaValue.setText(viaje.getHasta());
        return convertView;
    }

    public class ViewHolder{
        TextView fechaYHoraValue;
        TextView precioViajeValue;
        TextView desdeValue;
        TextView hastaValue;
    }

    public List<Viaje> getViajes() {
        return viajes;
    }

    public void setViajes(List<Viaje> viajes) {
        this.viajes = viajes;
    }
}
