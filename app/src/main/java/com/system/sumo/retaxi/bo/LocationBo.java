package com.system.sumo.retaxi.bo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.utils.Constantes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationBo {
    public static String initializeLocationPoint(JSONObject jsonObject, String object){
        try {
            JSONObject locationPoint = new JSONObject();
            locationPoint.put(Constantes.LOCATION_POINT_TYPE, "");
            Double[] d =  new Double[]{0.0,0.0};
            JSONArray arr = new JSONArray();
            arr.put(0.0);
            arr.put(0.0);
            locationPoint.put(Constantes.COORDINATES, arr);
            return jsonObject.put(Constantes.ULTIMA_UBICACION, locationPoint).toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}
