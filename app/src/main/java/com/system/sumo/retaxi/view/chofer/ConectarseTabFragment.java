package com.system.sumo.retaxi.view.chofer;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.Movil;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.services.LocationService;
import com.system.sumo.retaxi.services.MyFirebaseInstanceIDService;
import com.system.sumo.retaxi.services.MyFirebaseMessagingService;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static com.system.sumo.retaxi.dto.Movil.EXTRA_MOVIL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.DELETE_CAR;
import static com.system.sumo.retaxi.utils.ServicesRoutes.END_TRAVEL;

public class ConectarseTabFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks{
    public static String EXTRA_GOOGLE_API_CLIENT = "google_api_client";
    public final static int REQUEST_CHECK_SETTINGS = 0;
    public final static int REQUEST_CHECK_LOCATION_PERMISSIONS = 1;
    Button buttonConnect;
    public static UpdateLastLocationSended updateLastLocationSended;
    public static boolean isFragmentActive;
    FloatingActionButton floatingActionButtonAdd;
    FloatingActionButton floatingActionButtonEdit;
    FloatingActionButton floatingActionButtonDelete;
    ProgressDialogFragment progressDialogFragment;
    NotificationManager mNotificationManager;
    Spinner spinner;
    LinearLayout linearLayoutConectarse;
    Button disconnect;
    TextView lastLocationSended;
    public static int NOTIFICATION_ID_PANIC_BUTTON = 1;
    LocationRequest locationRequest;
    private GoogleApiClient mGoogleApiClient;
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }

    public ConectarseTabFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conectarse_tab, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponents();
    }

    public static LocationRequest createLocationRequest(){
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    private void initComponents(){
        updateLastLocationSendedTime();
        buildGoogleApiClient();
        lastLocationSended = (TextView) getView().findViewById(R.id.idLastLocationSendedTv);
        locationRequest = createLocationRequest();
        mGoogleApiClient.connect();
        linearLayoutConectarse = (LinearLayout) getView().findViewById(R.id.linearLayoutConectado);
        buttonConnect = (Button) getView().findViewById(R.id.buttonConnect);
        spinner = (Spinner) getView().findViewById(R.id.textView);
        cargarSpinnerMoviles();
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                MyApplication.getInstance().setMovilSelected(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        buttonConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLocationSettings();
                /*if(MyApplication.getInstance().getPreferences().getUsuario().getMoviles().size() > 0){
                    cancelarViajesActivos();
                }else{
                    ABMMovilDialog.newInstance(new Bundle(), new ABMMovilDialog.ABMMovilDialogListener() {
                        @Override
                        public void onAccept() {
                            cargarSpinnerMoviles();
                            cancelarViajesActivos();
                        }
                    }).show(getFragmentManager(), "");
                }*/
            }
        });
        buttonConnect.setEnabled(false);
        disconnect = getView().findViewById(R.id.buttonDisconnect);
        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().stopService(new Intent(getActivity(), MyFirebaseInstanceIDService.class));
                getActivity().stopService(new Intent(getActivity(), MyFirebaseMessagingService.class));
                getActivity().stopService(new Intent(getActivity(), LocationService.class));
                linearLayoutConectarse.setVisibility(View.INVISIBLE);
                buttonConnect.setEnabled(true);
                MyApplication.getInstance().setChoferConectado(false);
                LoginManager.getInstance().logOut();
                if(mNotificationManager != null)
                    mNotificationManager.cancel(NOTIFICATION_ID_PANIC_BUTTON);
            }
        });

        if(MyApplication.getInstance().isChoferConectado()){
            linearLayoutConectarse.setVisibility(View.VISIBLE);
            buttonConnect.setEnabled(false);
        }

        floatingActionButtonAdd = (FloatingActionButton) getView().findViewById(R.id.idNewCar);
        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ABMMovilDialog.newInstance(new Bundle(), new ABMMovilDialog.ABMMovilDialogListener() {
                    @Override
                    public void onAccept() {
                        cargarSpinnerMoviles();
                    }
                }).show(getFragmentManager(), "");
            }
        });
        floatingActionButtonEdit = (FloatingActionButton) getView().findViewById(R.id.idEditCar);
        floatingActionButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyApplication.getInstance().getMovilSelected() != null) {
                    Bundle b = new Bundle();
                    b.putSerializable(EXTRA_MOVIL, MyApplication.getInstance().getMovilSelected());
                    ABMMovilDialog.newInstance(b).show(getFragmentManager(), "");
                }
            }
        });
        floatingActionButtonDelete = (FloatingActionButton) getView().findViewById(R.id.idRemoveCar);
        floatingActionButtonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MyApplication.getInstance().getMovilSelected() != null) {
                    String mensaje = getString(R.string.confirmacionEliminarMovil) + MyApplication.getInstance().getMovilSelected().toString();
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, mensaje, getString(R.string.confirmacion), new SimpleDialogFragment.OkCancelListener() {
                        @Override
                        public void onCancelSelected() {
                            checkLocationSettings();
                        }

                        @Override
                        public void onOkSelected() {
                            progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.validandoOperacion));
                            progressDialogFragment.show(getFragmentManager(), "");
                            JSONObject body = new JSONObject();
                            try {
                                Movil movilAEliminar = MyApplication.getInstance().getPreferences().getUsuario().getMoviles().get(spinner.getSelectedItemPosition());
                                body.put(getString(R.string._patente), movilAEliminar.getPatente());
                            } catch (JSONException e) { e.printStackTrace(); }
                            //new RestCustomer(this, url, body, Constantes.POST, MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth(), getOnRequestCustomerFinish()).execute();
                            RestCustomerVolley.addQueue(Request.Method.POST, DELETE_CAR, body, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
                                @Override
                                public void onOkRestCustomerResult(String object) {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    MyApplication.getInstance().getPreferences().getUsuario().getMoviles().remove((Movil)spinner.getSelectedItem());
                                    cargarSpinnerMoviles();
                                    Toast.makeText(getContext(), R.string.eliminadoCorrectamente, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onErrorRestCustomerResult(String result) {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    Toast.makeText(getContext(), R.string.noEliminado, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }).show(getFragmentManager(), "");
                }
            }
        });
    }

    private void cargarSpinnerMoviles(){
        //List<String> listVehiculos = new ArrayList<>();
        /*for(Movil movil: MyApplication.getInstance().getPreferences().getUsuario().getMoviles()){
            listVehiculos.add(movil.toString());
        }*/
        ArrayAdapter<Movil> adapterVehiculos = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, MyApplication.getInstance().getPreferences().getUsuario().getMoviles());
        spinner.setAdapter(adapterVehiculos);
    }
    private void cancelarViajesActivos(){
        RestCustomerVolley.addQueue(Request.Method.POST, END_TRAVEL, null, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                checkLocationSettings();
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                checkLocationSettings();
            }
        });
        MyApplication.getInstance().setConductorInRequest(false);
        MyApplication.getInstance().setConductorEnViaje(false);
    }

    private void checkPermissions(){
        if ( ContextCompat.checkSelfPermission( getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            requestPermissions(
                    new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION  },
                    REQUEST_CHECK_LOCATION_PERMISSIONS );
        }else{
            startServicesAndLocationRequests();
        }
    }

    private void checkLocationSettings(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        LocationSettingsRequest locationSettingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    checkPermissions();

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                resolvable.startResolutionForResult(
                                        getActivity(),
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            }
        });
    }

    public void updateLastLocationSendedTime(){
        updateLastLocationSended = new UpdateLastLocationSended(){
            @Override
            public void actualizarUbicacion(Date date) {
                lastLocationSended.setText(Formatter.formatDateTime(date));
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CHECK_SETTINGS: {
                //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
                if (resultCode == Activity.RESULT_OK)
                    checkPermissions();
                break;
            }
            case REQUEST_CHECK_LOCATION_PERMISSIONS:{
                if (resultCode == Activity.RESULT_OK)
                    startServicesAndLocationRequests();
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CHECK_LOCATION_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    startServicesAndLocationRequests();
                break;
        }
    }

    public void startServicesAndLocationRequests(){
        buttonConnect.setEnabled(false);
        linearLayoutConectarse.setVisibility(View.VISIBLE);
        //startNotification();
        MyApplication.getInstance().setChoferConectado(true);
        buttonConnect.setEnabled(false);
        getActivity().startService(new Intent(getActivity(), MyFirebaseInstanceIDService.class));
        getActivity().startService(new Intent(getActivity(), MyFirebaseMessagingService.class));
        getActivity().startService(new Intent(getActivity(), LocationService.class));
    }

    private void startNotification(){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + 911));
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, callIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews notificationView = new RemoteViews(getActivity().getPackageName(),
                R.layout.notification_panic_button);
        notificationView.setOnClickPendingIntent(R.id.imageCallId, pendingIntent);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getActivity())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.mipmap.ic_call_white_24dp)
                .setCustomContentView(notificationView)
                .setDefaults(Notification.FLAG_NO_CLEAR)
                .setOngoing(true);


        mNotificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_ID_PANIC_BUTTON, notification.build());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        MyApplication.getInstance().setLocationRequest(locationRequest);
        if(!MyApplication.getInstance().isChoferConectado()) {
            buttonConnect.setEnabled(true);
            linearLayoutConectarse.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public interface UpdateLastLocationSended{
        void actualizarUbicacion(Date date);
    }

    @Override
    public void onResume() {
        super.onResume();
        isFragmentActive = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        isFragmentActive = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isFragmentActive = false;
    }
}
