package com.system.sumo.retaxi.dao.sqlite;

import android.database.Cursor;

import com.google.android.gms.maps.model.LatLng;
import com.system.sumo.retaxi.bo.BaseDeDatosBo;
import com.system.sumo.retaxi.dao.CondicionDao;
import com.system.sumo.retaxi.dao.IViajeDao;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.PlaceAutoComplete;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.dto.mapping.ViajeMapping;
import com.system.sumo.retaxi.utils.Formatter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.system.sumo.retaxi.utils.Formatter.convertToDateTime;

/**
 * Created by usuario on 21/09/2016.
 */
public class ViajeSqliteDao extends GenericSqliteDao implements IViajeDao {
    @Override
    public Integer create(Viaje entity) throws Exception {
        table = ViajeMapping.TABLA;
        insertMap.put(ViajeMapping.DESDE, entity.getDesde());
        insertMap.put(ViajeMapping.HASTA, entity.getHasta());
        insertMap.put(ViajeMapping.FECHA, entity.getFechaHora());
        insertMap.put(ViajeMapping.LATITUD_DESDE, entity.getPuntoDesde().latitude);
        insertMap.put(ViajeMapping.LONGITUD_DESDE, entity.getPuntoDesde().longitude);
        if (entity.getPuntoHastaLocationPoint() != null) {
            insertMap.put(ViajeMapping.LATITUD_HASTA, entity.getPuntoHasta().latitude);
            insertMap.put(ViajeMapping.LONGITUD_HASTA, entity.getPuntoHasta().longitude);
        }
        if (entity.getTarifa() != null)
            insertMap.put(ViajeMapping.MOTNO, entity.getTarifa().getPrecio_viaje());
        insertMap.put(ViajeMapping.USER_TYPE, MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario());
        super.create();
        return null;
    }

    @Override
    public Viaje recoveryByID(Integer integer ) throws Exception {
        return null;
    }

    @Override
    public List<Viaje> recoveryAll( ) throws Exception {
        return null;
    }

    public List<PlaceAutoComplete> recoveryDistinct() throws Exception {
        String sql=
                "SELECT consulta.DIRECCION, consulta." + ViajeMapping.LATITUD_DESDE
                        + ", consulta." + ViajeMapping.LONGITUD_DESDE
                        + " FROM ("
                + "SELECT " + ViajeMapping.DESDE + " AS DIRECCION, "
                        + ViajeMapping.LATITUD_DESDE + ", "
                        + ViajeMapping.LONGITUD_DESDE + ", "
                        + ViajeMapping.LATITUD_HASTA + ", "
                        + ViajeMapping.LONGITUD_HASTA + ", "
                        + ViajeMapping.FECHA
                    + " FROM " + ViajeMapping.TABLA
                    + " WHERE " + ViajeMapping.USER_TYPE + " = " + MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario()
                    + " GROUP BY " + ViajeMapping.DESDE
                    + " UNION "
                + "SELECT " + ViajeMapping.HASTA + " AS DIRECCION, "
                        + ViajeMapping.LATITUD_DESDE + ", "
                        + ViajeMapping.LONGITUD_DESDE + ", "
                        + ViajeMapping.LATITUD_HASTA + ", "
                        + ViajeMapping.LONGITUD_HASTA + ", "
                        + ViajeMapping.FECHA
                    + " FROM " + ViajeMapping.TABLA
                    + " WHERE " + ViajeMapping.USER_TYPE + " = " + MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario()
                    + " GROUP BY " + ViajeMapping.HASTA
                + ") AS consulta ORDER BY fecha "
                + " LIMIT 10 ";
        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
        Cursor cursor = bd.getWritableDatabase().rawQuery(sql, null);
        return getDirecciones(cursor);
    }

    public List<PlaceAutoComplete> recoveryDesde( ) throws Exception {
        String sql=
            "SELECT " + ViajeMapping.DESDE + " AS DIRECCION, "
            + ViajeMapping.LATITUD_DESDE + ", "
            + ViajeMapping.LONGITUD_DESDE
            + " FROM " + ViajeMapping.TABLA
            + " WHERE " + ViajeMapping.USER_TYPE + " = " + MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario()
            + " GROUP BY " + ViajeMapping.DESDE
            + " ORDER BY fecha desc "
            + " LIMIT 10 ";
        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
        Cursor cursor = bd.getWritableDatabase().rawQuery(sql, null);
        return getDirecciones(cursor);
    }

    public List<PlaceAutoComplete> recoveryHasta( ) throws Exception {
        String sql=
            "SELECT " + ViajeMapping.HASTA + " AS DIRECCION, "
            + ViajeMapping.LATITUD_HASTA + " AS " + ViajeMapping.LATITUD_DESDE + ", "
            + ViajeMapping.LONGITUD_HASTA + " AS " + ViajeMapping.LONGITUD_DESDE
            + " FROM " + ViajeMapping.TABLA
            + " WHERE " + ViajeMapping.USER_TYPE + " = " + MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario()
            + " GROUP BY " + ViajeMapping.HASTA
            + " ORDER BY fecha desc "
            + " LIMIT 10 ";
        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
        Cursor cursor = bd.getWritableDatabase().rawQuery(sql, null);
        return getDirecciones(cursor);
    }

    private List<PlaceAutoComplete> getDirecciones(Cursor cursor){
        List<PlaceAutoComplete> direcciones = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    direcciones.add(getDireccion(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return direcciones;
    }

    private PlaceAutoComplete getDireccion(Cursor cursor){
        PlaceAutoComplete placeAutoComplete = new PlaceAutoComplete();
        int indexDireccion = cursor.getColumnIndex("DIRECCION");
        if (indexDireccion >= 0) {
            placeAutoComplete.setPlaceDesc(cursor.getString(indexDireccion));
        }
        int indexLatitudDesde = cursor.getColumnIndex(ViajeMapping.LATITUD_DESDE);
        int indexLongitudDesde = cursor.getColumnIndex(ViajeMapping.LONGITUD_DESDE);
        if (indexLatitudDesde >= 0 && indexLongitudDesde >= 0) {
            placeAutoComplete.setPunto(new LatLng(cursor.getDouble(indexLatitudDesde), cursor.getDouble(indexLongitudDesde)));
        }
        return placeAutoComplete;
    }

    private List<Viaje> getViajes(Cursor cursor){
        List<Viaje> viajes = new ArrayList<>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    viajes.add(mappingToDto(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return viajes;
    }

    @Override
    public void update(Viaje entity) throws Exception {

    }

    @Override
    public void delete(Viaje entity) throws Exception {

    }

    @Override
    public void delete(Integer integer) throws Exception {

    }

    public static Viaje mappingToDto(Cursor cursor){
        Viaje viaje = new Viaje();
        viaje.setId(cursor.getColumnIndex(ViajeMapping.ID));
        int indexDesde = cursor.getColumnIndex(ViajeMapping.DESDE);
        if (indexDesde >= 0) {
            viaje.setDesde(cursor.getString(indexDesde));
        }
        int indexFecha = cursor.getColumnIndex(ViajeMapping.FECHA);
        if (indexFecha >= 0) {
            String fecha = cursor.getString(indexFecha);
            try {
                viaje.setFecha(convertToDateTime(fecha));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        int indexHasta = cursor.getColumnIndex(ViajeMapping.HASTA);
        if (indexHasta >= 0) {
            viaje.setHasta(cursor.getString(indexHasta));
        }
        int indexLatitudDesde = cursor.getColumnIndex(ViajeMapping.LATITUD_DESDE);
        int indexLongitudDesde = cursor.getColumnIndex(ViajeMapping.LONGITUD_DESDE);
        if (indexLatitudDesde >= 0 && indexLongitudDesde >= 0) {
            LocationPoint locationPoint = new LocationPoint();
            double[] location = {cursor.getDouble(indexLatitudDesde), cursor.getDouble(indexLongitudDesde)};
            locationPoint.setCoordinates(location);
            //LatLng l = new LatLng(cursor.getDouble(indexLatitudDesde), cursor.getDouble(indexLongitudDesde));
            viaje.setPuntoDesdeLocationPoint(locationPoint);
        }

        int indexLatitudHasta = cursor.getColumnIndex(ViajeMapping.LATITUD_HASTA);
        int indexLongitudHasta = cursor.getColumnIndex(ViajeMapping.LONGITUD_HASTA);
        if (indexLatitudHasta >= 0 && indexLongitudHasta >= 0) {
            LocationPoint locationPoint = new LocationPoint();
            double[] location = {cursor.getDouble(indexLatitudHasta), cursor.getDouble(indexLongitudHasta)};
            locationPoint.setCoordinates(location);
            //LatLng l = new LatLng(cursor.getDouble(indexLatitudDesde), cursor.getDouble(indexLongitudDesde));
            viaje.setPuntoHastaLocationPoint(locationPoint);
        }


        int indexMonto = cursor.getColumnIndex(ViajeMapping.MOTNO);
        if (indexMonto >= 0) {
            viaje.setMonto(cursor.getString(indexMonto));
        }
        return viaje;
    }

    @Override
    public List<Viaje> recoveryByPeriodo(String fechaDesde, String fechaHasta) throws Exception {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(Formatter.convertToDate(fechaHasta)); // Configuramos la fecha que se recibe
        calendar.add(Calendar.HOUR, 23);  // numero de horas a añadir, o restar en caso de horas<0
        calendar.add(Calendar.MINUTE, 59);

        fechaHasta = Formatter.formatDateWs(calendar.getTime());
        String sql = "SELECT * FROM " + ViajeMapping.TABLA
                + " WHERE " + ViajeMapping.FECHA
                + " BETWEEN '" + fechaDesde + "' AND '" + fechaHasta + "'"
                + " AND " + ViajeMapping.USER_TYPE + " = " + MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario()
                + " ORDER BY " + ViajeMapping.FECHA + " DESC";
        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
        Cursor cursor = bd.getWritableDatabase().rawQuery(sql, null);
        return getViajes(cursor);
    }

    @Override
    public List<Viaje> recoveryDiferidos() throws Exception {
        listTables.add(ViajeMapping.TABLA);
        parameterWhere.put(ViajeMapping.TIPE, new CondicionDao("=", Viaje.TravelTypes.DIFERIDO.type()));
        Cursor cursor = recoveryData();
        return getViajes(cursor);
    }
}
