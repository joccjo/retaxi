package com.system.sumo.retaxi.view.dialogs;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.travels.ViajesArrayAdapter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by eMa on 29/11/2016.
 */

public class TravelProgrammerDialogFragment extends Fragment {
    private List<Viaje> viajes;
    private ListView listViewViajes;
    private TextView txtFechaDesde;
    private TextView txtFechaHasta;
    private EditText textViewNumberOfRepeat;
    private Spinner spiinerRepeat;
    private RadioButton radioButtonRepeatNever;
    private RadioButton radioButtonRepeatEach;
    private RadioButton radioButtonThe;
    private RadioButton radioButtonAfterOf;
    private RadioButton radioButtonNeverFinish;
    private Date fechaDesde;
    private Date fechaHasta;
    private static final int DIALOG_DATE_THE = 0;
    private static final int DIALOG_DATE_AFTER_OF = 1;
    static ViajeSelected viajeSelected;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.lyt_travel_programmer_dialog_fragment, container, false);
        initComponents(view);
        enableOrDisableViewsProgrammer(view, false);
        initVar();
        loadViajes();
        return view;
    }

    public static TravelProgrammerDialogFragment newInstance(Bundle b, ViajeSelected listener){
        TravelProgrammerDialogFragment travelProgrammerDialogFragment = new TravelProgrammerDialogFragment();
        viajeSelected = listener;
        travelProgrammerDialogFragment.setArguments(b);
        return travelProgrammerDialogFragment;
    }

    /*@Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(R.string.viajes);
        View view = View.inflate(getActivity(), R.layout.fragment_viajes_tab, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        initVar();
        loadViajes();
        return alertDialogBuilder.show();
    }
*/
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void loadViajes(){
        ViajeBo viajeBo = new ViajeBo();
        try {
            String fechaDesde = Formatter.formatDate(txtFechaDesde.getText().toString());
            String fechaHasta = Formatter.formatDate(txtFechaHasta.getText().toString());

             // Devuelve el objeto Date con las nuevas horas añadidas

            viajes = viajeBo.recoveryByPeriodo(fechaDesde, fechaHasta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(viajes != null) {
            listViewViajes.setAdapter(new ViajesArrayAdapter(getActivity(), R.layout.listview_item_viajes, viajes));
            if(MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario() == Usuario.TIPO_PASAJERO){
                listViewViajes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                        SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.repetirViaje), new SimpleDialogFragment.OkCancelListener() {
                            @Override
                            public void onCancelSelected() {

                            }

                            @Override
                            public void onOkSelected() {
                                if (ViajeBo.existActiveTravel())
                                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.activeTravel) ,getString(R.string.ups), new SimpleDialogFragment.OkListener() {
                                        @Override
                                        public void onOkSelected() {

                                        }
                                    }).show(getFragmentManager(),  "");
                                    else
                                        viajeSelected.onViajeSelected(viajes.get(i));
                                //dismiss();
                                /*PedirTaxiFragment pedirTaxiFragment = PedirTaxiFragment.newInstance(viajes.get(i));
                                getFragmentManager().beginTransaction().replace(R.id.content_frame, pedirTaxiFragment).commit();*/
                            }
                        }).show(getFragmentManager(), "");
                    }
                });
            }
        }
    }

    private void enableOrDisableViewsProgrammer(View view, boolean enable){
        textViewNumberOfRepeat.setEnabled(enable);
        spiinerRepeat.setEnabled(enable);
        LinearLayout layout = view.findViewById(R.id.idRadioGroupFinish);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            child.setEnabled(enable);
        }
    }

    private void initComponents(final View view){
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.back_button);
        TextView textView = toolbar.findViewById(R.id.toolbar_title);
        textView.setText(R.string.periodicity);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        textViewNumberOfRepeat = view.findViewById(R.id.textViewNumberOfRepeat);
        spiinerRepeat = view.findViewById(R.id.spiinerRepeat);
        radioButtonThe = view.findViewById(R.id.radioButtonThe);
        radioButtonNeverFinish = view.findViewById(R.id.radioButtonNeverFinish);
        radioButtonAfterOf = view.findViewById(R.id.radioButtonAfterOf);
        radioButtonRepeatEach = view.findViewById(R.id.radioButtonRepeatEach);
        radioButtonRepeatEach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableOrDisableViewsProgrammer(view, true);
            }
        });
        radioButtonRepeatNever = view.findViewById(R.id.radioButtonRepeatNever);
        radioButtonRepeatNever.setChecked(true);
        radioButtonRepeatNever.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableOrDisableViewsProgrammer(view, false);
            }
        });

        this.txtFechaDesde = view.findViewById(R.id.txtFechaDesde);
        this.txtFechaDesde.setKeyListener(null);
        this.txtFechaDesde.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_DATE_THE);
            }
        });

        //txtFechaHasta
        this.txtFechaHasta = view.findViewById(R.id.txtFechaHasta);
        this.txtFechaHasta.setKeyListener(null);
        this.txtFechaHasta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_DATE_AFTER_OF);
            }
        });
        listViewViajes = view.findViewById(R.id.listView);
    }

    private void initVar(){
        Calendar calendar = Calendar.getInstance();

        this.fechaHasta = calendar.getTime();
        calendar.add(Calendar.MONTH, -1);
        this.fechaDesde = calendar.getTime();

        this.txtFechaDesde.setText(Formatter.formatDate(fechaDesde));
        this.txtFechaHasta.setText(Formatter.formatDate(fechaHasta));
    }

    private void getDialogSeleccionarFecha(final int tipoFecha){
        Date fecha;
        if (tipoFecha == DIALOG_DATE_THE)
            fecha = this.fechaDesde;
        else
            fecha = this.fechaHasta;

        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialogFecha = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year, monthOfYear, dayOfMonth);

                if (tipoFecha == DIALOG_DATE_THE){
                    fechaDesde = cal.getTime();
                    String fechaDesdeString = Formatter.formatDate(fechaDesde);
                    txtFechaDesde.setText(fechaDesdeString);
                }else{
                    fechaHasta = cal.getTime();
                    String fechaHastaString = Formatter.formatDate(fechaHasta);
                    txtFechaHasta.setText(fechaHastaString);
                }
                loadViajes();
            }
        }, year, month, day);

        //_ventas = ventaBo.recoveryVentas(this.fechaDesde, this.fechaHasta);

        dialogFecha.show();
    }

    public interface ViajeSelected {
        void onViajeSelected(Viaje viaje);
    }
}
