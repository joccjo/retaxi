package com.system.sumo.retaxi.view.dialogs.generics;




import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.view.KeyEvent;

public class ProgressDialogFragment extends BaseDialogFragment {

	private static boolean isCancelable;
	protected CancelProgressListener mListener;
	public static ProgressDialogFragment newInstance(String msjProgress){
		isCancelable = false;
		
		Bundle bundle = new Bundle();
		bundle.putString(KEY_MESSAGE, msjProgress);
		
		ProgressDialogFragment dialogFragment = new ProgressDialogFragment();
		dialogFragment.setArguments(bundle);
		dialogFragment.setCancelable(false);
		return dialogFragment;
	}
	/**
	 * ProgressDialog que puede ser cancelado, el listener sirve para devolver el callback de cuando se selecciona cancelar
	 */
	public static ProgressDialogFragment newInstance(String msjProgress, CancelProgressListener listener){
		
		isCancelable = true;
		
		Bundle bundle = new Bundle();
		bundle.putString(KEY_MESSAGE, msjProgress);
		
		ProgressDialogFragment dialogFragment = new ProgressDialogFragment();
		dialogFragment.setArguments(bundle);
		dialogFragment.mListener= listener;
		dialogFragment.setCancelable(false);
		return dialogFragment;
	}
	@Override
	public void dismiss() {
		// TODO Auto-generated method stub
		super.dismissAllowingStateLoss();
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		mMessage = getArguments().getString(KEY_MESSAGE);
		
		ProgressDialog dialog = new ProgressDialog(getActivity());
		
		dialog.setMessage(mMessage);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		dialog.setIndeterminate(true);
		
		if (isCancelable){
			dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar ", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					mListener.onCancelProgress();
					
				}
			});
			
		}
		dialog.setOnKeyListener(new OnKeyListener() {
			
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				return ((keyCode == KeyEvent.KEYCODE_BACK)) ;
				//return true pretend we've processed it else pass on to be processed as normal
			}
		});
		return dialog;
	}	
	public interface CancelProgressListener{
		void onCancelProgress();
	}	
}
