package com.system.sumo.retaxi.dao;

public class CondicionDao {

	private String operator;
	private Object value;
	public static String EQUALS = "=";
	
	public CondicionDao(){
		
	}
	
	public CondicionDao(String operator, Object value){
		this.operator = operator;
		this.value = value;
	}
	
	public String getOperator() {
		return this.operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Object getValue() {
		return this.value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
}
