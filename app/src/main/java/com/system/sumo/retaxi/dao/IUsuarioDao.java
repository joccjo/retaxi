package com.system.sumo.retaxi.dao;

import android.content.Context;

import com.system.sumo.retaxi.dto.Usuario;

import java.util.List;

/**
 * Created by usuario on 21/09/2016.
 */
public interface IUsuarioDao extends IGenericDao<Usuario, Integer> {
    List<Usuario> recoveryByUserType(int userType) throws Exception;
    void deleteByUserType(int userType) throws Exception;
}
