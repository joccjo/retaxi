package com.system.sumo.retaxi.bo;

import android.content.Context;

import com.google.android.gms.maps.model.Marker;
import com.system.sumo.retaxi.dao.DaoFactory;
import com.system.sumo.retaxi.dao.IViajeDao;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.utils.Formatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by usuario on 23/10/2016.
 */

public class ViajeBo {
    DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.SQLITE);
    IViajeDao iViajeDao = daoFactory.getViajeDao();

    public void create(Viaje v) throws Exception{
        iViajeDao.create(v);
    }

    public static boolean existActiveTravel(){
        if (MyApplication.getInstance().getPreferences().getViaje() != null &&
                !MyApplication.getInstance().getPreferences().getViaje().getEstado().equals(Viaje.TRAVEL_STATE_ACCEPTED) &&
                        !MyApplication.getInstance().getPreferences().getViaje().getEstado().equals(Viaje.TRAVEL_STATE_STARTED ))
            return false;
        return true;
    }

    public void createDeferredTravel(String dateTime, double latOrigin, double lngOrigin, double latDestination, double longDestination, String originDescription, String destinationDescription){
        Viaje travel = new Viaje();
        try {
            travel.setFecha(Formatter.convertToDateTimeText(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        travel.setDesde(originDescription);
        travel.setHasta(destinationDescription);
        travel.setPuntoDesdeLocationPoint(new LocationPoint("point", new double[]{latOrigin, lngOrigin}));
        travel.setPuntoHastaLocationPoint(new LocationPoint("point", new double[]{latDestination, longDestination}));
        travel.setTipo(Viaje.TravelTypes.DIFERIDO.type());
        try {
            create(travel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Viaje> filterByDate(List<Viaje> travels, String startDateString, String endDateString) throws  Exception{
        List<Viaje> travelsFiltered = new ArrayList<>();
        Date startDate = Formatter.convertStringToDate(startDateString);
        Date endDate = Formatter.convertStringToDate(endDateString);
        for (Viaje travel: travels){
            Date date = Formatter.convertToDateTimeDjangoTravel(travel.getFechaHora());
            if (date.after(startDate) && date.before(endDate))
                travelsFiltered.add(travel);
        }
        return travelsFiltered;
    }

    public static void createTravelRequestSended(int travelId, String from, String to, Marker markerFrom, Marker markerTo){
        Viaje travel = new Viaje();
        travel.setId(travelId);
        travel.setEstado(Viaje.TRAVEL_STATE_PENDING);
        travel.setDesde(from);
        travel.setHasta(to);
        travel.setPuntoDesdeLocationPoint(new LocationPoint("type", new double[]{markerFrom.getPosition().latitude, markerFrom.getPosition().longitude}));
        if (!to.equals(""))
            travel.setPuntoHastaLocationPoint(new LocationPoint("type", new double[]{markerTo.getPosition().latitude, markerTo.getPosition().longitude}));
        PreferencesBo.saveTravelInPreferences(travel);
    }

    public List<Viaje> recoveryByPeriodo(String fechaDesde, String fechaHasta) throws Exception{
        return iViajeDao.recoveryByPeriodo(fechaDesde, fechaHasta);
    }

    public List<Viaje> recoveryDiferidos() throws Exception{
        return iViajeDao.recoveryDiferidos();
    }

}
