package com.system.sumo.retaxi.view.pasajero;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.view.dialogs.generics.BaseDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import static com.system.sumo.retaxi.utils.Constantes.FINALIZAR_VIAJE_EXITO;
import static com.system.sumo.retaxi.utils.Constantes.NO_SE_PUEDE_CALIFICAR_TODAVIA;
import static com.system.sumo.retaxi.utils.ServicesRoutes.QUALIFY_DRIVER;
import static com.system.sumo.retaxi.view.dialogs.VerTarifaDialogFragment.KEY_EXTRA_VIAJE;

/**
 * Created by usuario on 06/11/2016.
 */

public class CalificarDialogFragment extends BaseDialogFragment {
    TextView txtNombre;
    TextView apellido;
    TextView vehiculo;
    ProgressDialogFragment progressDialogFragment;
    static FinalizarCalificacionListener finalizarCalificacionListener;
    public static CalificarDialogFragment newInstance(Bundle b, FinalizarCalificacionListener listener){
        CalificarDialogFragment calificarDialogFragment = new CalificarDialogFragment();
        finalizarCalificacionListener = listener;
        calificarDialogFragment.setArguments(b);
        return calificarDialogFragment;
    }

    public interface FinalizarCalificacionListener {
        void onFinalizarCalificacionListener();
    }

    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.lyt_calificar_dialog_fragment, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        return alertDialogBuilder.show();
    }

    private void initComponents(View view) {
        Bundle b = getArguments();
        final Viaje viaje = (Viaje) b.getSerializable(KEY_EXTRA_VIAJE);
        TextView tvApellido = ((TextView) view.findViewById(R.id.tvApellido));
        tvApellido.setText(viaje.getConductor().getApellido());
        TextView tvNombre = ((TextView) view.findViewById(R.id.tvNombre));
        tvNombre.setText(viaje.getConductor().getNombre());
        TextView tvVehiculo = ((TextView) view.findViewById(R.id.tvVehiculo));
        if (viaje.getMovil() != null)
            tvVehiculo.setText(viaje.getMovil().toString());
        final RatingBar ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
        ratingBar.setRating(viaje.getConductor().getReputation() / viaje.getConductor().getViajes_realizados());
        final EditText etComentario = view.findViewById(R.id.editTextId);

        Button button = (Button) view.findViewById(R.id.buttonCalificar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*finalizarCalificacionListener.onFinalizarCalificacionListener(ratingBar.getRating());
                dismiss();*/
                progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.calificando));
                progressDialogFragment.show(getActivity().getSupportFragmentManager(), "calificando");
                JSONObject body = new JSONObject();
                try {
                    body.put(getString(R.string.parameterWsTravel), viaje.getId());
                    body.put(getString(R.string.parameterWsConductor), viaje.getConductor().getDni());
                    body.put(getString(R.string.parameterWsPuntajeConductor), ratingBar.getRating());
                    body.put(getString(R.string.parameterWsPuntajeBase), "5");
                    body.put(getString(R.string.parameterWsComentario), etComentario.getText());
                } catch (JSONException e) { e.printStackTrace(); }
                RestCustomerVolley.addQueue(Request.Method.POST, QUALIFY_DRIVER, body, true, getOnRequestCustomerFinish());
            }
        });

        final ImageView imageView = view.findViewById(R.id.imgViewProfile);
        imageView.getLayoutParams().height = 200;
        imageView.getLayoutParams().width = 200;
        imageView.requestLayout();
        if (!viaje.getConductor().getUrlImagen().equals(""))
            Picasso.get().load(viaje.getConductor().getUrlImagen())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Picasso.get().load(viaje.getConductor().getUrlImagen())
                                    .into(imageView);
                        }
                    });
    }

    private RestCustomerVolley.OnRequestRestCustomerFinish getOnRequestCustomerFinish(){
        return  new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String objectResponse) {
                try{
                    Gson gson = new Gson();
                    ResultWsCall object = gson.fromJson(objectResponse, ResultWsCall.class);
                    switch (object.getRespuesta()){
                        case NO_SE_PUEDE_CALIFICAR_TODAVIA:
                            progressDialogFragment.dismissAllowingStateLoss();
                            SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.viajeNoIniciado)).show(getFragmentManager(), "");
                            break;
                        case FINALIZAR_VIAJE_EXITO:
                            progressDialogFragment.dismissAllowingStateLoss();
                            dismiss();
                            finalizarCalificacionListener.onFinalizarCalificacionListener();
                            break;
                        default:
                            progressDialogFragment.dismissAllowingStateLoss();SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.noSePudoCalificar)).show(getFragmentManager(), "");
                            break;
                    }
                }catch (Exception e){
                    progressDialogFragment.dismissAllowingStateLoss();
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.noSePudoCalificar)).show(getFragmentManager(), "");
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                Toast.makeText(getActivity(),getString(R.string.noSePudoCalificar), Toast.LENGTH_SHORT).show();
            }
        };
    }
}
