package com.system.sumo.retaxi.ws;

public class GenericWs {

    public void setAdditionalData(String telephone, String sex, final String birthDate, final GenericWsResponse genericWsResponse){}

    public void associateDriver(String dni, final GenericWsResponse genericWsResponse){}

    public interface GenericWsResponse {
        void onOkResult(Object result);
        void onErrorResult();
    }
}
