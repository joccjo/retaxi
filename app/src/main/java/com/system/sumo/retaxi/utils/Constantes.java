package com.system.sumo.retaxi.utils;

/**
 * Created by usuario on 21/08/2016.
 */
public class Constantes {
    public static String DIRECCION_WS = "";
    public static String HEADER_CONTENT_TYPE = "Content-Type";
    public static String HEADER_ACCEPT = "Accept";
    public static String HEADER_JSON = "application/json";
    public static String HEADER_AUTHORIZATION = "Authorization";
    public static String ACCESS_TOKEN = "?access_token=";
    public static String PARAMETER_DNI = "&dni=";
    public static int NUMBER_STARS_RATING = 5;
    public static String KEY_TOKEN_OAUTH = "KEY_TOKEN_OAUTH";
    public static String KEY_USER_TYPE = "KEY_USER_TYPE";



    public static int TIEMPO_DISPONIBLE_PARA_ACEPTAR_SOLICITUD = 150;// 100 10 segundos
    public static int TIEMPO_ESPERA_PEDIR_TAXI = 20000;

    public final static String VIAJE = "viaje";
    public final static String TARIFA = "tarifa";
    public final static String IS_DIFERIDO ="is_diferido";
    public final static String MODELO = "model";
    public final static String FECHA_HORA = "fecha_hora";
    public final static String TRAVEL_ID = "viaje";
    public final static String POSITION = "posicion";
    public final static String FIRST_NAME = "first_name";
    public final static String LAST_NAME = "last_name";
    public final static String ORIGIN_DESCRIPTION = "origen_descripcion";
    public final static String DESTINATION_DESCRIPTION = "destino_descripcion";
    public final static String LAST_LOCATION = "last_location";
    public final static String LOCATION_POINT_TYPE = "type";
    public final static String ORIGIN = "origen";
    public final static String DESTINATION = "destino";
    public final static String SOCIAL_THUMB = "social_thumb";
    public final static String DNI = "dni";
    public final static String ULTIMA_UBICACION = "ultima_ubicacion";
    public final static String ERROR = "Error";
    public final static String UBICACION_DESDE = "ubicacion_desde";
    public final static String UBICACION_HASTA = "ubicacion_hasta";
    public final static String TOKEN = "token";
    public final static String MOVIL = "movil";
    public final static String COORDINATES = "coordinates";
    public final static String REPUTACION = "reputacion";
    public final static String MENSAJE_CHAT = "data";
    public final static String MENSAJE = "mensaje";
    public final static String FECHA_CHAT = "fecha";
    public final static String MESSAGE_TYPE_PARAM = "tipo_mensaje";
    public final static String TITLE = "titulo";

    /*firebase responsess*/
    public final static String MESSAGE_TYPE = "type";
    public final static String MESSAGE_TYPE_TAXI_REQUEST = "taxi_request";
    public final static String MESSAGE_TYPE_REQUEST_ACCEPTED = "request_accepted";
    public final static String MESSAGE_TYPE_CONDUCTORES_NO_DISPONIBLES = "conductores_no_disponibles";
    public final static String MESSAGE_TYPE_CANCELADO_X_CONDUCTOR = "cancelado_x_conductor";
    public final static String MESSAGE_TYPE_ERROR_AL_INTENTAR_INGRESAR = "error al intentar ingresar";
    public final static String MESSAGE_TYPE_CANCELLED_BY_RIDER = "cancelado_x_pasajero";
    public final static String MESSAGE_TYPE_CANCELADO_X_BASE = "cancelado_x_base";
    public final static String MESSAGE_TYPE_UBICACION_CONDUCTOR = "ubicacion_conductor";
    public final static String MESSAGE_TYPE_CHAT = "chat";


    /*results call ws*/
    public final static int ERROR_CANCELA_VIAJE= -1;
    public final static int ERROR_CANCELA_VIAJE_= 10;
    public final static int NO_RESPONDIO_ERROR = 0;
    public final static int ERROR_INICIAR_VIAJE = 11;
    public final static int ERROR_FINALIZAR_VIAJE = 12;
    public final static int CONDUCTORES_NO_DISPONIBLES= 13;
    public final static int NO_SE_PUEDE_CALIFICAR_TODAVIA= 14;
    public final static int PASAJERO_A_BORDO_EXITO= 15;
    public final static int VIAJE_CANCELADO_POR_PASAJERO = 19;
    public final static int CANCELA_VIAJE_EXITO = 20;
    public final static int INICIA_VIAJE_EXITO = 21;
    public final static int FINALIZAR_VIAJE_EXITO=22;
    public final static int NO_TIENE_DNI = 100;
    public final static int CONDUCTOR_NO_REGISTRADO = 200;
    public final static int USUARIO_INVALIDO = 300;//el conductor esta asociado a otro usuario
    public final static int SOLICITUD_ACEPTADA_CORRECTAMENTE = 400;
    public final static int VIAJE_ACEPTADO_POR_OTRO_CONDUCTOR = 500;


    public final static String GET = "GET";
    public final static String POST = "POST";
    public final static String PATCH = "PATCH";

}
