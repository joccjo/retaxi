package com.system.sumo.retaxi.utils.screen;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

/**
 * Created by usuario on 03/09/2016.
 */
public class UpdateScreenOnOfService extends Service {
    @Override
    public void onCreate() {
        super.onCreate();
        // REGISTER RECEIVER THAT HANDLES SCREEN ON AND SCREEN OFF LOGIC
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenOnOffReceiver(getApplicationContext());
        registerReceiver(mReceiver, filter);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Bundle b = intent.getExtras();
        String itsFromBroadCast = b.getString(ScreenOnOffReceiver.SCREEN_STATE_EXTRA);
        if (itsFromBroadCast != null) {
            runCall();
        }
        return null;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        onBind(intent);
    }

    public void runCall(){
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel://911"));
        intentCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            startActivity(intentCall);
        }
    }

}
