package com.system.sumo.retaxi.view.chat;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.asynkTasks.RestCustomer;
import com.system.sumo.retaxi.bo.ChatMessageBo;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.services.ConnectionReceiver;
import com.system.sumo.retaxi.services.MyFirebaseMessagingService;
import com.system.sumo.retaxi.services.NetworkSchedulerService;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.ws.MessageWs;
import com.system.sumo.retaxi.view.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

import static com.system.sumo.retaxi.utils.Avisos.ID_NOTIFICATION_CHAT;
import static com.system.sumo.retaxi.utils.Constantes.SOLICITUD_ACEPTADA_CORRECTAMENTE;


public class ChatActivity extends AppCompatActivity{
	public static boolean active = false;
	private EditText messageET;
	private RecyclerView messagesContainer;
	private ImageView sendBtn;
	public ChatAdapter adapter;
	private List<ChatMessage> chatHistory;
    AppCompatCheckBox checkBox;
	//public static Activity myDialog;
	public static FinishMyDialogChatListener finishListener;
	private static String EXTRA_VIAJE = "EXTRA_VIAJE";
	private static String EXTRA_USUARIO = "EXTRA_USUARIO";
    ChatMessageBo chatMessageBo = new ChatMessageBo();
    MessageStateSendedChange messageStateSendedChange;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lyt_chat_activity);
		getFinishListenner();
		//myDialog = ChatActivity.this;
		initControls();
	}

	@Override
	public void onBackPressed() {
		startActivity(new Intent(this, MainActivity.class));
		this.finish();
	}

	private void initControls() {
		messagesContainer = findViewById(R.id.messagesContainer);
        messagesContainer.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
		messageET = findViewById(R.id.messageEdit);
		sendBtn = findViewById(R.id.chatSendButton);
        ActionBar abar = getSupportActionBar();
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.chat_actionbar, null);
        TextView tittle = mCustomView.findViewById(R.id.title_text);
        tittle.setText(getString(R.string.app_name));
        TextView subTitle= mCustomView.findViewById(R.id.sub_title_text);
        subTitle.setText(getString(R.string.online));

        abar.setCustomView(mCustomView);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setDisplayShowHomeEnabled(true);
		loadMessages();

		sendBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String messageText = messageET.getText().toString();
				if (TextUtils.isEmpty(messageText)) {
					return;
				}

				ChatMessage chatMessage = new ChatMessage();
				chatMessage.setMessage(messageText);
				chatMessage.setDate(Calendar.getInstance().getTime());
				chatMessage.setMe(true);
				chatMessage.setNew(false);
				if (checkBox.isChecked())
                    chatMessage.setTipo(ChatMessage.MessageTypes.RECLAMO.type());
				else
                    chatMessage.setTipo(ChatMessage.MessageTypes.COMUN.type());
				messageET.setText("");
				try {
					chatMessageBo.create(chatMessage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				displayMessage(chatMessage);

				sendMessageToServer(chatMessage);
			}
		});
        messageStateSendedChange = new MessageStateSendedChange() {
            @Override
            public void onChange() {
                loadMessages();
            }
        };
        ChatMessageBo.messageStateSendedChange = messageStateSendedChange;
        checkBox = findViewById(R.id.checkboxReclamoId);
	}

    private void scroll(int numbUnSeen) {
        messagesContainer.scrollToPosition(chatHistory.size() - numbUnSeen - 1);
    }

	public void displayMessage(ChatMessage message) {
		adapter.add(message);
		adapter.notifyDataSetChanged();
		scroll();
	}

    @Override
    public boolean onSupportNavigateUp() {
        startActivity(new Intent(this, MainActivity.class));
        this.finish();
        return true;
    }

	public static JSONObject getJsonBodyForSendMsj(ChatMessage chatMessage, Context context){
		JSONObject body = new JSONObject();
		try {
            body.put(context.getString(R.string.parameterWsMessageType), chatMessage.getTipo());
			if(MyApplication.getUserType() == Usuario.TIPO_CONDUCTOR){
				body.put(context.getString(R.string.origen), ChatMessage.ORIGEN_MENSAJE_CONDUCTOR);
				body.put(context.getString(R.string.destino), MyApplication.getInstance().getPreferences().getViaje().getPasajero().getDni());
				body.put(context.getString(R.string.titulo), /*MyApplication.getInstance().getPreferences().getUsuario().getNombre()*/"");
			}else{
				body.put(context.getString(R.string.origen), ChatMessage.ORIGEN_MENSAJE_PASAJERO);
				body.put(context.getString(R.string.destino), ""/*MyApplication.getInstance().getViaje().getConductor().getDni()*/);
				body.put(context.getString(R.string.titulo), "");
			}
			body.put(context.getString(R.string.mensajeFirebase), chatMessage.getMessage());
		} catch (JSONException e) { e.printStackTrace(); }
		return body;
	}

	private void sendMessageToServer(ChatMessage chatMessage){
	    MessageWs messageWs = new MessageWs();
        messageWs.sendMessageToServer(chatMessage, getOnMessageSendResult());
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private RestCustomer.OnRequestRestCustomerFinish getOnRequestCustomerFinish() {
		return new RestCustomer.OnRequestRestCustomerFinish() {
			@Override
			public void onOkRestCustomerResult(String objectResponse) {
				Gson gson = new Gson();
				try {
					ResultWsCall object = gson.fromJson(objectResponse, ResultWsCall.class);
					switch (object.getRespuesta()) {
						case SOLICITUD_ACEPTADA_CORRECTAMENTE:
							break;
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void onErrorRestCustomerResult(String result) {
				if(result.equals("sin internet")){
					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
						startService(new Intent(ChatActivity.this, NetworkSchedulerService.class));
					}else{
						startService(new Intent(ChatActivity.this, ConnectionReceiver.class));
					}
				}
			}
		};
	}

	private RestCustomerVolley.OnMessageSendResult getOnMessageSendResult(){
		return new RestCustomerVolley.OnMessageSendResult(){
			@Override
			public void onOkMessageSend(String objectResponse, ChatMessage chatMessage) {
				Gson gson = new Gson();
				try {
					ResultWsCall object = gson.fromJson(objectResponse, ResultWsCall.class);
					switch (object.getRespuesta()) {
						case SOLICITUD_ACEPTADA_CORRECTAMENTE:
							chatMessage.setEnviado(true);
							chatMessageBo.updateSendedMessage(chatMessage.getId());
							adapter.notifyDataSetChanged();
							break;
					}
				}catch (Exception e){
                    Intent connectionReceiverIntent = new Intent(ChatActivity.this, ConnectionReceiver.class);
                    startService(connectionReceiverIntent);
					e.printStackTrace();
				}
			}

			@Override
			public void onOErrorMessageSend(ChatMessage chatMessage) {

			}

		};
	}

	private MyFirebaseMessagingService.OnMessageChatRecibed getOnMansajeCHatRecibed(){
		return new MyFirebaseMessagingService.OnMessageChatRecibed() {
			@Override
			public void doCallBack(final ChatMessage chatMessage) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						adapter.add(chatMessage);
						adapter.notifyDataSetChanged();
						scroll();
					}
				});
			}
		};
	}

	private void scroll() {
		messagesContainer.scrollToPosition(chatHistory.size() - 1);
	}

	private void loadMessages(){
        try {
            chatHistory = chatMessageBo.recoveryAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter = new ChatAdapter(ChatActivity.this, chatHistory);
        scroll();
		messagesContainer.setAdapter(adapter);
        adapter.notifyDataSetChanged();
	}

	@Override
	protected void onResume() {
		super.onResume();
		NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(ID_NOTIFICATION_CHAT);

        try {
            chatMessageBo.updateNewMessages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        MyFirebaseMessagingService.setOnMessageChatRecibed(getOnMansajeCHatRecibed());
		active = true;
	}

	@Override
	protected void onPause() {
		super.onPause();
		active = false;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		active = false;
	}

	private void getFinishListenner(){
		finishListener = new FinishMyDialogChatListener() {
			@Override
			public void finishActivity() {
				MyApplication.getInstance().setChatMessages(null);
				ChatActivity.this.finish();
			}
		};
	}

	public interface FinishMyDialogChatListener {
		void finishActivity();
	}

    public interface MessageStateSendedChange {
        void onChange();
    }
}
