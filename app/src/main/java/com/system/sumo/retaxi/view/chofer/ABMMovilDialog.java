package com.system.sumo.retaxi.view.chofer;

import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.Movil;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.view.dialogs.generics.BaseDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import static com.system.sumo.retaxi.dto.Movil.EXTRA_MOVIL;
import static com.system.sumo.retaxi.utils.ServicesRoutes.SAVE_NEW_CAR;

/**
 * Created by eMa on 2/10/2017.
 */

public class ABMMovilDialog extends BaseDialogFragment {
    Bundle bundle;
    Button buttonAceptar;
    Button buttonCancelar;
    EditText etMarca;
    EditText etModelo;
    EditText etPatente;
    Movil movil;
    ProgressDialogFragment progressDialogFragment;
    ABMMovilDialogListener abmMovilDialogListener;

    public static ABMMovilDialog newInstance(Bundle b, ABMMovilDialogListener abmMovilDialogListener){
        ABMMovilDialog dialogFragment = new ABMMovilDialog();
        dialogFragment.abmMovilDialogListener = abmMovilDialogListener;
        dialogFragment.setArguments(b);
        return dialogFragment;
    }

    public static ABMMovilDialog newInstance(Bundle b){
        ABMMovilDialog dialogFragment = new ABMMovilDialog();
        dialogFragment.setArguments(b);
        return dialogFragment;
    }

    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.dialog_alta_modificaion_moviles, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        return alertDialogBuilder.show();
    }

    private void initComponents(View view){
        etMarca = (EditText) view.findViewById(R.id.marca);
        etModelo = (EditText) view.findViewById(R.id.modelo);
        etPatente = (EditText) view.findViewById(R.id.patente);
        buttonAceptar = (Button) view.findViewById(R.id.buttonAccept);
        buttonAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movil = new Movil();
                movil.setMarca(etMarca.getText().toString());
                movil.setModelo(etModelo.getText().toString());
                movil.setPatente(etPatente.getText().toString());
                JSONObject body = new JSONObject();
                try {
                    body.put(getString(R.string._marca), etMarca.getText());
                    body.put(getString(R.string.model), etModelo.getText());
                    body.put(getString(R.string._patente), etPatente.getText());
                } catch (JSONException e) { e.printStackTrace(); }
                progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.validandoOperacion));
                progressDialogFragment.show(getFragmentManager(), "");
                //new RestCustomer(this, url, body, Constantes.POST, MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth(), getOnRequestCustomerFinish()).execute();
                RestCustomerVolley.addQueue(Request.Method.POST, SAVE_NEW_CAR, body, true, getOnRequestCustomerFinish());
            }
        });
        buttonCancelar = (Button) view.findViewById(R.id.buttonReject);
        buttonCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ABMMovilDialog.this.dismissAllowingStateLoss();
            }
        });
        bundle = getArguments();
        Movil movil = (Movil) bundle.getSerializable(EXTRA_MOVIL);
        if(movil != null){
            etMarca.setText(movil.getMarca());
            etModelo.setText(movil.getModelo());
            etPatente.setText(movil.getPatente());
        }
    }

    private RestCustomerVolley.OnRequestRestCustomerFinish getOnRequestCustomerFinish() {
        return new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                /*Movil movil = null;
                try {
                    Gson gson = new Gson();
                    movil = gson.fromJson(object, Movil.class);
                }catch (Exception e){
                    e.printStackTrace();
                }*/
                progressDialogFragment.dismissAllowingStateLoss();
                if(movil != null) {
                    MyApplication.getInstance().getPreferences().getUsuario().getMoviles().add(movil);
                    if (abmMovilDialogListener != null)
                        abmMovilDialogListener.onAccept();
                    ABMMovilDialog.this.dismissAllowingStateLoss();
                }else{
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.noSepudoRealizarPeticion)).show(getFragmentManager(), "");
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.noSepudoRealizarPeticion)).show(getFragmentManager(), "");
            }
        };
    }

    public interface ABMMovilDialogListener{
        void onAccept();
    }
}
