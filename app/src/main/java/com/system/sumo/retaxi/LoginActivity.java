package com.system.sumo.retaxi;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import android.text.InputType;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.utils.Constantes;
import com.system.sumo.retaxi.view.dialogs.AdditionalDataDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.InputDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.LoginWs;
import com.system.sumo.retaxi.view.MainActivity;

import java.util.Arrays;

import io.fabric.sdk.android.Fabric;

import static com.system.sumo.retaxi.utils.Constantes.CONDUCTOR_NO_REGISTRADO;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_ERROR_AL_INTENTAR_INGRESAR;
import static com.system.sumo.retaxi.utils.Constantes.USUARIO_INVALIDO;


public class LoginActivity extends FragmentActivity implements GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LoginActivity.class.getCanonicalName();
    private Usuario usuario;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_GET_AUTH_CODE = 9003;
    String dni = null;
    private boolean isFragmentActive = false;

    private TextView tvAcercaDe;
    private CallbackManager callbackManager;
    final LoginWs loginWs = new LoginWs();
    LoginButton loginButton;
    private SignInButton buttonLoginGoogle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponents();
        checkLogin();
    }

    private void checkLogin(){
        if(MyApplication.getInstance().getPreferences().getUsuario() != null){
            if(MyApplication.getUserType() == Usuario.TIPO_CONDUCTOR || MyApplication.getInstance().getPreferences().getUsuario().getBirthday() != null) {
                this.finish();
                startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            }else{
                checkResultRiderLogin(MyApplication.getInstance().getPreferences().getUsuario());
            }
        }
    }

    protected void setGooglePlusButtonText(SignInButton signInButton) {
        // Find the TextView that is inside of the SignInButton and set its text
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setText(getString(R.string.login_gmail));
                tv.setPadding(0, 0, 20, 0);
                return;
            }
        }
    }

    private void initComponents(){
        loginButton = findViewById(R.id.login_button_facebook);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email"));
        callbackManager = CallbackManager.Factory.create();
        buttonLoginGoogle = findViewById(R.id.login_button_google);
        setGooglePlusButtonText(buttonLoginGoogle);
        buttonLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isDeviceOnline()) {
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
                }else{
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.msjSinConexion)).show(getSupportFragmentManager(), "");
                }
            }
        });
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        loginInFacebook(loginResult.getAccessToken().getToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
        Fabric.with(this, new Crashlytics());
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        // [END configure_signin]

        // Build GoogleAPIClient with the Google Sign-In API and the above options.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        tvAcercaDe = findViewById(R.id.idAcercaDe);
        /*tvAcercaDe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getResources().getString(R.string.dirWeb)));
                startActivity(intent);
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        isFragmentActive = true;
        if(MyApplication.getInstance().getFirebaseMessajeType().equals(MESSAGE_TYPE_ERROR_AL_INTENTAR_INGRESAR)){
            showDialogErrorAlIngresar();
        }

        MyApplication.getInstance().setFirebaseMessajeType(null);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        isFragmentActive = false;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
    private void signOut(){
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {

                        }
                    }
                });
    }

    private void checkResultDriverLogin(Object result){
        if (result instanceof ResultWsCall){
            ResultWsCall resultWsCall = (ResultWsCall)result;
            switch (resultWsCall.getRespuesta()){
                case Constantes.NO_TIENE_DNI:
                    PreferencesBo.saveTokensInPreferences(resultWsCall.getToken());
                    InputDialogFragment inputDialogFragment = InputDialogFragment.newInstance(getApplicationContext(), "", InputDialogFragment.TYPE_OK_CANCEL, "", getString(R.string.ingreseDni), InputType.TYPE_CLASS_NUMBER, new InputDialogFragment.OkCancelListener() {
                        @Override
                        public void onCancelSelected() {
                            signOut();
                        }

                        @Override
                        public void onOkSelected(String textInput) {
                            final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.registrando));
                            progressDialogFragment.show(getSupportFragmentManager(), "");
                            loginWs.associateDriver(textInput, new GenericWs.GenericWsResponse() {
                                @Override
                                public void onOkResult(Object result) {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    if (result instanceof ResultWsCall) {
                                        ResultWsCall resultWsCall1 = (ResultWsCall) result;
                                        switch (resultWsCall1.getRespuesta()){
                                            case CONDUCTOR_NO_REGISTRADO:
                                                SimpleDialogFragment simpleDialogFragment2 = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.noConductor));
                                                simpleDialogFragment2.show(getSupportFragmentManager(), "");
                                                signOut();
                                                break;
                                            case USUARIO_INVALIDO:
                                                SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.usuarioOtraCuenta));
                                                simpleDialogFragment.show(getSupportFragmentManager(), "");
                                                signOut();
                                                break;
                                        }
                                    }else if (result instanceof Usuario) {
                                        checkLogin();
                                    }
                                }

                                @Override
                                public void onErrorResult() {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    signOut();
                                    if (isFragmentActive) {
                                        showDialogErrorAlIngresar();
                                    }else{
                                        MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_ERROR_AL_INTENTAR_INGRESAR);
                                    }
                                }
                            });
                        }
                    });
                    inputDialogFragment.show(getSupportFragmentManager(), "");
                    break;
                case CONDUCTOR_NO_REGISTRADO:
                    SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.noConductor));
                    simpleDialogFragment.show(getSupportFragmentManager(), "");
                    break;
            }
        }else if (result instanceof Usuario){
            checkLogin();
        }
    }

    private void showDialogErrorAlIngresar(){
        SimpleDialogFragment dialogo = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.noSePudoIngresar));
        dialogo.show(getSupportFragmentManager(), "");
    }

    public boolean isDeviceOnline() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void loginInGoogle(String authCode){
        final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.registrando));
        progressDialogFragment.show(getSupportFragmentManager(), "");
        loginWs.loginGoogle(authCode, new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogFragment.dismissAllowingStateLoss();
                if (MyApplication.getUserType() == Usuario.TIPO_CONDUCTOR)
                    checkResultDriverLogin(result);
                else
                    checkResultRiderLogin(result);
            }

            @Override
            public void onErrorResult() {
                progressDialogFragment.dismissAllowingStateLoss();
                signOut();
                SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.falloRequestSv));
                simpleDialogFragment.show(getSupportFragmentManager(),"");
            }
        });
    }

    private void checkResultRiderLogin(Object result){
        if (result instanceof Usuario){
            Usuario usuario = (Usuario) result;
            if (usuario.getBirthday() != null && !usuario.getBirthday().equals("")){
                checkLogin();
            }else{
                AdditionalDataDialogFragment.newInstance(new AdditionalDataDialogFragment.FinalizarCalificacionListener() {
                    @Override
                    public void onFinalizarCalificacionListener() {
                        checkLogin();
                    }
                }).show(getSupportFragmentManager(), "");
            }
        }
    }

    private void loginInFacebook(String authCode){
        final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.registrando));
        progressDialogFragment.show(getSupportFragmentManager(), "");
        loginWs.loginFacebook(authCode, new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogFragment.dismissAllowingStateLoss();
                checkResultDriverLogin(result);
            }

            @Override
            public void onErrorResult() {
                progressDialogFragment.dismissAllowingStateLoss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GET_AUTH_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                String authCode = acct.getServerAuthCode();
                loginInGoogle(authCode);
            }else{
                mGoogleApiClient.reconnect();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.getErrorCode() == ConnectionResult.API_UNAVAILABLE) {
            SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.playServicesNoDisponible), getString(R.string.importante), new SimpleDialogFragment.OkListener() {
                @Override
                public void onOkSelected() {

                }
            });
            simpleDialogFragment.show(getSupportFragmentManager(),"");
        }
    }

}

