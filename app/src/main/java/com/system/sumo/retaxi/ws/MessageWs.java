package com.system.sumo.retaxi.ws;

import android.content.Context;

import com.android.volley.Request;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.utils.RestCustomerVolley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.system.sumo.retaxi.utils.ServicesRoutes.SEND_MESSAGE;
import com.system.sumo.retaxi.utils.Constantes;

public class MessageWs extends GenericWs {

    public void sendMessageToServer(ChatMessage chatMessage, final RestCustomerVolley.OnMessageSendResult result){
        RestCustomerVolley.addQueue(Request.Method.POST, SEND_MESSAGE,
                getJsonBodyForSendMsj(chatMessage),
                true,
                null,
                new RestCustomerVolley.OnMessageSendResult() {
                    @Override
                    public void onOkMessageSend(String object, ChatMessage chatMessage) {
                        result.onOkMessageSend(object, chatMessage);
                    }

                    @Override
                    public void onOErrorMessageSend(ChatMessage chatMessage) {
                        result.onOErrorMessageSend(chatMessage);
                    }
                },
                chatMessage);
    }

    private static JSONObject getJsonBodyForSendMsj(ChatMessage chatMessage){
        JSONObject body = new JSONObject();
        try {
            body.put(Constantes.MESSAGE_TYPE_PARAM, chatMessage.getTipo());
            if(MyApplication.getUserType() == Usuario.TIPO_CONDUCTOR){
                body.put(Constantes.ORIGIN, ChatMessage.ORIGEN_MENSAJE_CONDUCTOR);
                body.put(Constantes.DESTINATION, MyApplication.getInstance().getPreferences().getViaje().getPasajero().getDni());
                body.put(Constantes.TITLE, /*MyApplication.getInstance().getPreferences().getUsuario().getNombre()*/"");
            }else{
                body.put(Constantes.ORIGIN, ChatMessage.ORIGEN_MENSAJE_PASAJERO);
                body.put(Constantes.DESTINATION, ""/*MyApplication.getInstance().getViaje().getConductor().getDni()*/);
                body.put(Constantes.TITLE, "");
            }
            body.put(Constantes.MENSAJE, chatMessage.getMessage());
        } catch (JSONException e) { e.printStackTrace(); }
        return body;
    }
}
