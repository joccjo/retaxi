package com.system.sumo.retaxi.view.pasajero;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.PlaceAutoComplete;
import com.system.sumo.retaxi.dto.PlacePredictions;
import com.system.sumo.retaxi.utils.VolleyJSONRequest;
import com.system.sumo.retaxi.view.dialogs.generics.BaseDialogFragment;

import java.util.HashMap;
import java.util.List;

import static com.system.sumo.retaxi.utils.Constantes.HEADER_AUTHORIZATION;
import static com.system.sumo.retaxi.utils.Constantes.HEADER_CONTENT_TYPE;
import static com.system.sumo.retaxi.utils.Constantes.HEADER_JSON;
import static com.system.sumo.retaxi.utils.Constantes.TOKEN;
import static com.system.sumo.retaxi.utils.ServicesRoutes.DIRECTION_PLACES;


public class CustomPlaceAutocompleteFragment extends BaseDialogFragment implements Response.Listener<String>, Response.ErrorListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    double latitude;
    double longitude;
    private ListView mAutoCompleteList;
    private EditText etAddress;
    private String GETPLACESHIT = "places_hit";
    private PlacePredictions predictions;
    private Location mLastLocation;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private int CUSTOM_AUTOCOMPLETE_REQUEST_CODE = 20;
    private static final int MY_PERMISSIONS_REQUEST_LOC = 30;
    private ImageView searchBtn;
    private String preFilledText;
    private Handler handler;
    private VolleyJSONRequest request;
    private GoogleApiClient mGoogleApiClient;
    private static OnListViewPlaceItemSelected onListViewPlaceItemSelected;
    private static int mDireccion;
    private static String mTexto;
    private static List<PlaceAutoComplete> mDireccionesBd;

    public static CustomPlaceAutocompleteFragment newInstance(int direccion, String texto, List<PlaceAutoComplete> direcciones, OnListViewPlaceItemSelected listener) {
        onListViewPlaceItemSelected = listener;
        mDireccion = direccion;
        mTexto = texto;
        mDireccionesBd = direcciones;
        CustomPlaceAutocompleteFragment dialog = new CustomPlaceAutocompleteFragment();
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.fragment_place_autocomplete, null);
        alertDialogBuilder.setView(view);
        if(savedInstanceState != null && savedInstanceState.getString("Search Text") != null) {
            preFilledText = savedInstanceState.getString("Search Text");
        }
        etAddress = (EditText) view.findViewById(R.id.adressText);
        if(mDireccion == PedirTaxiFragment.REQUEST_DIRECTION_FROM){
            etAddress.setHint(getString(R.string.direccionPartida));
        }else{
            etAddress.setHint(getString(R.string.direccionDestino));
        }
        etAddress.setText(mTexto);
        mAutoCompleteList = (ListView) view.findViewById(R.id.searchResultLV);
        searchBtn=(ImageView) view.findViewById(R.id.search);


        //get permission for Android M
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            fetchLocation();
        } else {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOC);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                fetchLocation();
            }
        }


        //Add a text change listener to implement autocomplete functionality
        etAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // optimised way is to start searching for laction after user has typed minimum 3 chars
                if (etAddress.getText().length() > 3) {


                    searchBtn.setVisibility(View.GONE);

                    Runnable run = new Runnable() {


                        @Override
                        public void run() {

                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            MyApplication.volleyQueueInstance.cancelRequestInQueue(GETPLACESHIT);

                            //build Get url of Place Autocomplete and hit the url to fetch result.
                            HashMap<String, String> head = new HashMap<String, String>();

                            head.put(HEADER_CONTENT_TYPE, HEADER_JSON);
                            head.put(HEADER_AUTHORIZATION, TOKEN + " " + MyApplication.getInstance().getPreferences().getUsuario().getTokenOauth());
                            request = new VolleyJSONRequest(Request.Method.GET, getPlaceAutoCompleteUrl(etAddress.getText().toString()), null, head, CustomPlaceAutocompleteFragment.this, CustomPlaceAutocompleteFragment.this);

                            //Give a tag to your request so that you can use this tag to cancle request later.
                            request.setTag(GETPLACESHIT);

                            MyApplication.volleyQueueInstance.addToRequestQueue(request);

                        }

                    };

                    // only canceling the network calls will not help, you need to remove all callbacks as well
                    // otherwise the pending callbacks and messages will again invoke the handler and will send the request
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 500);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

        etAddress.setText(preFilledText);
        etAddress.setSelection(etAddress.getText().length());

        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // pass the result to the calling activity
                if(predictions == null)
                    onListViewPlaceItemSelected.placeAutocomplete(mDireccionesBd.get(position));
                else
                    onListViewPlaceItemSelected.placeAutocomplete(predictions.getPlaces().get(position));
                getDialog().dismiss();
                /*Intent intent = new Intent();
                intent.putExtra("Location Address", predictions.getPlaces().get(position).getPlaceDesc());*/
                //setResult(CUSTOM_AUTOCOMPLETE_REQUEST_CODE, intent);
                //finish();
            }
        });
        if(mDireccionesBd != null) {
            mAutoCompleteAdapter = new AutoCompleteAdapter(getActivity(), mDireccionesBd, getActivity());
            mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
        }
        return alertDialogBuilder.show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = ViewGroup.LayoutParams.MATCH_PARENT;
        p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE;
        p.y = 0;
        getDialog().getWindow().setAttributes(p);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append(DIRECTION_PLACES);
        input.trim();
        input = input.replaceAll(" ", "%20");
        urlString.append(input.trim()+"/");
        urlString.append(latitude+","+longitude);
        return urlString.toString();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        searchBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResponse(String response) {
        searchBtn.setVisibility(View.VISIBLE);
        Log.d("PLACES RESULT:::", response);
        Gson gson = new Gson();
        predictions = gson.fromJson(response, PlacePredictions.class);

        if (mAutoCompleteAdapter == null) {
            mAutoCompleteAdapter = new AutoCompleteAdapter(getActivity(), predictions.getPlaces(), getActivity());
            mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
        } else {
            mAutoCompleteAdapter.clear();
            mAutoCompleteAdapter.addAll(predictions.getPlaces());
            mAutoCompleteAdapter.notifyDataSetChanged();
            mAutoCompleteList.invalidate();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            }

        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public void fetchLocation(){
        //Build google API client to use fused location
        buildGoogleApiClient();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOC: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission granted!
                    fetchLocation();

                } else {
                    // permission denied!
                    
                    Toast.makeText(getActivity(), "Please grant permission for using this app!", Toast.LENGTH_LONG).show();
                }
                return;
            }


        }
    }

    public interface OnListViewPlaceItemSelected{
        void placeAutocomplete(PlaceAutoComplete place);
    }

    public OnListViewPlaceItemSelected getOnListViewPlaceItemSelected() {
        return onListViewPlaceItemSelected;
    }

    public void setOnListViewPlaceItemSelected(OnListViewPlaceItemSelected onListViewPlaceItemSelected) {
        CustomPlaceAutocompleteFragment.onListViewPlaceItemSelected = onListViewPlaceItemSelected;
    }
}

