package com.system.sumo.retaxi.dao.sqlite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.database.Cursor;
import android.database.DatabaseUtils;

import com.google.android.gms.maps.GoogleMap;
import com.system.sumo.retaxi.bo.BaseDeDatosBo;
import com.system.sumo.retaxi.dao.CondicionDao;
import com.system.sumo.retaxi.dto.MyApplication;

public abstract class GenericSqliteDao {
	
	protected List<String> listTables;
	protected List<String> listColumns; 
	protected Map<String, CondicionDao> parameterWhere;
	protected List<String> parameterOrderBy;
	protected List<String> parameterGroupBy;
	protected Map<String, Object> insertMap; 
	protected String table;
	protected String column;
	protected Map<String, Object> updateMap;
	protected List<String> listJoin;

	public GenericSqliteDao(){
		listTables = new ArrayList<String>();
		parameterWhere = new HashMap<String, CondicionDao>();
		parameterOrderBy = new ArrayList<String>();
		parameterGroupBy = new ArrayList<String>();
		insertMap = new HashMap<String, Object>();
		table = "";
		updateMap = new HashMap<String, Object>();
		listJoin = new ArrayList<String>();
		listColumns = new ArrayList<String>(); 
		
	}
	
	public void create ()throws Exception{
		String columnList = "";
		String values = "";
		
		Object[] parametros = new Object[insertMap.size()];

		Set<String> columnSet = insertMap.keySet();		
		Iterator<String> iteratorColumnas = columnSet.iterator();
		
		int posicionParametro = 0;
		while (iteratorColumnas.hasNext()){
			String column = iteratorColumnas.next();
			Object value = insertMap.get(column);
			
			columnList += column + ", ";			
			values = values + "?, ";
			parametros[posicionParametro] = value;		
			
			posicionParametro++;			
		}
		columnList = columnList.substring(0,columnList.length() - 2);
		values = values.substring(0, values.length() - 2);

		String query = "INSERT INTO " + table + "(" + columnList + ") VALUES (" + values + ")";

		BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
        bd.getWritableDatabase().execSQL(query, parametros);
		clearVar();
	}

	public void update() throws Exception{
        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();

		//armamos el query
		String update = "UPDATE " + table + " SET";

		//creo la lista de columnas del update segun las columnas cargadas en insertMap
		String columnList = "";

		Object[] parametros = new Object[updateMap.size() + parameterWhere.size()];

		Set<String> columnSet = updateMap.keySet();
		Iterator<String> iteratorColumnas = columnSet.iterator();

		int posicionParametro = 0;
		while (iteratorColumnas.hasNext()){
			String column = iteratorColumnas.next();
			columnList += column + " = ?, ";

			Object value = updateMap.get(column);
			parametros[posicionParametro] = value;
			posicionParametro++;
		}
		columnList = columnList.substring(0,columnList.length() - 2);

		String where = "WHERE ";
		for (Map.Entry<String, CondicionDao> entry : parameterWhere.entrySet()) {
			CondicionDao value = entry.getValue();
			where = where + entry.getKey() + value.getOperator() + " ? AND ";
			parametros[posicionParametro] = value.getValue();
			posicionParametro++;
		}

		if (parameterWhere.size() > 0)
		    where = where.substring(0, where.length() - 4);
		String query = update + " " + columnList + " " + where;
        bd.getWritableDatabase().execSQL(query, parametros);

		clearVar();
	}
	
	public void delete() throws Exception{

        BaseDeDatosBo bd = new BaseDeDatosBo();
        bd.open();
		
		//armamos el query
		String delete = "DELETE FROM " + table;		
		
		Object[] parametros = new Object[parameterWhere.size()];
		int posicionParametro = 0;
		
		String where = "WHERE ";
		for (Map.Entry<String, CondicionDao> entry : parameterWhere.entrySet()) {
			CondicionDao value = entry.getValue();
			where = where + entry.getKey() + value.getOperator() + " ? AND ";
			parametros[posicionParametro] = value.getValue();
			posicionParametro++;
		}
		
		where = where.substring(0, where.length() - 4);
		
		String query = delete + " " + where;

		bd.getWritableDatabase().execSQL(query, parametros);
		clearVar();
	}
	
	public Cursor recoveryData() throws Exception{
		Cursor cursor = null;
		//Lista de seleccion
		if(listTables.size()> 0)
		{
			//COLUMNS
			String[] columns = null;
			if(!listColumns.isEmpty())
			{
				columns = new String[listColumns.size()];
				columns = listColumns.toArray(columns);
			}
				
			//FROM
			String tables = "";
			for(String table: listTables)
				tables += table + ", ";			
			tables = tables.substring(0, tables.length() - 2);
			
			
			String where = "";
			//WHERE primero agrego los join de las tablas
			for(String join: listJoin)
				where += join + " AND ";
			
			for(Map.Entry<String, CondicionDao> entry: parameterWhere.entrySet()){
				CondicionDao value = entry.getValue();
				Object o = value.getValue();
				if (o instanceof String)
					o = "'" + value.getValue() + "'";
				where += entry.getKey() + value.getOperator() + o + " AND ";
			}
			if(parameterWhere.size()>0 || listJoin.size() > 0)
				where = where.substring(0, where.length() - 5);
			
			String groupBy = "";
			for(String group: parameterGroupBy)
				groupBy += group + ", ";
			if(parameterGroupBy.size() > 0)
				groupBy = groupBy.substring(0, groupBy.length() - 2);
			
			String orderBy = "";
			for(String order: parameterOrderBy)
				orderBy += order + ", ";
			if(parameterOrderBy.size() > 0)
				orderBy = orderBy.substring(0, orderBy.length() - 2);


            BaseDeDatosBo bd = new BaseDeDatosBo();
            bd.open();

			//SQLiteDatabase sqLiteDatabase = SqliteDaoFactory.open();
			cursor = bd.getWritableDatabase().query(tables, columns, where, null, groupBy, null, orderBy);
			
			clearVar();
		}
		return cursor;
	}

	public long count(){
	    String where = "";
        for(Map.Entry<String, CondicionDao> entry: parameterWhere.entrySet()){
            CondicionDao value = entry.getValue();
            Object o = value.getValue();
            if (o instanceof String)
                o = "'" + value.getValue() + "'";
            where += entry.getKey() + value.getOperator() + o + " AND ";
        }
		if(parameterWhere.size()>0 || listJoin.size() > 0)
			where = where.substring(0, where.length() - 5);
        BaseDeDatosBo bd = new BaseDeDatosBo();

        return DatabaseUtils.queryNumEntries(bd.open(), table, where);

    }
	
	public void clearVar(){
		listTables.clear();
		parameterWhere.clear();
		parameterOrderBy.clear();
		insertMap.clear();
		table = "";
		column = "";
		updateMap.clear();
		listJoin.clear();
		listColumns.clear();
	}
	
	public boolean existsTable(String nameTable) throws Exception{
		listTables.add("sqlite_master");
		parameterWhere.put("name", new CondicionDao("=", nameTable));
		Cursor cursor = recoveryData();
		int cantidad = cursor.getCount();
		cursor.close();
		return cantidad != 0;
	}
}
