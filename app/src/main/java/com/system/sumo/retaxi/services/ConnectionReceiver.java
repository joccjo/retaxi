package com.system.sumo.retaxi.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.google.gson.Gson;


import com.system.sumo.retaxi.bo.ChatMessageBo;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.utils.RestCustomerVolley;

import static com.system.sumo.retaxi.utils.Constantes.SOLICITUD_ACEPTADA_CORRECTAMENTE;

/**
 * Created by eMa on 27/12/2016.
 */

public class ConnectionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager cm = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        if (cm == null)
            return;
        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
            ChatMessageBo chatMessageBo = new ChatMessageBo();
            if (!chatMessageBo.sendMessagesToServer()){
                context.stopService(new Intent(context, ConnectionReceiver.class));
            }
        }
    }

    private RestCustomerVolley.OnMessageSendResult getOnMessageSendResult(){
        return new RestCustomerVolley.OnMessageSendResult(){
            @Override
            public void onOkMessageSend(String objectResponse, ChatMessage chatMessage) {
                Gson gson = new Gson();
                try {
                    ResultWsCall object = gson.fromJson(objectResponse, ResultWsCall.class);
                    switch (object.getRespuesta()) {
                        case SOLICITUD_ACEPTADA_CORRECTAMENTE:
                            chatMessage.setEnviado(true);
                            break;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onOErrorMessageSend(ChatMessage chatMessage) {

            }

        };
    }
}
