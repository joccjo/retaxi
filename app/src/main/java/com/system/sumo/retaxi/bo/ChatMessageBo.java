package com.system.sumo.retaxi.bo;

import com.google.gson.Gson;
import com.system.sumo.retaxi.dao.DaoFactory;
import com.system.sumo.retaxi.dao.IChatMessageDao;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.view.chat.ChatActivity;
import com.system.sumo.retaxi.ws.MessageWs;

import java.util.List;

import static com.system.sumo.retaxi.utils.Constantes.SOLICITUD_ACEPTADA_CORRECTAMENTE;

/**
 * Created by eMa on 2/1/2017.
 */

public class ChatMessageBo {
    public static ChatActivity.MessageStateSendedChange messageStateSendedChange;
    DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.SQLITE);
    IChatMessageDao iChatMessageDao = daoFactory.chatMessageDao();

    public void create(ChatMessage v) throws Exception{
        iChatMessageDao.create(v);
    }

    public List<ChatMessage> recoveryAll() throws Exception{
        return iChatMessageDao.recoveryAll();
    }

    public long getNumberUnSeenMessages() throws Exception{
        return iChatMessageDao.getNumberUnSeenMessages();
    }

    public List<ChatMessage> getUnSeenMessages() throws Exception {
        return iChatMessageDao.getUnSeenMessages();
    }

    public List<ChatMessage> recoveryNotSended() throws Exception{
        return iChatMessageDao.recoveryNotSended();
    }

    public void updateNewMessages() throws Exception{
        iChatMessageDao.updateNewMessages();
    }

    public void updateSendedMessage(long id) throws Exception{
        iChatMessageDao.updateSendedMessage(id);
    }

    /**
     *
     * @return have messages for send?
     */
    public boolean sendMessagesToServer(){
        ChatMessageBo chatMessageBo = new ChatMessageBo();
        List<ChatMessage> messages = null;
        try {
            messages = chatMessageBo.recoveryNotSended();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (messages != null && messages.size() > 0) {
            for (ChatMessage chatMessage : messages) {
                MessageWs messageWs = new MessageWs();
                messageWs.sendMessageToServer(chatMessage, getOnMessageSendResult());
            }
            return true;
        }else{
            return false;
        }
    }

    private RestCustomerVolley.OnMessageSendResult getOnMessageSendResult(){
        return new RestCustomerVolley.OnMessageSendResult(){
            @Override
            public void onOkMessageSend(String objectResponse, ChatMessage chatMessage) {
                Gson gson = new Gson();
                try {
                    ResultWsCall object = gson.fromJson(objectResponse, ResultWsCall.class);
                    switch (object.getRespuesta()) {
                        case SOLICITUD_ACEPTADA_CORRECTAMENTE:
                            chatMessage.setEnviado(true);
                            updateSendedMessage(chatMessage.getId());
                            if (messageStateSendedChange != null){
                                messageStateSendedChange.onChange();
                            }
                            break;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onOErrorMessageSend(ChatMessage chatMessage) {

            }

        };
    }
}
