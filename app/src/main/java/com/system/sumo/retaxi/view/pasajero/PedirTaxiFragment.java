package com.system.sumo.retaxi.view.pasajero;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Geocoder;
import android.location.Location;
import android.media.RingtoneManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.bo.ChatMessageBo;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.ResultWsCall;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.services.MyFirebaseInstanceIDService;
import com.system.sumo.retaxi.services.MyFirebaseMessagingService;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.utils.maps.MapDraw;
import com.system.sumo.retaxi.utils.CityLocation;
import com.system.sumo.retaxi.view.chat.ChatActivity;
import com.system.sumo.retaxi.view.markers.InfoWindowMarker;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.dialogs.VerTarifaDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.TravelWs;
import com.transitionseverywhere.Slide;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.system.sumo.retaxi.utils.Avisos.ID_NOTIFICATION_ACCEPTED_TRAVEL;
import static com.system.sumo.retaxi.utils.Avisos.getRingtonNotification;
import static com.system.sumo.retaxi.utils.Avisos.startNotification;
import static com.system.sumo.retaxi.utils.Constantes.CANCELA_VIAJE_EXITO;
import static com.system.sumo.retaxi.utils.Constantes.CONDUCTORES_NO_DISPONIBLES;
import static com.system.sumo.retaxi.utils.Constantes.ERROR_CANCELA_VIAJE;
import static com.system.sumo.retaxi.utils.Constantes.ERROR_CANCELA_VIAJE_;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CANCELADO_X_CONDUCTOR;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_CONDUCTORES_NO_DISPONIBLES;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_REQUEST_ACCEPTED;
import static com.system.sumo.retaxi.utils.Constantes.MESSAGE_TYPE_UBICACION_CONDUCTOR;
import static com.system.sumo.retaxi.utils.Constantes.SOLICITUD_ACEPTADA_CORRECTAMENTE;

public class PedirTaxiFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMyLocationButtonClickListener,
        LocationListener,
        //RestCustomer.OnRequestRestCustomerFinish,
        GoogleMap.OnInfoWindowClickListener {
    //MyFirebaseMessagingService.OnMessageRecibed{

    public static String EXTRA_DESC_PLACE_DESDE = "EXTRA_DESC_PLACE_DESDE";
    public static String EXTRA_DESC_PLACE_HASTA = "EXTRA_DESC_PLACE_HASTA";
    public static String EXTRA_LATITUD_MARKER_DESDE = "EXTRA_LATITUD_MARKER_DESDE";
    public static String EXTRA_LONGITUD_MARKER_DESDE = "EXTRA_LONGITUD_MARKER_DESDE";
    public static String EXTRA_LATITUD_MARKER_HASTA = "EXTRA_LATITUD_MARKER_HASTA";
    public static String EXTRA_LONGITUD_MARKER_HASTA = "EXTRA_LONGITUD_MARKER_HASTA";
    public final static int REQUEST_CHECK_SETTINGS = 4;
    private final static int REQUEST_CHECK_LOCATION_PERMISSIONS = 2;
    public final static int REQUEST_DIRECTION_FROM = 0;
    private final static int REQUEST_DIRECTION_TO = 1;

    private static String TAG = PedirTaxiFragment.class.getCanonicalName();
    private static GoogleMap map;
    private static String ID_SELECCIONAR_EN_MAPA = "seleccionar en mapa";
    Fragment mMapFragment;
    PolylineOptions rectOptions;
    Marker markerFrom;
    Marker markerTo;
    static Marker markerUbicacionChofer;
    MapDraw mapDraw;
    Button getTaxiButton;
    Button cancelTravelButton;
    Button btnABordo;
    Button endTravelButton;
    public static boolean itsFragmentActive = false;
    private TextView textViewDesde;
    private TextView textViewHasta;
    private TextView dateTimeText;
    private AVLoadingIndicatorView dialogSearchingLocation;
    private LinearLayout linearLayoutDescriptionContainer;
    private LinearLayout linearLayoutProgrammerContainer;
    private LinearLayout idLinarContainer;

    ViewGroup viewContainer;
    private static GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private final int ADRESS_LINE_DIRECCTION = 0;
    private final int ADRESS_LINE_PROVINCIA = 2;
    private final int ADRESS_LINE_PAIS = 3;
    ChatMessageBo chatMessageBo = new ChatMessageBo();
    TravelWs viajews = new TravelWs();
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;

    private ImageView openChatButton;
    private TextView numMssgUnSeen;
    SupportMapFragment mapFragment;
    private boolean creacionDeVentana = true;
    private ProgressDialogFragment progressDialogFragment;
    private ProgressDialogFragment progressDialogCancelarViaje;
    //public static String token;
    boolean isFirstClick = true;
    Usuario usuario;
    private final String GET_TAXI = "GET_TAXI";
    public static String KEY_EXTRA_VIAJE = "viaje";
    Context mContext;
    TravelWs travelWs = new TravelWs();
    ImageView cleanDateTime;
    ImageView imageButtonTo;
    ImageView imageButtonFrom;
    ImageView showAddicionalData;
    EditText descriptionText;
    //Thread thread;

    private int wsCalledFrom = CALL_FROM_NOTHING;
    private static final int CALL_FROM_NOTHING = 0;
    private static final int CALL_FROM_CALIFICATION_WS = 1;
    private static final int CALL_FROM_PEDIR_TAXI = 2;
    private static final int CALL_FROM_CANCEL_IN_PROGRESS_DIALOG = 3;
    private Bundle savedInstanceState;

    public static PedirTaxiFragment newInstance(Viaje viaje) {
        PedirTaxiFragment pedirTaxiFragment = new PedirTaxiFragment();
        if (viaje != null) {
            Bundle b = new Bundle();
            b.putSerializable(KEY_EXTRA_VIAJE, viaje);
            pedirTaxiFragment.setArguments(b);
        }
        return pedirTaxiFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lyt_fragment_get_taxi, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(ID_NOTIFICATION_ACCEPTED_TRAVEL);
        itsFragmentActive = true;
        loadUnSeenMessages();
        MyFirebaseMessagingService.setOnMessageChatRecibed(getOnMansajeCHatRecibed());
        super.onResume();
        if (map != null) {
            if (MyApplication.getInstance().getPreferences().getViaje() != null || MyApplication.getInstance().getLastLocationFromConductorRecibed() != null) {
                if (MyApplication.getInstance().getPreferences().getViaje() != null) {
                    doTask(MyApplication.getInstance().getPreferences().getViaje(), MyApplication.getInstance().getFirebaseMessajeType());
                    //MyApplication.getInstance().setViaje(null);
                } else {
                    doTask(MyApplication.getInstance().getLastLocationFromConductorRecibed(), MyApplication.getInstance().getFirebaseMessajeType());
                    MyApplication.getInstance().setLastLocationFromConductorRecibed(null);
                }
            }
            if (MyApplication.getInstance().getPreferences().getViaje() != null) {
                doTask(MyApplication.getInstance().getPreferences().getViaje(), MyApplication.getInstance().getFirebaseMessajeType());
                //MyApplication.getInstance().setViaje(null);
            } else if (MyApplication.getInstance().getLastLocationFromConductorRecibed() != null) {
                doTask(MyApplication.getInstance().getLastLocationFromConductorRecibed(), MyApplication.getInstance().getFirebaseMessajeType());
                //MyApplication.getInstance().setLastLocationFromConductorRecibed(null);
            } else if (MyApplication.getInstance().getFirebaseMessajeType() != null) {
                doTask(null, MyApplication.getInstance().getFirebaseMessajeType());
                //MyApplication.getInstance().setFirebaseMessajeType(null);
            }
            MyApplication.getInstance().setFirebaseMessajeType(null);
        }
    }

    private void loadUnSeenMessages() {
        long numbUnseen = 0;
        try {
            numbUnseen = chatMessageBo.getNumberUnSeenMessages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (numbUnseen > 0) {

            Transition transition = new Slide(Gravity.RIGHT);
            transition.setDuration(300);
            transition.setInterpolator(new FastOutSlowInInterpolator());
            transition.setStartDelay(200);

            TransitionManager.beginDelayedTransition(viewContainer, transition);
            numMssgUnSeen.setVisibility(View.VISIBLE);
            numMssgUnSeen.setText(String.valueOf(numbUnseen));
        } else {
            numMssgUnSeen.setVisibility(View.GONE);
        }
    }

    private MyFirebaseMessagingService.OnMessageChatRecibed getOnMansajeCHatRecibed() {
        return new MyFirebaseMessagingService.OnMessageChatRecibed() {
            @Override
            public void doCallBack(ChatMessage chatMessage) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadUnSeenMessages();
                    }
                });
            }
        };
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        itsFragmentActive = false;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    private void getPlacePicker(int direction) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        builder.setLatLngBounds(CityLocation.LAT_LNG_BOUNDS);
        try {
            startActivityForResult(builder.build(getActivity()), direction);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void addMarkerFrom(LatLng lugar) {
        if (markerFrom == null) {
            MarkerOptions markerOptions = new MarkerOptions().position(lugar).title(getString(R.string.desde));
            //markerOptions.draggable(true);
            markerOptions.flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_radio_button_unchecked_green_24dp))
                    .anchor(0.5f, 0.5f);
            markerFrom = map.addMarker(markerOptions);
        } else {
            markerFrom.setPosition(lugar);
        }
        animateCameraToPoint(markerFrom.getPosition());
    }

    private void addMarkerTo(LatLng lugar) {
        if (markerTo == null) {
            MarkerOptions markerOptions = new MarkerOptions().position(lugar).title(getString(R.string.hasta));
            markerTo = map.addMarker(markerOptions);
        } else {
            markerTo.setPosition(lugar);
        }
        animateCameraToPoint(markerTo.getPosition());
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(Places.GEO_DATA_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }

    private void showGetTaxiButton() {
        cancelTravelButton.setVisibility(View.GONE);
        endTravelButton.setVisibility(View.GONE);
        btnABordo.setVisibility(View.GONE);
        getTaxiButton.setVisibility(View.VISIBLE);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (isAdded()){
            initComponents(getView());
        }
    }

    private void disableClickCleanTo() {
        imageButtonTo.setOnClickListener(null);
        imageButtonTo.setImageResource(R.mipmap.ic_search);
        textViewHasta.setText("");
    }

    private void enableClickCleanTo() {
        imageButtonTo.setImageResource(R.mipmap.ic_close);
        imageButtonTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableClickCleanTo();
                if (markerTo != null) {
                    markerTo.remove();
                    markerTo = null;
                }
            }
        });
    }

    private void disableClickCleanFrom() {
        imageButtonFrom.setOnClickListener(null);
        imageButtonFrom.setImageResource(R.mipmap.ic_search);
        textViewDesde.setText("");
    }

    private void enableClickCleanFrom() {
        imageButtonFrom.setImageResource(R.mipmap.ic_close);
        imageButtonFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableClickCleanFrom();
                markerFrom.remove();
                markerFrom = null;
            }
        });
    }

    private void showAddicionalDataOnClick(){
        TransitionManager.beginDelayedTransition(idLinarContainer);
        if (linearLayoutDescriptionContainer.getVisibility() == View.GONE){
            linearLayoutDescriptionContainer.setVisibility(View.VISIBLE);
            linearLayoutProgrammerContainer.setVisibility(View.VISIBLE);
            showAddicionalData.setImageResource(R.mipmap.baseline_keyboard_arrow_up_white_24);
        }else {
            linearLayoutDescriptionContainer.setVisibility(View.GONE);
            linearLayoutProgrammerContainer.setVisibility(View.GONE);
            showAddicionalData.setImageResource(R.mipmap.baseline_keyboard_arrow_down_white_24);
        }
    }

    private void initComponents(View view) {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
        idLinarContainer = view.findViewById(R.id.idLinarContainer);
        linearLayoutDescriptionContainer = view.findViewById(R.id.linearLayoutDescriptionContainer);
        linearLayoutProgrammerContainer = view.findViewById(R.id.linearLayoutProgrammerContainer);
        showAddicionalData = view.findViewById(R.id.showAddicionalData);
        descriptionText = view.findViewById(R.id.descriptionText);
        showAddicionalData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddicionalDataOnClick();
            }
        });
        dialogSearchingLocation = view.findViewById(R.id.dialogSearchingLocation);
        viewContainer = view.findViewById(R.id.containerId);
        numMssgUnSeen = view.findViewById(R.id.numMssgUnSeen);
        imageButtonTo = view.findViewById(R.id.imageButtonTo);
        imageButtonFrom = view.findViewById(R.id.imageButtonFrom);
        cleanDateTime = view.findViewById(R.id.cleanDateTime);
        cleanDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateTimeText.setText(getString(R.string.viajeParaAhora));
                cleanDateTime.setVisibility(View.INVISIBLE);
            }
        });
        openChatButton = view.findViewById(R.id.idImgMsjChat);
        //openChatButton.setVisibility(View.GONE);
        openChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(mContext, ChatActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(it);
            }
        });
        mContext.startService(new Intent(mContext, MyFirebaseInstanceIDService.class));
        mContext.startService(new Intent(mContext, MyFirebaseMessagingService.class));
        MyFirebaseMessagingService.setOnMessageRecibed(getOnMansajeRecibed());
        if(mGoogleApiClient == null){
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
        creacionDeVentana = true;
        ImageView imageButton = view.findViewById(R.id.buttonMenu);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        dateTimeText = view.findViewById(R.id.hourText);
        textViewDesde = view.findViewById(R.id.adressTextDesde);
        textViewHasta = view.findViewById(R.id.adressTextHasta);
        initTextViews();
        disableClickCleanFrom();
        disableClickCleanTo();
        btnABordo = view.findViewById(R.id.btnABordo);
        btnABordo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.startingTravel));
                progressDialogFragment.show(getFragmentManager(), "");
                viajews.startTravel(new GenericWs.GenericWsResponse() {
                    @Override
                    public void onOkResult(Object result) {
                        progressDialogFragment.dismissAllowingStateLoss();
                        onStartedTravel();
                    }

                    @Override
                    public void onErrorResult() {
                        progressDialogFragment.dismissAllowingStateLoss();
                        Toast.makeText(getActivity(), getString(R.string.noSepudoRealizarPeticion), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        cancelTravelButton = (Button) view.findViewById(R.id.btnCancelarViaje);
        cancelTravelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.cancelarViajePregunta), new SimpleDialogFragment.OkCancelListener() {
                    @Override
                    public void onCancelSelected() {

                    }

                    @Override
                    public void onOkSelected() {
                        cancelTaxi();
                    }
                }).show(getFragmentManager(), "");
            }
        });
        endTravelButton = (Button) view.findViewById(R.id.btnFinViaje);
        endTravelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyApplication.getInstance().getPreferences().getViaje().getConductor() != null)
                    VerTarifaDialogFragment.newInstance(getFinalizarListener()).show(getFragmentManager(), "");
                else
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.conductorNoAsignado)).show(getFragmentManager(), "");
            }
        });
        getTaxiButton = view.findViewById(R.id.btnPedirTaxi);
        getTaxiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (markerFrom != null && !textViewDesde.getText().equals("")) {
                    if (textViewHasta.getText().equals(""))
                        Toast.makeText(getActivity(), getString(R.string.emptyDestination), Toast.LENGTH_SHORT).show();
                    getTaxi();
                } else {
                    SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.ups), getString(R.string.completarOrigenDestino));
                    simpleDialogFragment.show(getFragmentManager(), "");
                }
            }
        });
        rectOptions = new PolylineOptions();
        mapDraw = new MapDraw();


        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    private void getDialogSeleccionarFecha() {

        Calendar cal = Calendar.getInstance();

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        final int hour = cal.get(Calendar.HOUR_OF_DAY);
        final int minute = cal.get(Calendar.MINUTE);

        DatePickerDialog d = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year, monthOfYear, dayOfMonth);
                final Date dateSelected = cal.getTime();
                TimePickerDialog t = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String dateTime = Formatter.formatDate(dateSelected) + " " + selectedHour + ":" + selectedMinute;
                        dateTimeText.setText(dateTime);
                        cleanDateTime.setVisibility(View.VISIBLE);
                    }
                }, hour, minute, true);
                t.show();
            }
        }, year, month, day);
        d.show();
    }

    private void lockTextViews() {
        textViewHasta.setOnClickListener(null);
        textViewDesde.setOnClickListener(null);
        dateTimeText.setOnClickListener(null);
    }

    private void initTextViews(){
        dateTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialogSeleccionarFecha();
            }
        });
        textViewHasta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlacePicker(REQUEST_DIRECTION_TO);
            }
        });
        textViewDesde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPlacePicker(REQUEST_DIRECTION_FROM);
            }
        });
    }

    private void onStartedTravel() {
        cancelTravelButton.setVisibility(View.GONE);
        btnABordo.setVisibility(View.GONE);
        PreferencesBo.updateTravelState(Viaje.TRAVEL_STATE_STARTED);
    }

    private void onTravelCancelled(){
        initTextViews();
        showGetTaxiButton();
        if (markerUbicacionChofer != null)
            markerUbicacionChofer.remove();
        if (ChatActivity.finishListener != null)
            ChatActivity.finishListener.finishActivity();
    }
    private void cancelTaxi(){
        final ProgressDialogFragment progressDialogCancelTravel = ProgressDialogFragment.newInstance(getString(R.string.cancelandoViaje));
        progressDialogCancelTravel.show(getFragmentManager(), "");
        travelWs.cancelTravel(new GenericWs.GenericWsResponse() {
            @Override
            public void onOkResult(Object result) {
                progressDialogCancelTravel.dismissAllowingStateLoss();
                switch (((ResultWsCall)result).getRespuesta()) {
                    case CANCELA_VIAJE_EXITO:
                        Toast.makeText(mContext, R.string.viajeCancelado, Toast.LENGTH_SHORT).show();
                        if (wsCalledFrom == CALL_FROM_CANCEL_IN_PROGRESS_DIALOG)
                            try {
                                progressDialogCancelarViaje.dismissAllowingStateLoss();
                            }catch (Exception e){e.printStackTrace();}
                        wsCalledFrom = CALL_FROM_NOTHING;
                        if (markerUbicacionChofer != null)
                            markerUbicacionChofer.remove();

                        PreferencesBo.updateTravelState(Viaje.TRAVEL_STATE_CANCELLED_BY_ME);
                        reiniciarViaje();
                        break;
                    case ERROR_CANCELA_VIAJE:
                        SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.errorCancelaViaje)).show(getFragmentManager(), "");
                        break;
                    case ERROR_CANCELA_VIAJE_:
                        SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.errorCancelaViaje)).show(getFragmentManager(), "");
                        break;
                }
            }

            @Override
            public void onErrorResult() {
                progressDialogCancelTravel.dismissAllowingStateLoss();
                SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.noSepudoRealizarPeticion));
                simpleDialogFragment.show(getFragmentManager(), "");

            }
        });
    }

    private void getTaxi(){
        final ProgressDialogFragment progressDialogGetTaxi = ProgressDialogFragment.newInstance(getString(R.string.buscandoConductor), new ProgressDialogFragment.CancelProgressListener() {
            @Override
            public void onCancelProgress() {
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.cancelarViajePregunta), new SimpleDialogFragment.OkCancelListener(){

                    @Override
                    public void onOkSelected() {
                        cancelTaxi();
                    }

                    @Override
                    public void onCancelSelected() {

                    }
                }).show(getFragmentManager(), "");
            }
        });
        progressDialogGetTaxi.show(getFragmentManager(), "");
        String dateTime = dateTimeText.getText().equals(getString(R.string.viajeParaAhora))? null: dateTimeText.getText().toString();
        travelWs.getTaxi(dateTime, markerFrom, markerTo, textViewDesde.getText().toString(), textViewHasta.getText().toString(), new GenericWs.GenericWsResponse() {
                    @Override
                    public void onOkResult(Object result) {
                        progressDialogGetTaxi.dismissAllowingStateLoss();
                        ResultWsCall resultWsCall = (ResultWsCall)result;
                        switch (resultWsCall.getRespuesta()) {
                            case CONDUCTORES_NO_DISPONIBLES:
                                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.conductoresNoDisponibles)).show(getFragmentManager(), "");
                                break;
                            case SOLICITUD_ACEPTADA_CORRECTAMENTE:
                                if (dateTimeText.getText().equals(getString(R.string.viajeParaAhora))) {
                                    ViajeBo.createTravelRequestSended(Integer.parseInt(resultWsCall.getData()), textViewDesde.getText().toString(), textViewHasta.getText().toString(), markerFrom, markerTo);
                                    onTravelRequestSended();
                                    Toast.makeText(mContext, R.string.operacionValidada, Toast.LENGTH_SHORT).show();
                                }else{
                                    String text = getString(R.string.travelProgrammedCorrectly) + " " + dateTimeText.getText();
                                    Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
                                }
                                break;
                        }
                    }

                    @Override
                    public void onErrorResult() {
                        progressDialogGetTaxi.dismissAllowingStateLoss();
                        SimpleDialogFragment simpleDialogFragment = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.error), getString(R.string.noSepudoRealizarPeticion));
                        simpleDialogFragment.show(getFragmentManager(), "");
                    }
                });
    }


    private void animateCameraToPoint(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng)
                .zoom(15) // Sets the zoom
                //.bearing(90) // Sets the orientation of the camera to
                //.tilt(30) // Sets the tilt of the camera to 30 degrees
                .build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onMapReady(GoogleMap map) {
        PedirTaxiFragment.map = map;
        MapDraw.setStartCityMap(PedirTaxiFragment.map);
        checkLocationSettings();

        checkIfWantRepeatTravel();
        checkLastTravelState();

        if (textViewDesde.getText().toString().equals("")){
            addLastLocationToMap();
        }
        showHighLightFirstRun();
    }

    private void showHighLightFirstRun(){
        if (MyApplication.getInstance().getPreferences().isFirstRun()){
            TapTargetView.showFor(getActivity(),                 // `this` is an Activity
                    TapTarget.forView(showAddicionalData, getString(R.string.additionalDataInfoTitle), getString(R.string.additionalDataInfo))
                            // All options below are optional
                            .outerCircleColor(R.color.colorPrimary)      // Specify a color for the outer circle
                            .outerCircleAlpha(0.96f)            // Specify the alpha amount for the outer circle
                            .targetCircleColor(R.color.colorPrimaryDarkSub)   // Specify a color for the target circle
                            .titleTextSize(25)                  // Specify the size (in sp) of the title text
                            .titleTextColor(R.color.colorText2)      // Specify the color of the title text
                            .descriptionTextSize(20)            // Specify the size (in sp) of the description text
                            .descriptionTextColor(R.color.colorText2)  // Specify the color of the description text
                            .textColor(R.color.colorText2)            // Specify a color for both the title and description text
                            .textTypeface(Typeface.SANS_SERIF)  // Specify a typeface for the text
                            .dimColor(R.color.colorAccent)            // If set, will dim behind the view with 30% opacity of the given color
                            .drawShadow(true)                   // Whether to draw a drop shadow or not
                            .cancelable(false)                  // Whether tapping outside the outer circle dismisses the view
                            .tintTarget(true)                   // Whether to tint the target view's color
                            .transparentTarget(false)           // Specify whether the target is transparent (displays the content underneath)
                            .icon(getResources().getDrawable(R.mipmap.baseline_keyboard_arrow_down_white_24))                     // Specify a custom drawable to draw as the target
                            .targetRadius(30),                  // Specify the target radius (in dp)
                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);      // This call is optional
                            showAddicionalDataOnClick();
                        }
                    });
            PreferencesBo.saveFirstRun();
        }
    }

    private void loadLastTravel(Viaje travel){
        addMarkerFrom(travel.getPuntoDesde());
        if (!travel.getHasta().equals("")) {
            addMarkerTo(travel.getPuntoHasta());
        }
        textViewDesde.setText(travel.getDesde());
        textViewHasta.setText(travel.getHasta());
        lockTextViews();
    }

    private void checkLastTravelState(){
        if(textViewDesde.getText().toString().trim().equals("")){
            Viaje travel = MyApplication.getInstance().getPreferences().getViaje();
            if (travel != null){
                switch (travel.getEstado()){
                    case Viaje.TRAVEL_STATE_PENDING:
                        loadLastTravel(travel);
                        onTravelRequestSended();
                        break;
                    case Viaje.TRAVEL_STATE_FINISHED:
                        reiniciarViaje();
                        break;
                    case Viaje.TRAVEL_STATE_ACCEPTED:
                        loadLastTravel(travel);
                        travelAccepted(travel);
                        onTravelRequestSended();
                        break;
                    case Viaje.TRAVEL_STATE_CANCELLED_BY_COMPANY:
                        reiniciarViaje();
                        break;
                    case Viaje.TRAVEL_STATE_STARTED:
                        loadLastTravel(travel);
                        onTravelRequestSended();
                        travelAccepted(travel);
                        onStartedTravel();
                        break;
                }
            }else{
                addLastLocationToMap();
            }
        }
    }

    private void checkIfWantRepeatTravel(){
        Bundle b = getArguments();
        if(b != null){
            Viaje repetirViaje = (Viaje) b.getSerializable(KEY_EXTRA_VIAJE);
            if(repetirViaje != null) {
                textViewDesde.setText(repetirViaje.getDesde());
                textViewHasta.setText(repetirViaje.getHasta());
                enableClickCleanTo();
                enableClickCleanFrom();
                //LatLng puntoDesde = new LatLng(repetirViaje.getPuntoDesdeLocationPoint().getCoordinates()[0], repetirViaje.getPuntoDesdeLocationPoint().getCoordinates()[1]);
                addMarkerFrom(repetirViaje.getPuntoDesde());
                //LatLng puntoHasta = new LatLng(repetirViaje.getPuntoHastaLocationPoint().getCoordinates()[0], repetirViaje.getPuntoHastaLocationPoint().getCoordinates()[1]);
                if (!repetirViaje.getHasta().equals("")) {
                    MarkerOptions markerOptionsHasta = new MarkerOptions().position(repetirViaje.getPuntoHasta()).title(getString(R.string.hasta));
                    markerTo = map.addMarker(markerOptionsHasta);
                }
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private LocationCallback getLocationCallback(){
        if (locationCallback == null) {
            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        if (location != null && location.getAccuracy() <= CityLocation.ACCUARY_LOCATION_MTS) {
                            fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                            Viaje travelÏnPreferences = MyApplication.getInstance().getPreferences().getViaje();
                            dialogSearchingLocation.setVisibility(View.GONE);
                            if (travelÏnPreferences != null && (travelÏnPreferences.getEstado().equals(Viaje.TRAVEL_STATE_ACCEPTED) || travelÏnPreferences.getEstado().equals(Viaje.TRAVEL_STATE_STARTED))) {
                                animateCameraToPoint(new LatLng(location.getLatitude(), location.getLongitude()));
                            } else {
                                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                                String adressLocation = getAdressLocation(latLng);
                                if (!adressLocation.equals("")) {
                                    textViewDesde.setText(adressLocation);
                                    enableClickCleanFrom();
                                    addMarkerFrom(latLng);
                                }
                            }
                        }
                    }
                }

                ;

            };
        }

        return locationCallback;
    }

    protected void addLastLocationToMap() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_CHECK_LOCATION_PERMISSIONS);
        }else {
            PedirTaxiFragment.map.setMyLocationEnabled(true);
            PedirTaxiFragment.map.setOnMyLocationButtonClickListener(this);
            dialogSearchingLocation.setVisibility(View.VISIBLE);
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
            fusedLocationProviderClient.requestLocationUpdates(locationRequest, getLocationCallback(), null);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CHECK_LOCATION_PERMISSIONS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addLastLocationToMap();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(intent);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case RESULT_OK:
                        addLastLocationToMap();
                        break;
                }
                break;
            case REQUEST_DIRECTION_FROM:
                if (resultCode == RESULT_OK) {
                    if (map == null)
                        mapFragment.getMapAsync(this);
                    try {
                        Place place = PlacePicker.getPlace(mContext, data);
                        textViewDesde.setText(Formatter.getFormatPlacePicker(place.getName().toString(), place.getAddress().toString()));
                        addMarkerFrom(place.getLatLng());
                        Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.getId())
                                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(PlaceBuffer places) {
                                        int cnt = places.getCount();
                                        if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                            for (int i=0; i < places.getCount(); i++) {
                                                final Place myPlace = places.get(i);
                                                Log.d("<< cache >> ", "Place found: " + myPlace.getName());
                                                Toast.makeText(getActivity(), myPlace.getAddress(), Toast.LENGTH_LONG).show();
                                                Toast.makeText(getActivity(), myPlace.getName(), Toast.LENGTH_LONG).show();
                                            }
                                        } else {
                                            Log.d("<< cache >> ", "Place not found");
                                        }
                                        places.release();
                                    }
                                });

                        enableClickCleanFrom();
                    }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getContext(), getString(R.string.tryAgain), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case REQUEST_DIRECTION_TO:
                if (resultCode == RESULT_OK) {
                    if (map == null)
                        mapFragment.getMapAsync(this);
                    try {
                        Place place = PlacePicker.getPlace(mContext, data);
                        textViewHasta.setText(Formatter.getFormatPlacePicker(place.getName().toString(), place.getAddress().toString()));
                        addMarkerTo(place.getLatLng());
                        enableClickCleanTo();
                    }catch (Exception e){
                        Toast.makeText(getContext(), getString(R.string.error) + ". " + getString(R.string.intenteNuevamente), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case REQUEST_CHECK_LOCATION_PERMISSIONS:
                addLastLocationToMap();
                break;
        }
    }



    @Override
    public void onConnectionSuspended(int i) {
        //Toast.makeText(mContext, R.string.connectionGoogleApiFailed, Toast.LENGTH_SHORT);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public boolean onMyLocationButtonClick() {
        addLastLocationToMap();
        /*if (mGoogleApiClient.isConnected())
            addLastLocationToMap();
        else
            mGoogleApiClient.connect();*/
        return true;
    }

    private String getAdressLocation(LatLng latLng){
        String adress = "";
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        List<android.location.Address> list;
        try {
            list = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            adress = list.get(0).getAddressLine(ADRESS_LINE_DIRECCTION) /*+ ", " + list.get(0).getAddressLine(ADRESS_LINE_PROVINCIA) + ", " + list.get(0).getAddressLine(ADRESS_LINE_PAIS)*/;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return adress;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    private void reiniciarViaje(){
        addLastLocationToMap();
        if(ChatActivity.finishListener != null)
            ChatActivity.finishListener.finishActivity();
        showGetTaxiButton();
        initTextViews();
        disableClickCleanFrom();
        disableClickCleanTo();
        if (markerFrom != null){
            markerFrom.remove();
            markerFrom = null;
        }

        if (markerTo != null){
            markerTo.remove();
            markerTo = null;
        }
        if (markerUbicacionChofer != null)
            markerUbicacionChofer.remove();
    }

    private VerTarifaDialogFragment.FinalizarListener getFinalizarListener() {
        return new VerTarifaDialogFragment.FinalizarListener() {
            @Override
            public void finalizarListener() {
                Bundle b = new Bundle();
                b.putSerializable(KEY_EXTRA_VIAJE, MyApplication.getInstance().getPreferences().getViaje());
                CalificarDialogFragment.newInstance(b, new CalificarDialogFragment.FinalizarCalificacionListener() {
                    @Override
                    public void onFinalizarCalificacionListener() {
                        Toast.makeText(mContext, R.string.operacionValidada, Toast.LENGTH_SHORT).show();
                        PreferencesBo.updateTravelState(Viaje.TRAVEL_STATE_FINISHED);
                        reiniciarViaje();
                    }
                }).show(getActivity().getSupportFragmentManager(), "");
            }
        };
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        SimpleDialogFragment s = SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK_CANCEL, getString(R.string.llamarConductor), new SimpleDialogFragment.OkCancelListener(){

            @Override
            public void onOkSelected() {
            }

            @Override
            public void onCancelSelected() {

            }
        });
    }

    private MyFirebaseMessagingService.OnMessageRecibed getOnMansajeRecibed(){
        return new MyFirebaseMessagingService.OnMessageRecibed() {
            @Override
            public void doCallBack(final Object o, String indicadorFirebase) {
                doTask(o, indicadorFirebase);
            }
        };
    }

    private void checkLocationSettings(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        LocationSettingsRequest locationSettingsRequest = builder.build();
        SettingsClient settingsClient = LocationServices.getSettingsClient(getActivity());
        settingsClient.checkLocationSettings(locationSettingsRequest);
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);

                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                resolvable.startResolutionForResult(
                                        getActivity(),
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            }
        });
    }

    private void onTravelRequestSended(){
        /*if (PedirTaxiFragment.map != null)
            PedirTaxiFragment.map.setMyLocationEnabled(false);*/
        cancelTravelButton.setVisibility(View.VISIBLE);
        endTravelButton.setVisibility(View.VISIBLE);
        btnABordo.setVisibility(View.VISIBLE);
        getTaxiButton.setVisibility(View.GONE);
        lockTextViews();
    }

    private void travelAccepted(final Viaje viaje){
        final MarkerOptions markerOptions = new MarkerOptions().position(viaje.getConductor().getLastLocation()).title("").icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_retaxi_marker));
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewDesde.setText(viaje.getDesde());
                textViewHasta.setText(viaje.getHasta());
                if (viaje.isDiferido()) {
                    try {
                        Date date = Formatter.convertToDateTimeDjangoTravel(viaje.getFechaHora());
                        dateTimeText.setText(Formatter.formatDateTimeMinutes(date));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }else{
                    dateTimeText.setText(getString(R.string.viajeParaAhora));}
                addMarkerFrom(viaje.getPuntoDesde());
                if (!viaje.getHasta().equals(""))
                    addMarkerTo(viaje.getPuntoHasta());
                if (markerUbicacionChofer != null)
                    markerUbicacionChofer.remove();
                markerUbicacionChofer = map.addMarker(markerOptions);
                map.setInfoWindowAdapter(new InfoWindowMarker(mContext, markerUbicacionChofer, viaje));
                markerUbicacionChofer.showInfoWindow();
                animateCameraToPoint(viaje.getConductor().getLastLocation());

            }
        });
        MyApplication.getInstance().getPreferences().setViaje(viaje);
        PreferencesBo.saveTravelInPreferences(viaje);
    }

    private void doTask(final Object o, String indicadorFirebase){
        switch (indicadorFirebase){
            case MESSAGE_TYPE_REQUEST_ACCEPTED:
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        onTravelRequestSended();
                        lockTextViews();
                    }
                });
                final Viaje viaje = (Viaje) o;
                travelAccepted(viaje);
                break;
            case MESSAGE_TYPE_CONDUCTORES_NO_DISPONIBLES:
                //progressDialogFragment.dismissAllowingStateLoss();
                SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.conductoresNoDisponibles)).show(getFragmentManager(), "");
                break;
            case MESSAGE_TYPE_CANCELADO_X_CONDUCTOR:
                if(ChatActivity.finishListener != null)
                    ChatActivity.finishListener.finishActivity();
                if(itsFragmentActive) {
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.activeTravel), getString(R.string.atencion), new SimpleDialogFragment.OkListener() {
                        @Override
                        public void onOkSelected() {

                        }
                    }).show(getFragmentManager(), "");
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                reiniciarViaje();
                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }else {
                    MyApplication.getInstance().setFirebaseMessajeType(MESSAGE_TYPE_CANCELADO_X_CONDUCTOR);
                    startNotification(getString(R.string.conductoresCancelo), mContext);
                }
                getRingtonNotification(mContext, RingtoneManager.TYPE_RINGTONE);
                break;
            case MESSAGE_TYPE_UBICACION_CONDUCTOR:
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            LocationPoint locationPoint = (LocationPoint) o;
                            LatLng latLng = new LatLng(locationPoint.getCoordinates()[0], locationPoint.getCoordinates()[1]);
                            markerUbicacionChofer.setPosition(latLng);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
                break;
        }
    }
}
