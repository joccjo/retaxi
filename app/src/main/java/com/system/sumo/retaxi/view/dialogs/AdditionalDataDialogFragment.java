package com.system.sumo.retaxi.view.dialogs;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.dialogs.generics.BaseDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.ProgressDialogFragment;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.ws.GenericWs;
import com.system.sumo.retaxi.ws.LoginWs;

import java.util.Calendar;
import java.util.Date;

import static com.system.sumo.retaxi.dto.Usuario.SEX_MAN;
import static com.system.sumo.retaxi.dto.Usuario.SEX_WOMAN;

/**
 * Created by usuario on 06/11/2016.
 */

public class AdditionalDataDialogFragment extends BaseDialogFragment {
    TextView textViewBirthDate;
    ProgressDialogFragment progressDialogFragment;
    static FinalizarCalificacionListener finalizarCalificacionListener;
    public static AdditionalDataDialogFragment newInstance(FinalizarCalificacionListener listener){
        AdditionalDataDialogFragment calificarDialogFragment = new AdditionalDataDialogFragment();
        finalizarCalificacionListener = listener;
        return calificarDialogFragment;
    }

    public interface FinalizarCalificacionListener {
        void onFinalizarCalificacionListener();
    }

    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        View view = View.inflate(getActivity(), R.layout.lyt_additional_data_dialog_fragment, null);

        alertDialogBuilder.setView(view);
        initComponents(view);
        return alertDialogBuilder.show();
    }

    private void initComponents(View view) {
        final RadioButton radioButtonMan = view.findViewById(R.id.radioButtonMan);
        final RadioButton radioButtonWoman = view.findViewById(R.id.radioButtonWoman);
        textViewBirthDate = view.findViewById(R.id.textViewBirthDate);
        textViewBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialogSeleccionarFecha();
            }
        });
        final EditText editTextPhone = view.findViewById(R.id.editTextPhone);

        Button buttonAccept = view.findViewById(R.id.buttonAccept);
        buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!textViewBirthDate.getText().toString().equals("") && !editTextPhone.getText().toString().equals("") && (radioButtonMan.isChecked() || radioButtonWoman.isChecked())){
                    final ProgressDialogFragment progressDialogFragment = ProgressDialogFragment.newInstance(getString(R.string.ingresando));
                    progressDialogFragment.show(getFragmentManager(), "");
                    LoginWs loginWs = new LoginWs();
                    loginWs.setAdditionalData(editTextPhone.getText().toString(),
                            (radioButtonMan.isSelected()) ? SEX_MAN : SEX_WOMAN,
                            textViewBirthDate.getText().toString(), new GenericWs.GenericWsResponse() {
                                @Override
                                public void onOkResult(Object result) {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    finalizarCalificacionListener.onFinalizarCalificacionListener();
                                }

                                @Override
                                public void onErrorResult() {
                                    progressDialogFragment.dismissAllowingStateLoss();
                                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.noSepudoRealizarPeticion)).show(getFragmentManager(), "");
                                }
                            });
                }else{
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.completeAllData)).show(getFragmentManager(), "");
                }
            }
        });

    }

    private void getDialogSeleccionarFecha(){
        Calendar cal = Calendar.getInstance();

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog d = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year, monthOfYear, dayOfMonth);
                final Date dateSelected = cal.getTime();
                try {
                    textViewBirthDate.setText(Formatter.formatJulianDate(dateSelected));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, year, month, day);
        d.show();
    }
}
