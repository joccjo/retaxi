package com.system.sumo.retaxi.view.chofer;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.system.sumo.retaxi.LoginActivity;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.bo.UsuarioBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.services.LocationService;

import static com.system.sumo.retaxi.utils.Constantes.NUMBER_STARS_RATING;


public class CuentaChoferFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener{
    private GoogleApiClient mGoogleApiClient;
    Button btnCloseSession;
    RoundedImageView userPicture;
    UsuarioBo usuarioBo = new UsuarioBo();

    public CuentaChoferFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cuenta_chofer, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponents();
    }

    private void initComponents(){
        //Usuario usuario = null;
        try {
            //usuario = usuarioBo.recoveryByUserType(Usuario.TIPO_CONDUCTOR, getActivity()).get(0);
            userPicture = (RoundedImageView) getView().findViewById(R.id.user_silueta);
            Picasso.get().load(MyApplication.getInstance().getPreferences().getUsuario().getUrlImagen())
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(userPicture, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError(Exception e) {
                            Picasso.get().load(MyApplication.getInstance().getPreferences().getUsuario().getUrlImagen())
                                    .into(userPicture);
                        }
                    });
            TextView tvNombre = (TextView) getView().findViewById(R.id.nombreId);
            tvNombre.setText(MyApplication.getInstance().getPreferences().getUsuario().getNombre());
            TextView tvAppelido = (TextView) getView().findViewById(R.id.apellidoId);
            tvAppelido.setText(MyApplication.getInstance().getPreferences().getUsuario().getApellido());
            RatingBar ratingBar = (RatingBar) getView().findViewById(R.id.ratingBar);
            ratingBar.setNumStars(NUMBER_STARS_RATING);
            ratingBar.setRating(MyApplication.getInstance().getPreferences().getUsuario().getReputation()/MyApplication.getInstance().getPreferences().getUsuario().getViajes_realizados());
        } catch (Exception e) {
            e.printStackTrace();
        }
        btnCloseSession = (Button) getView().findViewById(R.id.buttonCerrarSesion);
        btnCloseSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                        new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status status) {
                                if(status.isSuccess()) {
                                    MyApplication.getInstance().setLocationRequest(null);
                                    MyApplication.getInstance().setChoferConectado(false);
                                    getActivity().stopService(new Intent(getActivity(), LocationService.class));
                                    PreferencesBo.deleteUserFromPreferences();
                                    startActivity(new Intent(getActivity(), LoginActivity.class));
                                    getActivity().finish();
                                }
                            }
                        });
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(getActivity());
        mGoogleApiClient.disconnect();
    }
}
