package com.system.sumo.retaxi.bo;

import android.content.Context;
import android.content.SharedPreferences;

import com.system.sumo.retaxi.dao.DaoFactory;
import com.system.sumo.retaxi.dao.IUsuarioDao;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;

import java.util.List;

import static com.system.sumo.retaxi.utils.Constantes.KEY_TOKEN_OAUTH;
import static com.system.sumo.retaxi.utils.Constantes.KEY_USER_TYPE;

/**
 * Created by usuario on 21/09/2016.
 */
public class UsuarioBo {

    DaoFactory daoFactory = DaoFactory.getDaoFactory(DaoFactory.SQLITE);
    IUsuarioDao iUsuarioDao = daoFactory.getUsuarioDao();

    public void createUsuario(Usuario usuario) throws Exception{
        iUsuarioDao.create(usuario);
    }

    public Usuario recoveryById(int id) throws Exception{
        return iUsuarioDao.recoveryByID(id);
    }

    public List<Usuario> recoveryAll(Context context) throws Exception{
        return iUsuarioDao.recoveryAll();
    }

    public List<Usuario> recoveryByParameter(Context context) throws Exception{
        return iUsuarioDao.recoveryAll();
    }

    public List<Usuario> recoveryByUserType(int userType) throws Exception{
        return iUsuarioDao.recoveryByUserType(userType);
    }

    public void deleteByUserType(int userType)throws Exception{
        iUsuarioDao.deleteByUserType(userType);
    }
}
