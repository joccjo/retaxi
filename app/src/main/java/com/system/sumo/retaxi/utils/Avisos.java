package com.system.sumo.retaxi.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import androidx.core.app.NotificationCompat;
import android.widget.RemoteViews;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ChatMessageBo;
import com.system.sumo.retaxi.dto.ChatMessage;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.view.chat.ChatActivity;
import com.system.sumo.retaxi.view.MainActivity;

import java.util.List;


/**
 * Created by eMa on 30/11/2016.
 */

public class Avisos {
    private static int NOTIFICATION_CANCEL = 0;
    public static int ID_NOTIFICATION_CHAT = 1;
    public static int ID_NOTIFICATION_ACCEPTED_TRAVEL = 2;
    public static int ID_NOTIFICATION_LOCATION = 3;
    public static long[] paternVibrateNotification = {0, 500, 500};
    public static void getRingtonNotification(Context context, int type_ringtone){
        final Vibrator v;
        final Ringtone ringtone;
        v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        //long[] patern = {0, 500, 500};
        v.vibrate(paternVibrateNotification, 1);
        ringtone = RingtoneManager.getRingtone(context,
                RingtoneManager.getDefaultUri(type_ringtone));
        if (ringtone != null) {
            ringtone.play();
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);//3 segundos
                    v.cancel();
                    if(ringtone != null)
                        ringtone.stop();
                } catch (Exception e) {
                    e.printStackTrace();
                    v.cancel();
                    if(ringtone != null)
                        ringtone.stop();
                }
            }
        });
        thread.start();

    }

    public static void startNotification(Context context){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + 911));
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, callIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        RemoteViews notificationView = new RemoteViews(context.getPackageName(),
                R.layout.notification_panic_button);
        notificationView.setOnClickPendingIntent(R.id.imageCallId, pendingIntent);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.mipmap.ic_search)
                .setCustomContentView(notificationView)
                .setDefaults(Notification.FLAG_NO_CLEAR)
                .setOngoing(true);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFICATION_CANCEL, notification.build());
    }

    public static void startNotification(String mensaje, Context context){
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, "")
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setSmallIcon(R.drawable.icono_blanco_chat)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_warning_black_24dp))
                        .setContentTitle(context.getString(R.string.atencion))
                        .setContentText(mensaje)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("","Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(ID_NOTIFICATION_ACCEPTED_TRAVEL, notificationBuilder.build());
    }

    public static void startNotificationChat(Context context, ChatMessage chatMessage){
        Intent intent = new Intent(context, ChatActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        /*String titulo;
        String urlImg;
        if(MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario() == Usuario.TIPO_CONDUCTOR){
            titulo = MyApplication.getInstance().getViaje().getPasajero().getNombre();
            urlImg = MyApplication.getInstance().getViaje().getPasajero().getUrlImagen();
        }else{
            titulo = MyApplication.getInstance().getViaje().getConductor().getNombre();
            urlImg = MyApplication.getInstance().getViaje().getConductor().getUrlImagen();
        }*/

        /*RemoteViews notificationView = new RemoteViews(context.getPackageName());
        notificationView.setOnClickPendingIntent(R.id.idLinearLayout, pendingIntent);*/
        CharSequence contentTitle = MyApplication.getInstance().getApplicationContext().getString(R.string.app_name);
        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.app_icon);
        CharSequence contentText = chatMessage.getMessage();

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        ChatMessageBo chatMessageBo = new ChatMessageBo();
        List<ChatMessage> messages = null;
        try {
            messages = chatMessageBo.getUnSeenMessages();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(ChatMessage chatMessagee: messages){
            if(chatMessagee.isNew())
                inboxStyle.addLine(chatMessagee.getMessage());
        }
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context, "")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.icono_blanco_chat)
                .setLargeIcon(icon)
                .setContentIntent(pendingIntent)
                .setStyle(inboxStyle)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(paternVibrateNotification)
                .setSound(alarmSound)
                //.setCustomContentView(notificationView)
                .setContentTitle(contentTitle)
                .setContentText(contentText);
                //.build();


        /*final RemoteViews contentView = notification.contentView;
        final int iconId = android.R.id.icon;


        Picasso
                .with(context)
                .load(urlImg)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(contentView, iconId, ID_NOTIFICATION_CHAT, notification);*/
        //notificationView.setTextViewText(R.id.tittleMessage, titulo);
        //notificationView.setTextViewText(R.id.messageChat, chatMessage.getMessage());

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(ID_NOTIFICATION_CHAT, notification.build());
    }
}
