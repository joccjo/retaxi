package com.system.sumo.retaxi.dto.mapping;

/**
 * Created by eMa on 2/1/2017.
 */

public class ChatMessageMapping {
    public static String TABLA = "chat_message";
    public static String ID = "id";
    public static String MESSAGE = "message";
    public static String DATE_TIME = "date_time";
    public static String IS_ENVIADO = "is_enviado";
    public static String IS_NEW = "is_new";
    public static String IS_ME = "is_me";
    public static String TYPE = "type";
}
