package com.system.sumo.retaxi.dto;

import com.google.gson.annotations.SerializedName;
import com.system.sumo.retaxi.utils.Constantes;

import java.util.Date;

/**
 * Created by eMa on 21/12/2016.
 */

public class ChatMessage {
    public enum MessageTypes {
        COMUN("C"),
        RECLAMO("R");

        private String type;
        MessageTypes(String type) {
            this.type = type;
        }
        public String type() {
            return type;
        }
    }
    public static String ORIGEN_MENSAJE_CONDUCTOR = "C";
    public static String ORIGEN_MENSAJE_PASAJERO = "P";
    private long id;
    private boolean isMe;
    @SerializedName(Constantes.MENSAJE_CHAT)
    private String message;
    private Long userId;
    @SerializedName(Constantes.FECHA_CHAT)
    private Date dateTime;
    private boolean isEnviado;
    private boolean isNew = true;
    private String tipo;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public boolean getIsme() {
        return isMe;
    }
    public void setMe(boolean isMe) {
        this.isMe = isMe;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Date getDate() {
        return dateTime;
    }

    public void setDate(Date dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isEnviado() {
        return isEnviado;
    }

    public void setEnviado(boolean enviado) {
        isEnviado = enviado;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
