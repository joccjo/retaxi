package com.system.sumo.retaxi.dto;

import com.google.gson.annotations.SerializedName;
import com.system.sumo.retaxi.utils.Constantes;

import java.io.Serializable;

/**
 * Created by usuario on 18/10/2016.
 */

public class Movil implements Serializable{
    public static String EXTRA_MOVIL = "extra_movil";
    private int id;
    private String marca;
    @SerializedName(Constantes.MODELO)
    private String modelo;
    private String patente;
    private String color;

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString(){
        return  getMarca() + " " + getModelo() + " " + getPatente() + " " + getColor();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movil movil = (Movil) o;

        return patente != null ? patente.equals(movil.patente) : movil.patente == null;

    }

    @Override
    public int hashCode() {
        return patente != null ? patente.hashCode() : 0;
    }
}
