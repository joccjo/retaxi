package com.system.sumo.retaxi.dao;

import android.content.Context;

import com.system.sumo.retaxi.dto.Viaje;

import java.util.List;

/**
 * Created by usuario on 21/09/2016.
 */
public interface IViajeDao extends IGenericDao<Viaje, Integer> {
    List<Viaje> recoveryByPeriodo(String fechaDesde, String fechaHasta) throws Exception;

    List<Viaje> recoveryDiferidos() throws Exception;
}
