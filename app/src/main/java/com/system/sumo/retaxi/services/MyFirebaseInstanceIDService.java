package com.system.sumo.retaxi.services;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.utils.RestCustomerVolley;

import org.json.JSONException;
import org.json.JSONObject;

import static com.system.sumo.retaxi.utils.ServicesRoutes.UPDATE_FIREBASE_TOKEN_DRIVER;
import static com.system.sumo.retaxi.utils.ServicesRoutes.UPDATE_FIREBASE_TOKEN_RIDER;

/**
 * Created by usuario on 15/09/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseMessagingService implements
        Response.Listener<String>,
        Response.ErrorListener{
        //RestCustomer.OnRequestRestCustomerFinish{

    private static String token;
    private static String SEND_REGISTRATION_TOKEN_SERVER = "SEND_TOKEN";

    @Override
    public void onCreate() {
        super.onCreate();
        token = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer();
    }

    @Override
    public void onNewToken(@NonNull String localToken) {
        super.onNewToken(token);
        token = localToken;
        Log.d("", "Refreshed token: " + token);
        sendRegistrationToServer();
    }

    private void sendRegistrationToServer(){
        if(MyApplication.getInstance().getPreferences().getUsuario() != null) {
            String url;
            if (MyApplication.getInstance().getPreferences().getUsuario().getTipoUsuario() == Usuario.TIPO_CONDUCTOR) {
                url = UPDATE_FIREBASE_TOKEN_DRIVER;
            } else {
                url = UPDATE_FIREBASE_TOKEN_RIDER;
            }

            JSONObject body = new JSONObject();
            try {
                body.put("token", token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RestCustomerVolley.addQueue(Request.Method.POST, url, body, true, null);

        }
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        MyFirebaseInstanceIDService.token = token;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
