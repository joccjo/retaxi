package com.system.sumo.retaxi.dto.mapping;

/**
 * Created by usuario on 20/09/2016.
 */
public class ViajeMapping {
    public static String TABLA = "viaje";
    public static String ID = "id";
    public static String LATITUD_DESDE = "latitud_desde";
    public static String LATITUD_HASTA = "latitud_hasta";
    public static String LONGITUD_DESDE = "longitud_desde";
    public static String LONGITUD_HASTA = "longitud_hasta";
    public static String DESDE = "desde";
    public static String HASTA = "hasta";
    public static String MOTNO = "monto";
    public static String FECHA = "fecha";
    public static String PLACE_ID = "place_id";
    public static String USER_TYPE = "user_type";
    public static String TIPE = "type";
}
