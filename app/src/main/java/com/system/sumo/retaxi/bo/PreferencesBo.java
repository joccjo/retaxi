package com.system.sumo.retaxi.bo;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Preferences;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.Viaje;

import static com.system.sumo.retaxi.dto.Preferences.KEY_ACESS_TOKEN;
import static com.system.sumo.retaxi.dto.Preferences.KEY_MY_PREFERENCES;
import static com.system.sumo.retaxi.dto.Preferences.KEY_REFRESH_TOKEN;
import static com.system.sumo.retaxi.dto.Preferences.KEY_USER;
import static com.system.sumo.retaxi.dto.Preferences.KEY_TRAVEL;
import static com.system.sumo.retaxi.dto.Preferences.KEY_IS_FIRST_RUN;

public class PreferencesBo {

    public static void saveUserInPreferences(Usuario usuario){
        if (usuario.getEmail() != null) {
            Gson gson = new Gson();
            String usuarioJson = gson.toJson(usuario);
            saveObject(KEY_USER, usuarioJson);
        }
    }

    private static void saveObject(String key, Object object){
        SharedPreferences sharedPref = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        if (object instanceof String)
            editor.putString(key, (String) object);
        else if (object instanceof Boolean)
            editor.putBoolean(key, (boolean) object);
        editor.apply();
        MyApplication.getInstance().loadPreferences();
    }

    public static void saveTravelInPreferences(Viaje viaje){
        if (viaje != null) {
            Gson gson = new Gson();
            String travelJson = gson.toJson(viaje);
            saveObject(KEY_TRAVEL, travelJson);
        }
    }

    public static void saveTokensInPreferences(String accessToken){
        saveObject(KEY_ACESS_TOKEN, accessToken);
    }

    public static void saveFirstRun(){
        saveObject(KEY_IS_FIRST_RUN, false);
    }

    public static void deleteUserFromPreferences(){
        SharedPreferences sharedPref = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(KEY_USER);
        editor.apply();
        MyApplication.getInstance().loadPreferences();
    }

    public static Preferences loadPreferences(){
        Preferences preferences = new Preferences();
        SharedPreferences sharedPreferences = getSharedPreferences();
        Gson gson = new Gson();
        Usuario user = gson.fromJson(sharedPreferences.getString(KEY_USER, ""), Usuario.class);
        preferences.setUsuario(user);
        preferences.setFirstRun(sharedPreferences.getBoolean(KEY_IS_FIRST_RUN, true));
        preferences.setAccessTokenBackend(sharedPreferences.getString(KEY_ACESS_TOKEN, ""));
        preferences.setRefreshTokenBackend(sharedPreferences.getString(KEY_REFRESH_TOKEN, ""));
        Viaje travel = gson.fromJson(sharedPreferences.getString(KEY_TRAVEL, ""), Viaje.class);
        preferences.setViaje(travel);
        return preferences;
    }

    private static SharedPreferences getSharedPreferences(){
        return MyApplication.getInstance().getApplicationContext().getSharedPreferences(KEY_MY_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void updateTravelState(String state) {
        MyApplication.getInstance().getPreferences().getViaje().setEstado(state);
        saveTravelInPreferences(MyApplication.getInstance().getPreferences().getViaje());
    }
}
