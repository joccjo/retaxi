package com.system.sumo.retaxi.dto;

public class Preferences {
    private Usuario usuario;
    private String refreshTokenBackend;
    private String accessTokenBackend;
    private boolean isFirstRun;
    private Viaje viaje;
    public static String KEY_USER = "KEY_USER";
    public static String KEY_IS_FIRST_RUN = "KEY_IS_FIRST_RUN";
    public static String KEY_TRAVEL = "KEY_TRAVEL";
    public static String KEY_ACESS_TOKEN = "KEY_ACESS_TOKEN";
    public static String KEY_REFRESH_TOKEN = "KEY_REFRESH_TOKEN";
    public static String KEY_MY_PREFERENCES = "KEY_MY_PREFERENCES";

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getRefreshTokenBackend() {
        return refreshTokenBackend;
    }

    public void setRefreshTokenBackend(String refreshTokenBackend) {
        this.refreshTokenBackend = refreshTokenBackend;
    }

    public Viaje getViaje() {
        return viaje;
    }

    public void setViaje(Viaje viaje) {
        this.viaje = viaje;
    }

    public String getAccessTokenBackend() {
        return accessTokenBackend;
    }

    public void setAccessTokenBackend(String accessTokenBackend) {
        this.accessTokenBackend = accessTokenBackend;
    }

    public boolean isFirstRun() {
        return isFirstRun;
    }

    public void setFirstRun(boolean firstRun) {
        isFirstRun = firstRun;
    }
}
