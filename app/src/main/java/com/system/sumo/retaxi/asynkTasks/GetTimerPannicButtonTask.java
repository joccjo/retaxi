package com.system.sumo.retaxi.asynkTasks;

import android.os.AsyncTask;

/**
 * Created by usuario on 05/09/2016.
 */
public class GetTimerPannicButtonTask extends AsyncTask<Void, Void, Integer> {
    private GetTimerListener mListener;
    private int contador;

    public GetTimerPannicButtonTask(GetTimerListener listener, int contador){
        mListener = listener;
        this.contador = contador;
    }

    @Override
    protected Integer doInBackground(Void... voide) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer contador) {
        super.onPostExecute(contador);
        mListener.onDone(this.contador);
    }

    public interface GetTimerListener{
        void onDone(int contador);
    }
}
