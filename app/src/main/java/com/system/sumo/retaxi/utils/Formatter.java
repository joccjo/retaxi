package com.system.sumo.retaxi.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Formatter {
	
	public static String FORMAT_MONEY = "#,##0.00";
	public static String FORMAT_MONEY_C_PESOS = "$ #,##0.00";

	private static final String[] formats = {
			"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ",
			"yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'",
			"yyyy-MM-dd'T'HH:mm:ssZ" };

	public static String getFormatPlacePicker(String name, String adress){
		try {
			if (name.contains("°")) {
				/*String[] parts = adress.split(",");

				String place = parts[0] + ", ";
				String[] ciudad = parts[1].trim().split(" ");
				place += ciudad[1] + ", ";
				place += parts[2].trim();*/
				return adress;
			}else{
				return name;
			}
		}catch (Exception e){
			return adress;
		}
		//Leandro N. Alem Oeste 1-99, H3508BNK Resistencia, Chaco, Argentina
	}

	public static String formatMoney(float value) {
		DecimalFormat formater = new DecimalFormat(FORMAT_MONEY_C_PESOS);
		return formater.format(value);
	}

	public static String formatNumber(Number value, String pattern) {
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(value);
	}

	public static double[] formatStringToDouble(String posicion){
		double[] posiciones = new double[2];
		posiciones[0] = 0.0;
		posiciones[1] = 0.0;
		if(posicion != null && !posicion.equals("")) {
			posicion = posicion.substring(17, posicion.length() - 1);
			String[] latLng = posicion.split("\\s+");
			posiciones[0] = Double.parseDouble(latLng[0]);
			posiciones[1] = Double.parseDouble(latLng[1]);
		}
		return posiciones;
	}
/**
 * 
 * @param date
 * @return string of date in format dd/mm/aaaa
 */
	public static String formatDate(Date date) {
		String sDate = "";
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
			sDate = simpleDateFormat.format(date);
		}
		return sDate;
	}
	public static String formatDateTime(Date date) {
		String sDate = "";
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			sDate = simpleDateFormat.format(date);
		}
		return sDate;
	}

	public static String formatDate(String date) throws Exception {
		String initDateFormat = "dd/MM/yyyy";
		String endDateFormat = "yyyy-MM-dd";
		Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String parsedDate = formatter.format(initDate);

		return parsedDate;
	}

	public static Date convertStringToDate(String date) throws Exception{
		String initDateFormat = "dd/MM/yyyy";
		return new SimpleDateFormat(initDateFormat).parse(date);
	}

	public static String formatDateTime(String date) throws Exception {
		String initDateFormat = "dd/MM/yyyy mm:ss";
		String endDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'";
		Date initDate = new SimpleDateFormat(initDateFormat).parse(date);
		SimpleDateFormat formatter = new SimpleDateFormat(endDateFormat);
		String parsedDate = formatter.format(initDate);

		return parsedDate;
	}

	public static String formatDateTimeMinutes(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return simpleDateFormat.format(date);
	}

	public static String formatDateWs(Date date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
		return sdf.format(date);
	}

	public static String formatJulianDate(Date date) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}

	public static Date convertToDateTime(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		return simpleDateFormat.parse(date);
	}

	public static String formatHoursMinutes(Date date) {
	    try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
            return simpleDateFormat.format(date);
        }catch (Exception e){
	        e.printStackTrace();
	        return "";
        }
	}

	public static Date convertToDateTimeText(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		return sdf.parse(date);
	}

	public static Date convertToDateTimeDjango(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
		return simpleDateFormat.parse(date);
	}

	public static Date convertToDateTimeDjangoTravel(String date) throws ParseException {
		for (String format : formats){
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			try{
				return sdf.parse(date);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		return null;
	}

	public static Date convertToDateTimeWs(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		return sdf.parse(date);
	}

	public static Date convertToDateWs(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(date);
	}

	public static Date convertToDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.parse(date);
	}

	public static String formatPercent(float value) {
		DecimalFormat formater = new DecimalFormat("##0.00%");
		return formater.format(value);
	}

	public static String formatTwoDecimals(String valuee) {
		DecimalFormat formater = new DecimalFormat("#0.00");
		return formater.format(Float.parseFloat(valuee));
	}

	public static boolean isValidCuit(String cuit) {
		if(cuit.length()<11 && cuit.length()!=0){
			return false;
		}
		if(cuit.length()==0){
			return true;
		}
		int factor = 5;
		int[] c = new int[11];
		int resultado = 0;
		
		for (int i = 0; i < 10; i++) {
			c[i] = Integer.valueOf(Character.toString(cuit.charAt(i))).intValue();
			resultado = resultado + c[i] * factor;
			factor = (factor == 2) ? 7 : factor - 1;
		}
		int modOnce = resultado % 11;
		int control = 11 - modOnce;
		if (control == 11) {
			control = 0;
		} else if (control == 10) {
			control = 9;
		}
		int posOnce =Integer.valueOf(Character.toString(cuit.charAt(10)));
		return control == posOnce;

	}
	public static float parseFloat(String string){
		try {
			return Float.parseFloat(string);
		} catch (NumberFormatException exception){
			return 0;
		}
	}
	public static int parseInt(String string){
		try {
			return Integer.parseInt(string);
		} catch (NumberFormatException exception){
			return 0;
		}
	}

	public static boolean parseBooleanString(String value) {
		if(value !=null){
			if(value.equals("S"))return true;
			
		}
		return false;
		
	}
}
