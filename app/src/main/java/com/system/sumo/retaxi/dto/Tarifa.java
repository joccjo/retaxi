package com.system.sumo.retaxi.dto;

import java.io.Serializable;

/**
 * Created by usuario on 02/11/2016.
 */

public class Tarifa implements Serializable{
    private String minuto_espera;
    private String ficha;
    private String cuadras;
    private String adicional;
    private String bajada_de_bandera;
    private String precio_viaje;
    private String first_name;
    private String metros;

    public String getMinuto_espera() {
        return minuto_espera;
    }

    public void setMinuto_espera(String minuto_espera) {
        this.minuto_espera = minuto_espera;
    }

    public String getFicha() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha = ficha;
    }

    public String getCuadras() {
        return cuadras;
    }

    public void setCuadras(String cuadras) {
        this.cuadras = cuadras;
    }

    public String getAdicional() {
        return adicional;
    }

    public void setAdicional(String adicional) {
        this.adicional = adicional;
    }

    public String getBajada_de_bandera() {
        return bajada_de_bandera;
    }

    public void setBajada_de_bandera(String bajada_de_bandera) {
        this.bajada_de_bandera = bajada_de_bandera;
    }

    public String getPrecio_viaje() {
        return precio_viaje;
    }

    public void setPrecio_viaje(String precio_viaje) {
        this.precio_viaje = precio_viaje;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMetros() {
        return metros;
    }

    public void setMetros(String metros) {
        this.metros = metros;
    }
}
