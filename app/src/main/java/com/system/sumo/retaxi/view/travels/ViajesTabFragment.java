package com.system.sumo.retaxi.view.travels;

import android.app.DatePickerDialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.ViajeBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.utils.Formatter;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.pasajero.PedirTaxiFragment;

import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class ViajesTabFragment extends Fragment {
    private List<Viaje> viajes;
    private ListView listViewViajes;
    private TextView txtFechaDesde;
    private TextView txtFechaHasta;
    private Date fechaDesde;
    private Date fechaHasta;
    private static final int DIALOG_FECHA_DESDE = 0;
    private static final int DIALOG_FECHA_HASTA = 1;

    public ViajesTabFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_viajes_tab, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initComponents();
        initVar();
        loadViajes();
    }

    private void loadViajes(){
        ViajeBo viajeBo = new ViajeBo();
        try {
            String fechaDesde = Formatter.formatDate(txtFechaDesde.getText().toString());
            String fechaHasta = Formatter.formatDate(txtFechaHasta.getText().toString());

            viajes = viajeBo.recoveryByPeriodo(fechaDesde, fechaHasta);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(viajes != null) {
            listViewViajes.setAdapter(new ViajesArrayAdapter(getActivity(), R.layout.listview_item_viajes, viajes));
            if(MyApplication.getUserType() == Usuario.TIPO_PASAJERO){
                listViewViajes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                        SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.repetirViaje), new SimpleDialogFragment.OkCancelListener() {
                            @Override
                            public void onCancelSelected() {

                            }

                            @Override
                            public void onOkSelected() {
                                PedirTaxiFragment pedirTaxiFragment = PedirTaxiFragment.newInstance(viajes.get(i));
                                getFragmentManager().beginTransaction().replace(R.id.content_frame, pedirTaxiFragment).commit();
                            }
                        }).show(getFragmentManager(), "");
                    }
                });
            }
        }
    }

    private void initComponents(){
        this.txtFechaDesde = (TextView) getView().findViewById(R.id.txtFechaDesde);
        this.txtFechaDesde.setKeyListener(null);
        this.txtFechaDesde.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_FECHA_DESDE);
            }
        });

        //txtFechaHasta
        this.txtFechaHasta = (TextView)getView().findViewById(R.id.txtFechaHasta);
        this.txtFechaHasta.setKeyListener(null);
        this.txtFechaHasta.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialogSeleccionarFecha(DIALOG_FECHA_HASTA);
            }
        });
        listViewViajes = (ListView) getView().findViewById(R.id.listView);

    }

    private void initVar(){
        Calendar calendar = Calendar.getInstance();

        this.fechaHasta = calendar.getTime();
        calendar.add(Calendar.MONTH, -1);
        this.fechaDesde = calendar.getTime();

        this.txtFechaDesde.setText(Formatter.formatDate(fechaDesde));
        this.txtFechaHasta.setText(Formatter.formatDate(fechaHasta));
    }

    private void getDialogSeleccionarFecha(int tipoFecha){
        final int tipo = tipoFecha;
        Date fecha;
        if (tipoFecha == DIALOG_FECHA_DESDE)
            fecha = this.fechaDesde;
        else
            fecha = this.fechaHasta;

        Calendar cal = Calendar.getInstance();
        cal.setTime(fecha);

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialogFecha = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(year, monthOfYear, dayOfMonth);

                if (tipo == DIALOG_FECHA_DESDE){
                    fechaDesde = cal.getTime();
                    String fechaDesdeString = Formatter.formatDate(fechaDesde);
                    txtFechaDesde.setText(fechaDesdeString);
                }else{
                    fechaHasta = cal.getTime();
                    String fechaHastaString = Formatter.formatDate(fechaHasta);
                    txtFechaHasta.setText(fechaHastaString);
                }
                loadViajes();
            }
        }, year, month, day);

        //_ventas = ventaBo.recoveryVentas(this.fechaDesde, this.fechaHasta);

        dialogFecha.show();
    }
}
