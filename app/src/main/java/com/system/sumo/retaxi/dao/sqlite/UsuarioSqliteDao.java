package com.system.sumo.retaxi.dao.sqlite;

import android.database.Cursor;

import com.system.sumo.retaxi.dao.CondicionDao;
import com.system.sumo.retaxi.dao.IUsuarioDao;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.dto.mapping.UsuarioMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 21/09/2016.
 */
public class UsuarioSqliteDao extends GenericSqliteDao implements IUsuarioDao{

    @Override
    public Integer create(Usuario entity ) throws Exception {
        table = UsuarioMapping.TABLA;
        insertMap.put(UsuarioMapping.ID, entity.getId());
        insertMap.put(UsuarioMapping.APELLIDO, entity.getApellido());
        insertMap.put(UsuarioMapping.DNI, entity.getDni());
        insertMap.put(UsuarioMapping.EMAIL, entity.getEmail());
        insertMap.put(UsuarioMapping.NOMBRE, entity.getNombre());
        insertMap.put(UsuarioMapping.URL_IMAGEN, entity.getUrlImagen());
        insertMap.put(UsuarioMapping.USER_TYPE, entity.getTipoUsuario());
        insertMap.put(UsuarioMapping.REPUTATION, entity.getReputation());
        super.create();
        return null;
    }

    @Override
    public Usuario recoveryByID(Integer id ) throws Exception {
        listTables.add(UsuarioMapping.TABLA);
        parameterWhere.put(UsuarioMapping.ID, new CondicionDao("=", id));
        Cursor cursor = recoveryData();
        return getUsuarios(cursor).get(0);
    }

    @Override
    public List<Usuario> recoveryByUserType(int userType ) throws Exception {

        listTables.add(UsuarioMapping.TABLA);
        parameterWhere.put(UsuarioMapping.USER_TYPE, new CondicionDao("=", userType));
        Cursor cursor = recoveryData();
        return getUsuarios(cursor);
    }

    @Override
    public void deleteByUserType(int userType ) throws Exception {
        table = UsuarioMapping.TABLA;
        parameterWhere.put(UsuarioMapping.USER_TYPE, new CondicionDao("=", userType));
        delete();
    }

    public static Usuario mappingToDto(Cursor cursor){
        Usuario usuario = new Usuario();
        int indexIdUsuario = cursor.getColumnIndex(UsuarioMapping.ID);
        usuario.setId(cursor.getInt(indexIdUsuario));
        int indexApellido = cursor.getColumnIndex(UsuarioMapping.APELLIDO);
        if (indexApellido >= 0) {
            usuario.setApellido(cursor.getString(indexApellido));
        }
        int indexDni = cursor.getColumnIndex(UsuarioMapping.DNI);
        if (indexDni >= 0) {
            usuario.setDni(cursor.getInt(indexDni));
        }
        int indexEmail = cursor.getColumnIndex(UsuarioMapping.EMAIL);
        if (indexEmail >= 0) {
            usuario.setEmail(cursor.getString(indexEmail));
        }
        int indexNombre = cursor.getColumnIndex(UsuarioMapping.NOMBRE);
        if (indexNombre >= 0) {
            usuario.setNombre(cursor.getString(indexNombre));
        }
        int indexUrlImagen = cursor.getColumnIndex(UsuarioMapping.URL_IMAGEN);
        if (indexUrlImagen >= 0) {
            usuario.setUrlImagen(cursor.getString(indexUrlImagen));
        }
        return usuario;
    }

    @Override
    public List<Usuario> recoveryAll( ) throws Exception {
        listTables.add(UsuarioMapping.TABLA);
        Cursor cursor = recoveryData();
        return getUsuarios(cursor);
    }

    private List<Usuario> getUsuarios(Cursor cursor){
        List<Usuario> usuarios = new ArrayList<Usuario>();
        try {
            if (cursor.moveToFirst()) {
                do {
                    usuarios.add(mappingToDto(cursor));
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return usuarios;
    }

    @Override
    public void update(Usuario entity ) throws Exception {

    }

    @Override
    public void delete(Usuario entity ) throws Exception {

    }

    @Override
    public void delete(Integer integer ) throws Exception {

    }
}
