package com.system.sumo.retaxi.view.chofer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.services.LocationService;
import com.system.sumo.retaxi.services.MyFirebaseInstanceIDService;
import com.system.sumo.retaxi.services.MyFirebaseMessagingService;

import static com.system.sumo.retaxi.view.chofer.ConectarseTabFragment.REQUEST_CHECK_SETTINGS;


public class ChoferMainTabsActivity extends FragmentActivity {
    private static String TAB1 = "tab1";
    private static String TAB2 = "tab2";
    private static String TAB3 = "tab3";
    private FragmentTabHost tabHost;
    private static String EXTRA_IS_CHOFER_EN_VIAJE = "EXTRA_IS_CHOFER_EN_VIAJE";
    private static String EXTRA_IS_CHOFER_CONECTADO = "EXTRA_IS_CHOFER_CONECTADO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chofer_main);
        initComponents();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {
            Fragment uploadType = getSupportFragmentManager().findFragmentByTag(TAB2);
            if (uploadType != null) {
                uploadType.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void initComponents(){

        tabHost= (FragmentTabHost) findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec(TAB1).setIndicator(getTabIndicator(this, getString(R.string.cuenta), R.mipmap.ic_perm_identity_white_24dp)),
                CuentaChoferFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec(TAB2).setIndicator(getTabIndicator(this, getString(R.string.conectarse), R.mipmap.ic_directions_car_white_24dp)),
                ConectarseTabFragment.class, null);
        tabHost.addTab(tabHost.newTabSpec(TAB3).setIndicator(getTabIndicator(this, getString(R.string.viajes), R.mipmap.ic_trending_up_white_24dp)),
                TravelsFragment.class, null);
        /*Intent i = new Intent(this, UpdateScreenOnOfService.class);
        this.startService(i);*/

    }

    private View getTabIndicator(Context context, String title, int icon) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_layout, null);
        ImageView iv = (ImageView) view.findViewById(R.id.imageView);
        iv.setImageResource(icon);
        return view;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, MyFirebaseInstanceIDService.class));
        stopService(new Intent(this, MyFirebaseMessagingService.class));
        stopService(new Intent(this, LocationService.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
