package com.system.sumo.retaxi.dto;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import com.system.sumo.retaxi.utils.Constantes;

/**
 * Created by usuario on 23/10/2016.
 */

public class LocationPoint implements Serializable{

    @SerializedName(Constantes.LOCATION_POINT_TYPE)
    private String type;
    @SerializedName(Constantes.COORDINATES)
    private double coordinates[];
    /*public LocationPoint (double coordinates[]){
        type = "Point";
        this.coordinates = coordinates;
    }*/

    public LocationPoint(){}

    public LocationPoint(String type, double coordinates[]){
        this.type = type;
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }
}
