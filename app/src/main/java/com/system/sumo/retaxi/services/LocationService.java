package com.system.sumo.retaxi.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.dto.LocationPoint;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.view.chofer.ConectarseTabFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static com.system.sumo.retaxi.utils.ServicesRoutes.SEND_LOCATION_TO_RIDER;
import static com.system.sumo.retaxi.utils.Constantes.POSITION;
import static com.system.sumo.retaxi.view.chofer.ConectarseTabFragment.createLocationRequest;

/**
 * Created by usuario on 24/10/2016.
 */

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks{

    private GoogleApiClient mGoogleApiClient;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback locationCallback;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startForeground(12345678, getNotification());
        startLocationUpdates();
    }

    public Notification getNotification(){
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, "")
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setSmallIcon(R.drawable.icono_blanco_chat)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_warning_black_24dp))
                        .setContentTitle(getString(R.string.atencion))
                        .setContentText(getResources().getString(R.string.update_localization))
                        .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("","Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        //notificationManager.notify(Avisos.ID_NOTIFICATION_LOCATION, notificationBuilder.build());
        return notificationBuilder.build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("D", "onConnected");
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private RestCustomerVolley.OnRequestRestCustomerFinish getOnRequestCustomerFinish() {
        return new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                if(ConectarseTabFragment.isFragmentActive && ConectarseTabFragment.updateLastLocationSended != null)
                    ConectarseTabFragment.updateLastLocationSended.actualizarUbicacion(Calendar.getInstance().getTime());
            }

            @Override
            public void onErrorRestCustomerResult(String result) {

            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    private void startLocationUpdates(){
        Log.d("D", "startLocationUpdates");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }else {
            boolean a = MyApplication.getGoogleApiHelper().getGoogleApiClient() != null;
            boolean b = MyApplication.getInstance().getLocationRequest() != null;
            Log.d("D", "getGoogleApiHelper is connected?:" + MyApplication.getGoogleApiHelper().isConnected() + " getGoogleApiHelper().getGoogleApiClient() != null: " + a + " getLocationRequest " +b);
            if(MyApplication.getGoogleApiHelper().isConnected() && MyApplication.getGoogleApiHelper().getGoogleApiClient() != null && MyApplication.getInstance().getLocationRequest() != null) {
                Log.d("D", "comenzando capturas de localizaciones");

                fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
                fusedLocationProviderClient.requestLocationUpdates(MyApplication.getInstance().getLocationRequest(), getLocationCallback(),null);
                //LocationServices.FusedLocationApi.requestLocationUpdates(
                        //MyApplication.getGoogleApiHelper().getGoogleApiClient(), >MyApplication.getInstance().getLocationRequest(), this);
            }else{
                Log.d("D", "inicializando variables");
                LocationRequest locationRequest = createLocationRequest();
                MyApplication.getInstance().setLocationRequest(locationRequest);
                MyApplication.getGoogleApiHelper().connect();
                buildGoogleApiClient();
                mGoogleApiClient.connect();
                //MyApplication.getGoogleApiHelper().setGoogleApiClient(mGoogleApiClient);
            }
        }
    }

    private LocationCallback getLocationCallback(){
        locationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    String url;
                    final JSONObject body = new JSONObject();
                    double[] point = {location.getLatitude(), location.getLongitude()};
                    LocationPoint locationPoint = new LocationPoint();
                    locationPoint.setType("Point");
                    locationPoint.setCoordinates(point);
                    Gson g = new Gson();
                    try {
                        body.put(POSITION,  g.toJsonTree(locationPoint));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    /*if(MyApplication.getInstance().isConductorEnViaje()) {
                        url = SEND_LOCATION_TO_RIDER;
                        try {
                            body.put(getString(R.string.extra_posicion),  g.toJsonTree(locationPoint));
                        } catch (JSONException e) { e.printStackTrace(); }
                    }else{
                        url = UPDATE_DRIVER_LOCATION;
                        try {
                            body.put(getString(R.string.extra_posicion), location.getLatitude() +","+location.getLongitude());
                        } catch (JSONException e) { e.printStackTrace(); }
                    }*/

                    RestCustomerVolley.addQueue(Request.Method.POST, SEND_LOCATION_TO_RIDER, body, true, getOnRequestCustomerFinish());
                }
            };

        };
        return locationCallback;
    }
}
