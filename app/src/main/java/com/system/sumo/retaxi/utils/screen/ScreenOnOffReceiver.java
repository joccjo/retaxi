package com.system.sumo.retaxi.utils.screen;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.system.sumo.retaxi.asynkTasks.GetTimerPannicButtonTask;

/**
 * Created by usuario on 03/09/2016.
 */
public class ScreenOnOffReceiver extends BroadcastReceiver {
    private boolean screenOff;
    public static String SCREEN_STATE_EXTRA = "screen_state";
    private static int contador = 0;
    private final static int CANTIDAD_BUTTON_PRESSED = 3;
    private Context context;

    public ScreenOnOffReceiver(){

    }

    public ScreenOnOffReceiver(Context context){
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        contador++;
        if(contador == CANTIDAD_BUTTON_PRESSED) {
            /*Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel://911"));
            intentCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            context.startActivity(intent);*/
            /*Intent i = new Intent(context, UpdateScreenOnOfService.class);
            i.putExtra(SCREEN_STATE_EXTRA, screenOff);
            context.startService(i);*/
            Intent i = new Intent(context, UpdateScreenOnOfService.class);
            i.putExtra(SCREEN_STATE_EXTRA, "extra");
            context.bindService(i, mServerConn, Context.BIND_AUTO_CREATE);

        }else{
            GetTimerPannicButtonTask timer = new GetTimerPannicButtonTask(getListenner(), contador);
            timer.execute();
        }

    }


    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d("ScreenOnOffReceiver", "onServiceConnected");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("ScreenOnOffReceiver", "onServiceDisconnected");
        }
    };

    private GetTimerPannicButtonTask.GetTimerListener getListenner(){
        GetTimerPannicButtonTask.GetTimerListener listenner = new GetTimerPannicButtonTask.GetTimerListener(){

            @Override
            public void onDone(int contadorTask) {
                if(contador == contadorTask){
                    contador = 0;
                }
            }
        };
        return listenner;
    }
}
