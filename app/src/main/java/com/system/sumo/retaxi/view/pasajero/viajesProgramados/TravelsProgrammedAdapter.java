package com.system.sumo.retaxi.view.pasajero.viajesProgramados;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.utils.Formatter;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class TravelsProgrammedAdapter extends RecyclerView.Adapter<TravelsProgrammedAdapter.ViewHolder>{

    private List<Viaje> travels;

    public TravelsProgrammedAdapter(List<Viaje> travels){
        this.travels = travels;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_travels_programmed_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Viaje travel = travels.get(position);
        holder.hastaValue.setText(travel.getHasta());
        holder.desdeValue.setText(travel.getDesde());

        try {
            Date date = Formatter.convertToDateTimeDjangoTravel(travel.getFechaHora());
            holder.fechaYHoraValue.setText(Formatter.formatDateTimeMinutes(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return travels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView fechaYHoraValue;
        TextView desdeValue;
        TextView hastaValue;

        ViewHolder(View view){
            super(view);
            desdeValue = view.findViewById(R.id.desdeValue);
            hastaValue = view.findViewById(R.id.hastaValue);
            fechaYHoraValue = view.findViewById(R.id.fechaYHoraValue);
        }
    }
}
