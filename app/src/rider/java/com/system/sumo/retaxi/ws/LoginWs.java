package com.system.sumo.retaxi.ws;

import android.util.Log;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Usuario;
import com.system.sumo.retaxi.utils.RestCustomerVolley;
import com.system.sumo.retaxi.ws.GenericWs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static com.system.sumo.retaxi.utils.ServicesRoutes.BASE_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_ID;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_ID_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_SECRET;
import static com.system.sumo.retaxi.utils.ServicesRoutes.CLIENT_SECRET_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.GET_ACCESS_TOKEN_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_ASSOCIATE_DRIVER_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_RIDER_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_CONVER_TOKEN;
import static com.system.sumo.retaxi.utils.ServicesRoutes.LOGIN_ADDITIONAL_DATA_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_BACKEND_FACEBOOK;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_BACKEND_GOOGLE;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_IP;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_ROUTE;

public class LoginWs extends GenericWs {

    private void convertToken(String tokenGoogle, String tokenFacebook, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("grant_type", "convert_token");
            body.put("client_id", CLIENT_ID);
            body.put("client_secret", CLIENT_SECRET);
            if(tokenGoogle != null)
                body.put("backend", "google-oauth2");
            else if (tokenFacebook != null)
                body.put("backend", "facebook");
            body.put("token", tokenGoogle);
        } catch (JSONException e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_CONVER_TOKEN, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                JSONObject result = null;
                try {
                    result = new JSONObject(object);
                    String accessToken = result.getString("access_token");
                    String refreshToken = result.getString("refresh_token");
                    PreferencesBo.saveTokensInPreferences(accessToken);
                    genericWsResponse.onOkResult(accessToken);
                } catch (JSONException e) {
                    e.printStackTrace();
                    genericWsResponse.onErrorResult();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void loginGoogle(String codigoAuth, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("provider", WS_BACKEND_GOOGLE);
            body.put("code", codigoAuth);
            body.put("redirect_uri",BASE_WS + WS_IP + WS_ROUTE);
        } catch (JSONException e) { e.printStackTrace(); }

        loginRider(body, genericWsResponse);
    }

    public void loginFacebook(String tokenfc, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("provider", WS_BACKEND_FACEBOOK);
            body.put("code", 1);
            body.put("access_token", tokenfc);
            body.put("redirect_uri",BASE_WS + WS_IP + WS_ROUTE);
        } catch (JSONException e) { e.printStackTrace(); }
        loginRider(body, genericWsResponse);
    }

    private void loginRider(JSONObject body, final GenericWsResponse genericWsResponse){
        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_RIDER_WS, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Gson gson = new Gson();
                Usuario user = gson.fromJson(object, Usuario.class);
                user.setTipoUsuario(Usuario.TIPO_PASAJERO);
                PreferencesBo.saveUserInPreferences(user);
                PreferencesBo.saveTokensInPreferences(user.getTokenOauth());
                genericWsResponse.onOkResult(user);
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                if(result != null) Log.d("LoginWsError: ", result);
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void setAdditionalData(String telephone, String sex, final String birthDate, final GenericWsResponse genericWsResponse){

        JSONObject body = new JSONObject();
        try {
            body.put("telephone", telephone);
            body.put("birthday", birthDate);
            body.put("sex", sex);
        } catch (Exception e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, LOGIN_ADDITIONAL_DATA_WS, body, true, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                Usuario user = MyApplication.getInstance().getPreferences().getUsuario();
                user.setBirthday(birthDate);
                PreferencesBo.saveUserInPreferences(user);
                genericWsResponse.onOkResult(user);
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }

    public void getAccessToken(String code, final GenericWsResponse genericWsResponse){
        JSONObject body = new JSONObject();
        try {
            body.put("grant_type", "authorization_code");
            body.put("client_id", CLIENT_ID_GOOGLE);
            body.put("client_secret", CLIENT_SECRET_GOOGLE);
            body.put("redirect_uri","");
            body.put("code", code);
        } catch (JSONException e) { e.printStackTrace(); }
        RestCustomerVolley.addQueue(Request.Method.POST, GET_ACCESS_TOKEN_GOOGLE, body, false, new RestCustomerVolley.OnRequestRestCustomerFinish() {
            @Override
            public void onOkRestCustomerResult(String object) {
                try {
                    JSONObject result = new JSONObject(object);
                    String access_token = result.getString("access_token");
                    convertToken(access_token, null, genericWsResponse);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorRestCustomerResult(String result) {
                genericWsResponse.onErrorResult();
            }
        });
    }
}
