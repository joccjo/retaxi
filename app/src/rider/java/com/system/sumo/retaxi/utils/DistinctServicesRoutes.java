package com.system.sumo.retaxi.utils;
import static com.system.sumo.retaxi.utils.ServicesRoutes.BASE_WS;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_IP;
import static com.system.sumo.retaxi.utils.ServicesRoutes.WS_ROUTE;

public class DistinctServicesRoutes {
    public static final String GET_TRAVELS = BASE_WS + WS_IP + WS_ROUTE + "viajes_pasajero/";
}
