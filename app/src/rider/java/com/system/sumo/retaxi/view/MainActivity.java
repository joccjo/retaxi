package com.system.sumo.retaxi.view;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.system.sumo.retaxi.R;
import com.system.sumo.retaxi.bo.PreferencesBo;
import com.system.sumo.retaxi.dto.MyApplication;
import com.system.sumo.retaxi.dto.Viaje;
import com.system.sumo.retaxi.services.ConnectionReceiver;
import com.system.sumo.retaxi.services.NetworkSchedulerService;
import com.system.sumo.retaxi.LoginActivity;
import com.system.sumo.retaxi.view.dialogs.generics.SimpleDialogFragment;
import com.system.sumo.retaxi.view.pasajero.viajesProgramados.TravelsProgrammedFragment;
import com.system.sumo.retaxi.view.pasajero.PedirTaxiFragment;
import com.system.sumo.retaxi.view.dialogs.ViajesDialogFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.OnConnectionFailedListener{


    private GoogleApiClient mGoogleApiClient;
    private String TAG_PEDIR_MOVIL = "tag_pedit_taxi";
    private String TAG_VIAJES_REALIZADOS = "tag_viajes_realizados";
    private String TAG_VIAJES_PROGRAMADOS = "tag_viajes_programados";
    boolean isFirstBack = true;
    PedirTaxiFragment pedirTaxiFragment;
    private static String EXTRA_USUARIO = "EXTRA_USUARIO";
    private static String EXTRA_VIAJE = "EXTRA_VIAJE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_pasajero);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isFirstBack = true;
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if(findViewById(R.id.content_frame) != null && savedInstanceState == null){
            pedirTaxiFragment = new PedirTaxiFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, pedirTaxiFragment, TAG_PEDIR_MOVIL);

            ft.addToBackStack(TAG_PEDIR_MOVIL);
            ft.commit();
        }

        View headerLayout = navigationView.getHeaderView(0);
        TextView tvNombre = (TextView) headerLayout.findViewById(R.id.tvNombre);
        tvNombre.setText(MyApplication.getInstance().getPreferences().getUsuario().getNombre());
        TextView tvApellido = (TextView) headerLayout.findViewById(R.id.tvApellido);
        tvApellido.setText(MyApplication.getInstance().getPreferences().getUsuario().getApellido());
        final RoundedImageView imageViewMenu = (RoundedImageView) headerLayout.findViewById(R.id.imgViewProfileMenu);
        Picasso.get().load(MyApplication.getInstance().getPreferences().getUsuario().getUrlImagen())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageViewMenu, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError(Exception e) {
                        Picasso.get().load(MyApplication.getInstance().getPreferences().getUsuario().getUrlImagen())
                                .into(imageViewMenu);
                    }
                });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob();
        }else{
            Intent connectionReceiverIntent = new Intent(this, ConnectionReceiver.class);
            startService(connectionReceiverIntent);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            if(isFirstBack){
                isFirstBack = false;
                Toast.makeText(this, R.string.presioneNuevamente, Toast.LENGTH_SHORT).show();
            }else{
                System.exit(0);
            }
        } else {
            drawer.openDrawer(GravityCompat.START);
            //super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment uploadType = getSupportFragmentManager().findFragmentByTag(TAG_PEDIR_MOVIL);
        if (uploadType != null) {
            uploadType.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Fragment uploadType = getSupportFragmentManager().findFragmentByTag(TAG_PEDIR_MOVIL);
        if (uploadType != null) {
            uploadType.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onStop() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            stopService(new Intent(this, NetworkSchedulerService.class));
        }else{
            stopService(new Intent(this, ConnectionReceiver.class));
        }
        super.onStop();
    }

    @Override
    protected void onStart() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            startService(new Intent(this, NetworkSchedulerService.class));
        }else{
            startService(new Intent(this, ConnectionReceiver.class));
        }
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_pasajero, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return false;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (item.getItemId()){
            case R.id.nav_pedir_taxi:
                //getSupportFragmentManager().popBackStack();
                startFragmentGetTaxi(null);
                break;
            case R.id.nav_viajes_realizados:
                ViajesDialogFragment viajesDialogFragment = ViajesDialogFragment.newInstance(null, new ViajesDialogFragment.ViajeSelected() {
                    @Override
                    public void onViajeSelected(Viaje viaje) {
                        startFragmentGetTaxi( viaje);
                    }
                });
                ft.replace(R.id.content_frame, viajesDialogFragment, TAG_VIAJES_REALIZADOS);
                ft.addToBackStack(TAG_PEDIR_MOVIL);
                ft.commit();

                /*TravelProgrammerDialogFragment.newInstance(null, new TravelProgrammerDialogFragment.ViajeSelected() {
                    @Override
                    public void onViajeSelected(Viaje viaje) {
                        PedirTaxiFragment pedirTaxiFragment = PedirTaxiFragment.newInstance(viaje);
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.content_frame, pedirTaxiFragment, TAG_VIAJES_REALIZADOS);

                        ft.addToBackStack(TAG_VIAJES_REALIZADOS);
                        ft.commit();
                    }
                }).show(getSupportFragmentManager(), "");*/
                break;
            case R.id.nav_viajes_programados:
                TravelsProgrammedFragment travelsProgrammedFragment = new TravelsProgrammedFragment();
                //travelsProgrammedFragment.show(getSupportFragmentManager(), "");
                ft.replace(R.id.content_frame, travelsProgrammedFragment, TAG_VIAJES_PROGRAMADOS);
                ft.addToBackStack(TAG_PEDIR_MOVIL);
                ft.commit();
                break;
            case R.id.nav_salir:
                this.finish();
                break;
            case R.id.nav_cerrar_session:
                Viaje currentTravel = MyApplication.getInstance().getPreferences().getViaje();
                if (currentTravel != null && !currentTravel.getEstado().equals(Viaje.TRAVEL_STATE_ACCEPTED) && !currentTravel.getEstado().equals(Viaje.TRAVEL_STATE_STARTED) && !currentTravel.getEstado().equals(Viaje.TRAVEL_STATE_PENDING)) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                            new ResultCallback<Status>() {
                                @Override
                                public void onResult(Status status) {
                                    if (status.isSuccess()) {
                                        PreferencesBo.deleteUserFromPreferences();
                                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                        finish();
                                    }
                                }
                            });
                }else{
                    SimpleDialogFragment.newInstance(SimpleDialogFragment.TYPE_OK, getString(R.string.atencion), getString(R.string.conductoresNoDisponibles)).show(getSupportFragmentManager(), "");
                }
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        startFragmentGetTaxi(null);
    }

    private void startFragmentGetTaxi(Viaje viaje) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        PedirTaxiFragment pedirTaxiFragment = PedirTaxiFragment.newInstance(viaje);
        ft.replace(R.id.content_frame, pedirTaxiFragment, TAG_PEDIR_MOVIL);
        ft.addToBackStack(TAG_PEDIR_MOVIL);
        ft.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(MainActivity.this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public interface OpenCloseNavigation{
        void onOpenCloseNavigation();
    }
}

